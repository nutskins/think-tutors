<?php $title= "Think Tutors | Tuition | Mentoring | Admissions Advisory"; ?>
<?php $metadescription= "A clear voice in a sea of noise, providing unrivalled academic tuition, admissions advisory and profound mentoring in equal measure across the UK & internationally.";?>
<?php include 'header.php' ?>
<main>
		<section>
			<div class="banner-home" style="background-image:url(images/1home.jpg);">
			</div>
		</section>
		<section>
				<div class="wrapper">
					<div>
					<h2>Elite Professional Tuition and Education Advisory </h2>
					<p style="text-align:justify">Forging close relationships with families in the UK and internationally, we are focused on cultivating minds to build confidence and think without limits. Think Tutors is a clear voice in a sea of noise, providing unrivalled academic tuition, advisory and profound mentoring in equal measure. </p>
					<p style="text-align:justify">Every student thinks independently, which is why we create a bespoke and actionable approach with each student we work with. Academic fulfilment and admissions success often begins with building confidence with the subject matter itself and by nurturing each student to think critically and find creative solutions to seemingly complex problems.</p>
					<p style="text-align:justify">At Think Tutors, we provide the flexibility to work around your busy schedule, providing tuition on an hourly basis or longer term as part of a residential placement in the UK or internationally.</p>
					<h2>Bespoke</h2>
					<p style="text-align:justify">We provide fully vetted, handpicked professional tutors and mentors, that match the individual needs and circumstances of each of our students. We pride ourselves on forging close relationships with families, schools and our tutors, and maintain frequent contact throughout the duration of the tuition and mentorship. All of our tutors and mentors attended top universities so they are well equipped to guide our students through the entire education process, from 7 Plus entry to postgraduate degrees.</p>
					<p style="text-align:justify">With headquarters based in Mayfair (London) and operations spanning the rest of the globe, we can cover any tuition or mentoring requirements at short notice anywhere in the world.</p>
							<h2>Academic Excellence</h2>
							<p style="text-align:justify">Over the years we have built a strong and extensive network of highly talented, exceptional tutors who can offer support at all levels of the educational system. All of our tutors have obtained an excellent Bachelor's degree as a minimum, with the majority having pursued postgraduate degrees in their specialist fields. As a result, they know first-hand what it takes to strive for academic excellence. Our tutors are quick and skilled at identifying gaps in the students’ understanding and focus on resolving these to deliver high results.</p>
							<h2>The Journey</h2>
							<p style="text-align:justify">First, we like to understand your needs as a family and ultimately the ambitions of your child. We can do this via telephone, online or face-to-face, no matter the location.
							<p style="text-align:justify">We meet with a carefully selected group of tutors and mentors from our elite network to discuss the aims and expectations of the family. We will also have a detailed discussion with our expert admissions consultant if school or university entry is being considered. </p>
							<p style="text-align:justify">We will bring forward the very best tutor and or mentor who we feel will be the best fit for the student and your family. We will provide you with a profile containing the tutor and or mentor’s qualifications, previous employment, testimonials and confirmation of an up-to-date enhanced DBS. You can rest assured that that the tutor or mentor we provide is passionate for their subject and can deliver expert tuition or mentorship in an engaging and professional manner. </p>
							<p style="text-align:justify">The tutor and or mentor will focus on building a rapport with the student and identifying key gaps in their learning. We are also there every step of the way for any questions or concerns you may have. We know the build up to exams can be stressful for everyone involved, including the parents, so we are always on hand to offer support. </p>
						<div class="clear"></div>
					</div>
		</section>

			<section>
		<div class="review_slide blue">
				<div class="wrapper">
					  <div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Our tutor was exceptional, showing the ability to convey<br>the course content in a simple yet concise manner,<br>making it easy to pick up and remember.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Thank you once again for the support<br>and guidance that you and Sebastian gave to our students,<br>it definitely did have a positive impact.</h4>
								<h5>Head of Sixth Form</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
                            <img src="images/ttquotewhite.svg" alt="" >
								<h4>A stroke of brilliance.</h4>
								<h5>Chris, father of BSc Geography dissertation student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>I now feel confident to take my exams and would like<br>to thank them for their patience and commitment<br>towards achieving my goal.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Very quick to reply to our initial search for a geography<br>tutor. They clearly have excellent knowledge of the<br>subject and the current curriculum.</h4>
								<h5>Andrea, mother of A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>James knows a lot about University<br>testing procedures and was able to give<br>advice on a difficult course.</h4>
								<h5>Rhea, mother of BSc Astro Geochemistry student</h5>
							</div>
							</div>
						</div>
					<div class="swiper-pagination"></div>
				  </div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		
		<?php include 'footer_contact-form.php';?>
	</main>

<?php include 'footer.php' ?>
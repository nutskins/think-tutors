<?php $title= "Who We Are | Think Tutors"; ?>
<?php $metadescription= "Meet the team dedicated to providing expert tuition, mentoring and admissions advisory across the UK & internationally.";?>
<?php $page = "services"; include 'header.php' ?>
<main>
	<section>
		<div class="banner" style=" background-image:url(images/2aboutus.jpg)">
		<div class="title"><h1>Who We Are</h1></div>
		</div>
	</section>
	
		<div class="int_content">
			<div class="wrapper">
                <div class="clear"></div>
</div>
</div>
                
                <section>

				<div class="wrapper">
					<div class="both_pnl">
						<div class="righttext">
							<h2>James Mitchell</h2>
							<h3>Co-founder and Executive Director</h3>
							<p>James is a Co-founder and Executive Director at Think Tutors. He has tutored & mentored professionally for over 10 years ranging from the pre-test at 11+ through to postgraduate level. James is well-versed with institutional admissions and leverages his large network of strategic connections within the education sector to assist our families with school and university entrance. James most recently studied at Saïd Business School, University of Oxford.</p>
						</div>
						<div class="leftimage">
							<img src="images/teamjames.png" alt="James">
						</div>
						<div class="clear"></div>
					</div>
				</div>

		</section>
		<section>

				<div class="wrapper">
					<div class="both_pnl">
						<div class="lefttext">
							<h2>Neil Ridley</h2>
							<h3>Co-founder and Executive Director</h3>
                            <p>Neil is a Co-founder and Executive Director at Think Tutors. Neil obtained a first-class Bachelor’s degree in Geology from the University of Southampton before embarking on a Master’s degree at Royal Holloway, University of London. Neil began his career working for almost 10 years in the Oil & Gas sector for different international operators. Throughout his tenure as a Geologist, Neil tutored many undergraduate and postgraduate students and brings his wealth of experience to Think Tutors.</p>
                            </div>
                            <div class="rightimage">
							<img src="images/teamneil.png" alt="Neil">
						</div>
						
						<div class="clear"></div>
					</div>
				</div>

        </section>

		<section>

				<div class="wrapper">
					<div class="both_pnl">
						<div class="righttext">
							<h2>Harry Gilson</h2>
							<h3>Tutor Manager</h3>
							<p>Harry is the Tutor Manager at Think Tutors. Having worked in the non-profit sector after graduating from UCL with an MSc in International Public Policy, he joined the team with experience managing a range of projects. Harry’s approach to the onboarding and management of tutors is both personal and exacting, ensuring that ultimately each student is allocated a tutor entirely equipped to meet their educational needs.</p>
						</div>
						<div class="leftimage">
							<img src="images/teamharry.png" alt="Harry">
						</div>
						<div class="clear"></div>
					</div>
				</div>

		</section>

              
		
       
				<div class="clear">

				</div>
        </div>
        <div class="clear"></div>
    </section>
    <div class="int_content">
			<div class="wrapper">

                <div class="clear"></div>
</div>
</div>
	
	
		<section>
		<div class="review_slide blue">
				<div class="wrapper">
					  <div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Our tutor was exceptional, showing the ability to convey<br>the course content in a simple yet concise manner,<br>making it easy to pick up and remember.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Thank you once again for the support<br>and guidance that you and Sebastian gave to our students,<br>it definitely did have a positive impact.</h4>
								<h5>Head of Sixth Form</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
                            <img src="images/ttquotewhite.svg" alt="" >
								<h4>A stroke of brilliance.</h4>
								<h5>Chris, father of BSc Geography dissertation student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>I now feel confident to take my exams and would like<br>to thank them for their patience and commitment<br>towards achieving my goal.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Very quick to reply to our initial search for a geography<br>tutor. They clearly have excellent knowledge of the<br>subject and the current curriculum.</h4>
								<h5>Andrea, mother of A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>James knows a lot about University<br>testing procedures and was able to give<br>advice on a difficult course.</h4>
								<h5>Rhea, mother of BSc Astro Geochemistry student</h5>
							</div>
							</div>
						</div>
					<div class="swiper-pagination"></div>
				  </div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<?php include 'footer_contact-form.php';?>
	</main>

<?php include 'footer.php' ?>
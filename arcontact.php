<?php

if(isset($_POST['submit'])) {
    // Import validation library
    require("validation-2.3.3.php");
    

    // Rules for validation
    $rules[] = "required,name,First Name field is required.";
	$rules[] = "required,email,Email field is required.";
	$rules[] = "valid_email,email,Please enter a valid email address.";
	$rules[] = "required,phone,Phone field is required.";
    $rules[] = "required,comment,Comments / Questions URL field is required.";
    $rules[] = "required,contact-data,Please indicate that you have read and agree to our Data Privacy Policy.";
    
    $errors = validateFields($_POST, $rules);

    // Validate captcha
    $post_data = http_build_query(
        array(
            "secret" => "6Lfkkl4UAAAAAIT1-k4GCKmKDldnRehul7SzsF8D",
            "response" => $_POST["g-recaptcha-response"],
            "remoteip" => $_SERVER["REMOTE_ADDR"]
        )
    );
    $opts = array("http" =>
        array(
            "method"  => "POST",
            "header"  => "Content-type: application/x-www-form-urlencoded",
            "content" => $post_data
        )
    );
    $context  = stream_context_create($opts);
    $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
    $result = json_decode($response);

    // Check if captcha was valid
    if (!$result->success) {
        $errors[] = "The reCAPTCHA submission failed."; // Incorrect captcha solution
    }

    // Check for errors
    if (!empty($errors)) {
        $fields = $_POST;
    } else { // No errors :)
        
        include("conn.php");

        $name = $_POST['name'];
		$email = $_POST['email'];
		$phone = $_POST['phone'];
		$comment = $_POST['comment'];
		$date = $_POST['date'];
		
		$bodycontent  = 'Name: '.$name .'<br>';
		$bodycontent .= 'Email: '.$email.'<br>';
		$bodycontent .= 'Phone: '.$phone.'<br>';
		$bodycontent .= 'Comments / Questions: '.$comment.'<br>';
		$sender_message = $name.','.'<br>'.
		
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		
		$webemail = 'contact@thinktutors.com';
		
		$headers .= "From: ".$name." <".$webemail.">\n";
		$toemail = "info@thinktutors.com";
		$subject = "Contact Form Submitted!";
		$sql = "insert into contact (name, email, phone, comments, date) VALUES ('$name', '$email', '$phone', '$comment', '$date')";
		if (mysqli_query ($conn , $sql)) {
			//echo "Successfully To Added Detail";
		}else{
			//echo "Error";
		}
		mysqli_close($conn);
		if (mail($toemail, $subject, $bodycontent, $headers)) {
			$msg = 'Contact Form Submitted!';
		} else {
			$msg = 'Message could not be sent.';
			//echo 'Mailer Error: ' . $mail->ErrorInfo;
		}
    }
}

?>

<?php $title= "اتصل | Think Tutors"; ?>
<?php $metadescription= "To enquire about our tuition services, please complete the contact form and include a few details of the tuition required.";?>
<?php $metakeywords= "";?>
<?php $page = "contact"; include 'arheader.php' ?>
<main>
	<section>
		<div class="map">
			<div style='overflow:hidden;height:400px;width:100%;position: relative;'><div id='gmap_canvas' style='height:410px;width:100%;'></div><div></div><div></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style></div>
		</div>
	</section>
	<section>
		<div class="contact_section top_pad">
				<div class="wrapper">
                	<div class="three-sec">
                        <div class="three_pnl">
                            <h3 class="mar-top">موصى عليه</h3>
                            <p>نحن نقدم مجموعة من المدرسين الذين تم إختيارهم بشكل كامل والذين يتناسبون مع الاحتياجات والظروف الفردية لطلابنا المحتملين.</p>
                            <h3>شخصي وبمودة</h3>
                            <p>يعمل جيمس ونيل بشكل وثيق مع كل طالب / عائلة ويمكن الاتصال بهما على مدار 24 ساعة في اليوم ، 7 أيام في الأسبوع.</p>
                            <h3>التميز الأكاديمي</h3>
                            <p>لقد أنشأنا شبكة من المدرسين الموهوبين الذين يمكنهم تقديم الدعم على جميع مستويات النظام التعليمي ، من 11 إلى ما قبل الاختبار إلى الدراسات العليا.</p>
                            <h3>دولي</h3>
                            <p>يقدم مدرسو Think Tutors دروسًا في جميع أنحاء المملكة المتحدة وحول العالم. كثير من عملائنا يسافرون ويعملون دوليًا ، لذلك نستفيد من خدماتنا الدراسية العالمية لتوفير دروس بدوام كامل أثناء التنقل.</p>
                        </div>
                        <div class="three_pnl">
                            <h3 class="mar-top">ابقى على تواصل</h3>
                            <p>للاستفسار عن خدماتنا الدراسية ، يرجى ملء نموذج الاتصال وتضمين بعض التفاصيل حول الرسوم الدراسية المطلوبة.</p>
                            <p>سنرد على جميع الاستفسارات في غضون 24 ساعة.</p>
                            <p>يمكن للمدرسين المستقبليين إكمال نموذج التسجيل الخاص بالمعلم.</a></p>
                            <p style="text-align:right" dir="ltr"><a href="mailto:info@thinktutors.com;">info@thinktutors.com</a><br><a href="tel:00442071172835">+44 (0) 207 117 2835</a></p>
                            <h3>مكاتب</h3>
                            <p>بيكاديللي تشامبرز ، دادلي هاوس<br>169 بيكاديللي ، مايفير ، لندن<br>W1J 9EH ، المملكة المتحدة.</p>
                        </div>
                        <div class="three_pnl" id="contact">
                            <div class="form_section">
                            <form action="" method="POST">
								<?php    
									if (!empty($errors)) {
									  echo "<div class='error' style='color:#003f70;'>Please fix the following errors:\n<ul>";
									  foreach ($errors as $error)
										echo "<li>$error</li>\n";

									  echo "</ul></div>";
									}
								?>
								<div class="row single">
                                <div class="col-sm-12 single">
                                    <div class="form-group">
                                        <label class="control-label">Name <span class="field_required" style="color:#ee0000;">*</span></label>
                                        <div class="">
                                            <input type="text" name="name" value="<?php if(isset($fields["name"])){ echo $fields["name"];}?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Email <span class="field_required" style="color:#ee0000;">*</span></label>
                                        <div class="">
                                            <input type="email" name="email" value="<?php if(isset($fields["email"])){ echo $fields["email"];}?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Phone <span class="field_required" style="color:#ee0000;">*</span></label>
                                        <div class="">
                                            <input id="phone" type="tel" class=" form-control" name="phone" value="<?php if(isset($fields["phone"])){ echo $fields["phone"];}?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Comments / Questions <span class="field_required" style="color:#ee0000;">*</span></label>
                                        <div class="">
                                            <textarea name="comment" class="form-control" rows="7"><?php if(isset($fields["comment"])){ echo $fields["comment"];}?></textarea>
                                        </div>
                                        <div dir="ltr" class="form-group">                                
                                            <label style="font-family: 'Hind Vadodara', sans-serif;">
                                                <input style="-webkit-appearance: checkbox; width: 18px; height: 18px; margin: 0; position: absolute;" type="checkbox" name="contact-data" <?php if(isset($fields["contact-data"])) echo "checked"; ?>>
                                                <span style="font-size: 14px; font-weight: 500; color: #78828c; display: inline-block; vertical-align: top; margin-top: -4px; padding-left: 30px; line-height: 25px;">By ticking this box you agree to our receipt and use of your data as set out in our <a href="https://thinktutors.com/data-privacy-policy.php">Data Privacy Policy</a></span>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <div class="g-recaptcha" data-sitekey="6Lfkkl4UAAAAAOu53yXbM8HpKgiocGozfJ1Q6dDj"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="submit">
											<input type="hidden" name="date" value="<?php echo date('Y-m-d'); ?>" />
                                            <input class="btn btn-default" type="submit" name="submit" value="Send" />
                                        </div>
                                        <?php if($msg){ echo $msg;}?>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="clear"></div>
                        </div>
                        </div>
                    </div>
				<div class="clear"></div>
			</div>
		</div>
	</section>
</main>
<?php include 'artestimonial.php';?>
<?php include 'arfooter.php' ?>
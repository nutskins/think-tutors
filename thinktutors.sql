-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jan 17, 2018 at 07:13 AM
-- Server version: 5.6.36-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `thinktutors`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog_cats`
--

CREATE TABLE IF NOT EXISTS `blog_cats` (
  `catID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catTitle` varchar(255) DEFAULT NULL,
  `catSlug` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`catID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `blog_cats`
--

INSERT INTO `blog_cats` (`catID`, `catTitle`, `catSlug`) VALUES
(6, 'News', 'news');

-- --------------------------------------------------------

--
-- Table structure for table `blog_members`
--

CREATE TABLE IF NOT EXISTS `blog_members` (
  `memberID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`memberID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `blog_members`
--

INSERT INTO `blog_members` (`memberID`, `username`, `password`, `email`) VALUES
(1, '@dmin_thinktutors', '$2y$10$Z0m7WwZSXf0796y6VSvHIuIB6Gy.v7piygDhPYwxawSB/TXpkYPk2', 'puneetjaini@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `blog_posts_seo`
--

CREATE TABLE IF NOT EXISTS `blog_posts_seo` (
  `postID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `postTitle` varchar(255) DEFAULT NULL,
  `postSlug` varchar(255) DEFAULT NULL,
  `postMetaTitle` text,
  `postMetaDesc` text,
  `postMetaKey` text,
  `postCont` text,
  `featuredImage` text,
  `postDate` datetime DEFAULT NULL,
  PRIMARY KEY (`postID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `blog_posts_seo`
--

INSERT INTO `blog_posts_seo` (`postID`, `postTitle`, `postSlug`, `postMetaTitle`, `postMetaDesc`, `postMetaKey`, `postCont`, `featuredImage`, `postDate`) VALUES
(9, 'The Elon Musk Master Plan', 'the-elon-musk-master-plan', 'elon musk', 'Elon Musk, the scientist and business mastermind behind Space-X, a private space travel company, Tesla Cars and Paypal has revealed his new â€˜master planâ€™ for Tesla Motors.', '', '<p>Love physics? Considering engineering at university? We are sure you have heard of Tesla Motors that has taken the UK media by storm this year, but have you heard of the secret master plan and the brains behind the brand?</p>\r\n<p>Elon Musk, the scientist and business mastermind behind Space-X, a private space travel company, Tesla Cars and Paypal has revealed his new &lsquo;master plan&rsquo; for Tesla Motors.</p>\r\n<p>In August 2006, Elon Musk revealed his &lsquo;secret Tesla Motors master plan&rsquo;.</p>\r\n<h3>In short the master plan was:</h3>\r\n<ol>\r\n<li>Build sports car</li>\r\n<li>Use that money to build an affordable car</li>\r\n<li>Use that money to build an even more affordable car</li>\r\n<li>While doing above, also provide zero emission electric power generation options</li>\r\n<li>Don&rsquo;t tell anyone</li>\r\n</ol>\r\n<p>In April 2016, the master plan was accomplished following the release of the Tesla 3 fully electric car which amounted to 115,000 pre orders after the initial press event.</p>\r\n<p>On July 20th, Tesla announced Master Plan, Part Deux. The new master plan consists of four stages:</p>\r\n<p><strong>Create stunning solar roofs with seamlessly integrated battery storage</strong></p>\r\n<p>Musk hopes Tesla will acquire another of his companies, Solar City. He wants to increase the adoption of solar panels onto cars with &lsquo;one ordering experience, one installation, one service contact, one phone app&rsquo;<strong><br /></strong></p>\r\n<p><strong>Expand the electric vehicle product line to address all major segments</strong></p>\r\n<p>Here, Musk outlines the vision to create autonomous high passenger-density transport and heavy-duty trucks to increase safety and reduce traffic congestion.</p>\r\n<p><strong>Develop a self-driving capability that is 10X safer than manual via massive fleet learning</strong></p>\r\n<p>Self driving busses with &lsquo;Fixed summon buttons at existing bus stops would serve those who don&rsquo;t have a phone&rsquo; making this option available for everyone. He envisages bus drivers transitioning into &lsquo;fleet managers&rsquo; for the self-driving busses.</p>\r\n<p><strong>Enable your car to make money for you when you aren&rsquo;t using it</strong></p>\r\n<p>The final point reaffirmed Tesla&rsquo;s plan to create a fully automatous mode for the entire Tesla fleet. This would allow a car to act as a &lsquo;taxi&rsquo; when not in personal use, thus &lsquo;dramatically lowers the true cost of ownership to the point where almost anyone could own a Tesla&rsquo;.</p>\r\n<p>Competition for Black Cabs and Uber?</p>\r\n<p><strong>At Think Tutors, we are fully behind Musk and his &lsquo;Master Plan Part Deux! The Tesla Model 3 is due to be realised in 2017, so in the mean check out tesla.com/en_GB/blog for updates on Elon&rsquo;s &lsquo;master plan&rsquo;!</strong></p>\r\n<p><strong>Sources</strong></p>\r\n<ol>\r\n<li>https://www.tesla.com/en_GB/blog<strong><br /></strong></li>\r\n</ol>', '../uploads/universities.png', '2018-01-10 12:24:49'),
(10, 'Cyber Savvy: Schools to Offer Cyber Security in England', 'cyber-savvy-schools-to-offer-cyber-security-in-england', 'cyber security', 'Hacking and cyber security has been a hot topic of 2016/2017 leading up to the US presidential election. The UK government now wants to introduce cyber security to schools in England.', '', '<p>Hacking and cyber security has been a hot topic of 2016/2017 leading up to the US presidential election. The UK government now wants to introduce cyber security to schools in England, in the hope of training up a new breed of cyber experts to defend against future attacks.</p>\r\n<p>Pupils aged over 14 will spend up to four hours a week on the subject with the chance of working on real world problems during work experience. Cyber security employs 58,000 people in the UK but there is a lack of experts with the right skills to fill the growing demand in this sector.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', '../uploads/bespoke-1.png', '2018-01-10 12:35:54'),
(11, 'Biofuels: Bursting the BioBubble', 'biofuels-bursting-the-biobubble', 'biofuels', 'In this article we discuss the role of biofuels and hype surrounding new and innovative biofuels.', '', '<p>Whether you study geography, geology or a natural science, climate change and associated amelioration methods form a large part of your GCSE or A level.</p>\r\n<p><strong>In this article we discuss the role of biofuels and hype surrounding new and innovative biofuels.</strong></p>\r\n<h2>Climate change</h2>\r\n<p>The Paris Agreement within the framework of United Nations Framework Convention on Climate Change sealed in 2015 set out to:</p>\r\n<ol>\r\n<li>Hold the increase in the global average temperature to well below 2 &deg;C above pre-industrial levels and to pursue efforts to limit the temperature increase to 1.5 &deg;C above pre-industrial levels, recognising that this would significantly reduce the risks and impacts of climate change;</li>\r\n<li>Increase the ability to adapt to the adverse impacts of climate change and foster climate resilience and low greenhouse gas emissions development, in a manner that does not threaten food production;</li>\r\n<li>Make finance flows consistent with a pathway towards low greenhouse gas emissions and climate-resilient development.</li>\r\n</ol>\r\n<h2>Biofuels</h2>\r\n<p>Biofuels, being energy derived from organic material have been at the forefront of scientific research and development since 2008, but have recently raised some serious questions about the ability to create a drastically greener climate.</p>\r\n<p>Norway is set to ban conventional cars and hybrid electric vehicles by 2025 in a bid to reduce carbon dioxide emissions. The European Union Commission has recently proposed the phase out of food-based biofuels after 2020.</p>\r\n<p><strong>What does this mean for the future of biofuels? Do biofuels have a place in the world energy mix?</strong></p>\r\n<h2>Advantages</h2>\r\n<h3>Cost benefit</h3>\r\n<p>Biofuels cost the same to consumers as regular fuel. However, they are deemed to be cleaner fuels and produce few emissions when combusted. With an increase demand into biofuels, especially for the growing use in air transportation, the cost to the consumer could be driven down creating a small drain on the wallet.</p>\r\n<h3>Easy to source</h3>\r\n<p>Regular hydrocarbons (petrol, diesel, kerosene) is refined from crude oil, a non-renewable resource. Whilst current hydrocarbon exploration and production is ongoing, increasing costs combined with a low oil price and pressure from climate change regulation will aid in the transition to greener fuels. Biofuels are far easier to source and consist of plant based feedstock, manure and more recently niche biofuels including spent coffee grounds (see: bio-bean.com).</p>\r\n<h3>Renewable</h3>\r\n<p>Fossil fuels derived from crude oil will eventually diminish as a high percentage of the known reserves have been exploited and the hydrocarbons take millions of years to form. Plants, coffee grounds and associated biomass can be constantly produced over a short time period, creating a highly renewable resource.</p>\r\n<h3>Reduce Greenhouse Gases</h3>\r\n<p>Fossil fuels, when combusted produce large amounts of greenhouse gases (carbon dioxide, methane and sulphur dioxide) which act as an atmospheric blanket surrounding the earth. This blanket traps sunlight causing anthropogenic global warming. Biofuels can reduce emissions by 65% when burnt!</p>\r\n<h3>Economic Security</h3>\r\n<p>Geology is the governing factor on a country&rsquo;s crude oil reserves. Some countries, noticeably, have far less reserves than others. If a country shifts towards biofuels, more jobs will be created in a growing industry thus reducing a national dependence on diminishing crude oil reserves.</p>\r\n<h2>Disadvantages</h2>\r\n<h3>High Cost of Production</h3>\r\n<p>With countries, such as Norway and businesses such as Tesla leapfrogging the use of biofuels and investing heavily into greener energy, less capital investment is being allocated for biofuels which reduces the short term scalability.</p>\r\n<h2>Monoculture</h2>\r\n<p>Monoculture refers to practice of producing same crops year after year, rather than producing various crops through a farmer&rsquo;s fields over time. This might be economically attractive to farmers, but without crop rotation, nutrients in the soil are not replenished, thus leading to less fertile land.</p>\r\n<h3>Use of Fertilisers</h3>\r\n<p>Biofuels are produced from crops so inevitably fertilisers are used to enchanced growth. The downside of using fertilisers is that they can have harmful effects on surrounding environment and may cause water pollution. Fertilisers contain nitrogen and phosphorus which if washed into water bodies can cause severe eutrophication.</p>\r\n<h3>Shortage of Food</h3>\r\n<p>Biofuels extracted from plants and crops take up large areas of agricultural space which reduced land available for the growth of food. Without regulation, an increase in cash crop growth could be seen which would create pressure on food security in developing countries and drive up consumer prices.</p>\r\n<h3>Industrial Pollution</h3>\r\n<p>The carbon footprint of biofuels is less than the traditional forms of fuel when combusted. However, the production of biomass and associated fuels are largely dependent on large amounts of water and oil. The dependence on these resources might create a far smaller dent in climate change and sustainability that once thought.</p>\r\n<h3>Water Use</h3>\r\n<p>Large quantities of water are required to irrigate the biofuel crops and it may impose strain on local and regional water resources, if not managed wisely. The Prickly Pear Cactus is currently being tested as a feedstock for biofuel. Being a cactus, it requires far less water than convention crops, so feedstocks like this could ameliorate this problem.</p>\r\n<p><strong>Have your say! Are biofuels the answer to climate change? Leave your answers in the comment section below!</strong></p>\r\n<p><strong>Sources</strong></p>\r\n<ol>\r\n<li>UNFCCC (2016). The Paris Agreement Unfccc.int. Available at: http://unfccc.int/paris_agreement/items/9485.php</li>\r\n<li>bio-bean. (2016). Biofuels &ndash; bio-bean. Available at: http://www.bio-bean.com/products/</li>\r\n<li>Conserve-Energy-Future. (2013). Advantages and Disadvantages of Biofuels &ndash; Conserve Energy Future. Available at: http://www.conserve-energy-future.com/advantages-and-disadvantages-of-biofuels.php</li>\r\n</ol>\r\n<p>&nbsp;</p>', '../uploads/alevels.png', '2018-01-10 12:49:08'),
(12, 'Tutors May Soon Legally Require a DBS', 'tutors-may-soon-legally-require-a-dbs', 'DBS', 'There are now calls to close these loopholes and hold tutors in the same regard as teachers who must legally undergo background checks.', '', '<p>Some of you may have seen the article published by the BBC last week regarding criminal background checks for private tutors, which at Think Tutors is extremely important to us. Unfortunately due to a loophole in UK law, it is not a legal requirement for private tutors to have a criminal background check, also known as a DBS certificate, which according to the NSPCC could be putting children at risk.&nbsp; There are now calls to close these loopholes and hold tutors in the same regard as teachers who must legally undergo background checks.</p>\r\n<p>At Think Tutors we put students and parents first and will always ensure that our tutors have have an up-to-date DBS certificate.&nbsp; We don&rsquo;t stop there, we personally interview tutors, verify their identify and obtain references in order to ensure piece of mind and bespoke, first class tutoring.</p>\r\n<p><strong>You can find the full article here.</strong></p>', '../uploads/universities.png', '2018-01-10 12:51:16');

-- --------------------------------------------------------

--
-- Table structure for table `blog_post_cats`
--

CREATE TABLE IF NOT EXISTS `blog_post_cats` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `postID` int(11) DEFAULT NULL,
  `catID` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=135 ;

--
-- Dumping data for table `blog_post_cats`
--

INSERT INTO `blog_post_cats` (`id`, `postID`, `catID`) VALUES
(132, 11, 6),
(130, 10, 6),
(134, 12, 6),
(128, 9, 6);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT '0',
  `email` varchar(255) DEFAULT '0',
  `phone` varchar(255) DEFAULT '0',
  `comments` varchar(255) DEFAULT '0',
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tutor_signup`
--

CREATE TABLE IF NOT EXISTS `tutor_signup` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT '0',
  `first_name` varchar(255) DEFAULT '0',
  `last_name` varchar(255) DEFAULT '0',
  `date_of_birth` varchar(255) DEFAULT '0',
  `email_address` varchar(255) DEFAULT '0',
  `contact_number` varchar(255) DEFAULT '0',
  `address_line_1` varchar(255) DEFAULT '0',
  `address_line_2` varchar(255) DEFAULT '0',
  `town` varchar(255) DEFAULT '0',
  `county` varchar(255) DEFAULT '0',
  `post_code` varchar(255) DEFAULT '0',
  `country` varchar(255) DEFAULT '0',
  `employment_status` varchar(255) DEFAULT '0',
  `personal_transport` varchar(255) DEFAULT '0',
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `vacancies`
--

CREATE TABLE IF NOT EXISTS `vacancies` (
  `vacancies_Id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `vacancies_Title` varchar(255) DEFAULT NULL,
  `vacancies_Location` varchar(255) DEFAULT NULL,
  `vacancies_Slug` varchar(255) DEFAULT NULL,
  `vacancies_Image` text,
  `vacancies_Content` varchar(255) DEFAULT NULL,
  `vacancies_Start` varchar(255) DEFAULT NULL,
  `vacancies_Duration` varchar(255) DEFAULT NULL,
  `vacancies_Salary` varchar(255) DEFAULT NULL,
  `vacancies_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`vacancies_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `vacancies`
--

INSERT INTO `vacancies` (`vacancies_Id`, `vacancies_Title`, `vacancies_Location`, `vacancies_Slug`, `vacancies_Image`, `vacancies_Content`, `vacancies_Start`, `vacancies_Duration`, `vacancies_Salary`, `vacancies_Date`) VALUES
(1, 'Vacancies 1', 'Monaco', 'vacancies-1', '../uploads/monaco.jpeg', 'A Level physics and maths, residential (35 hours p/w)', '5th July 2017', '2 months', 'Â£18,000', '2018-01-10 13:02:06'),
(2, 'Vacancies 2', 'Virginia Water, Berkshire', 'vacancies-2', '../uploads/berkshire.jpeg', 'Chemistry A-level', '18th June 2017', '3 months', 'Â£80 p/h', '2018-01-10 13:00:16'),
(5, 'Vacancies 3', 'Virginia Water, Berkshire', 'vacancies-3', '../uploads/berkshire.jpeg', 'Chemistry A-level', '18th June 2017', '3 months', 'Â£80 p/h', '2018-01-16 15:17:54');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

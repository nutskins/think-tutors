<?php $title= "Tuition that enables our students to reach their full potential | Think Tutors"; ?>
<?php $metadescription= "Our tuition is fully tailored to the needs of each student to improve confidence, deliver high results and obtain success in school or university entry.";?>
<?php include 'header.php' ?>
<main>
		<section>
			<div class="banner" style="background-image:url(images/1-min.png);">
			</div>
		</section>
		<section>
				<div class="wrapper">
					<div>
							
					<p style="text-align:justify">Our mission is simple. To deliver bespoke tuition services that enable our students to reach their full potential. Providing unrivalled private tuition across the UK and internationally, at Think Tutors we’re proud to help our students reach academic excellence.</p>
					<p style="text-align:justify">We know that no two students are the same, so why should our tuition be? That’s why we tailor our services to meet the unique needs of each student we work with, whether it be to improve confidence, deliver high results or obtain success in school or university entry.</p>
					<p style="text-align:justify">At Think Tutors, we provide the flexibility to work around your schedule, providing tuition on an hourly basis or longer term as part of a residential placement in the UK or internationally.</p>
					</div>
						<div class="clear"></div>
					</div>
			</div>
		</section>
		<section>
			<div class="light-color">
				<div class="wrapper">
					<div class="both_pnl">
						<div class="right">
							<h2>Bespoke</h2>
							<p>We provide fully vetted, handpicked professional tutors, that match the individual needs and circumstances of each of our students. We pride ourselves on forging close relationships with families, schools and our tutors, and maintain frequent contact throughout the duration of the tuition. All of our tutors attended top universities so they are well equipped to guide our students through the entire education process, from the 11+ pre-test to postgraduate degrees.</p>
							<p>Located across London, Surrey, and Berkshire and further afield in the UK, Europe and the rest of the world, our tutors really are the best in their fields.</p>
						</div>
						<div class="left">
							<img class="hide_img" src="images/bespoke_right_img.jpg" alt="">
						</div>
						<div class="clear"></div>
					</div>
				</div>
			</div>
		</section>
		<section>
			<div class="three_sec">
				<div class="wrapper">
					<div class="section">
						<div class="three_pnl">
							<img src="images/school.png" alt="school">
							<h3>School</h3>
							<p>Our tailored private tuition covers all secondary and higher education subjects from 11+ pre-test to Common Entrance, GCSE and A-level. Our expert private tutors ensure they’re up to speed with the latest specifications, amends and exam board requirements to streamline the tuition and deliver the best possible results for our students along the way.<br><br><br></p>
							<div class="read_more">
								<a href="our-services.php">Read More</a>
							</div>
						</div>
						<div class="three_pnl">
							<img src="images/university.png" alt="university">
							<h3>University</h3>
								<p>Our university service can be tailored to meet your requirements. We work with students to provide both consistent,  regular tuition throughout the academic year and to give  intensive, focused guidance on a specific assignment or examinations. Our university tutors are academic elites in their respective fields. We guide our students to come up with a solution that truly works for them, around their timetable and to meet their individual requirements.</p>
								<div class="read_more">
								<a href="our-services.php">Read More</a>
							</div>
						</div>
						<div class="three_pnl">
							<img src="images/international.png" alt="international">
							<h3>International</h3>
							<p>Think Tutors delivers tuition throughout the UK and around the world. Many of our clients travel and work internationally and utilise our global tuition services to provide full time tuition whilst on the move. Our tutors have worked around the world and are used to planning tuition to meet changing schedules.<br><br><br><br><br></p>
							<div class="read_more">
								<a href="international-tutoring.php">Read More</a>
							</div>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			</div>
		</section>
		<section>
			<div class="light-color">
				<div class="wrapper">
					<div class="both_pnl">
						<div class="right">
							<img class="hide_img" src="images/alevels.png" alt="">
						</div>
						<div class="left">
							<h2>Academic Excellence</h2>
							<p>Over the years we have built a strong and extensive network of highly talented, exceptional tutors who can offer support at all levels of the educational system. All of our tutors have obtained an excellent Bachelor's degree as a  minimum, with the majority having pursued postgraduate degrees in their specialist fields. As a result,  they know first-hand what it takes to strive for academic excellence. Our tutors are quick and skilled at identifying gaps in the students’ understanding and focus on resolving these to deliver high results.</p>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			</div>
		</section>
		<section>
			<div class="review_slide">
				<div class="wrapper">
					  <div class="swiper-container">
						<div class="swiper-wrapper">
						  <div class="swiper-slide">
							<div class="review">
								<img src="images/ttquoteblue.svg" alt="" >
								<h4> <br>A stroke of brilliance.</h4>
								<h5>Chris, father of BSc Geography dissertation student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquoteblue.svg" alt="" >
								<h4>Thank you once again for the support<br> and guidance that you and Sebastian gave to our students,<br> it definitely did have a positive impact.</h4>
								<h5>Head of Sixth Form</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquoteblue.svg" alt="" >
								<h4>Our tutor was exceptional, showing the ability to convey<br> the course content in a simple yet concise manner,<br> making it easy to pick up and remember.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquoteblue.svg" alt="" >
								<h4>I now feel confident to take my exams and would like<br> to thank them for their patience and commitment<br> towards achieving my goal.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquoteblue.svg" alt="" >
								<h4>Very quick to reply to our initial search for a geography<br> tutor. They clearly have excellent knowledge of the<br> subject and the current curriculum.</h4>
								<h5>Andrea, mother of A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquoteblue.svg" alt="" >
								<h4>James knows a lot about University<br> testing procedures and was able to give<br> advice on a difficult course.</h4>
								<h5>Rhea, mother of BSc Astro Geochemistry student</h5>
							</div>
							</div>
						</div>
					<div class="swiper-pagination"></div>
				  </div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<section>
			<div class="owner_detail">
				<div class="wrapper">
					<div class="panel">
						<div class="left_image"><img src="images/james.png" alt=""></div>
						<div class="middle_content">
							<h2>James & Neil</h2>
							<p>James and Neil established Think Tutors in 2014 and have over 10 years of experience tutoring students from 11+ pre-test to postgraduate degrees. They both obtained BSc (Hons) degrees from two leading UK universities before going on to further their education by undertaking MSc degrees in their respective fields. James and Neil work closely with every student/family and are contactable 24 hours a day, 7 days a week.</p>
						</div>
						<div class="right_image"><img src="images/neil.png" alt=""></div>
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<?php include 'footer_contact-form.php';?>
	</main>

<?php include 'footer.php' ?>
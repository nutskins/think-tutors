<?php $title= "خدماتنا | Think Tutors"; ?>
<?php $metadescription= "Our services are fully tailored to our students’ needs whether on an hourly basis or a long-term residential placement in the UK and overseas.";?>
<?php $page = "services"; include 'arheader.php' ?>
<main>
	<section>
		<div class="banner" style=" background-image:url(images/tutor.jpg)">
		</div>
	</section>
	<section>
		<div class="int_content">
			<div class="wrapper">
				<h1>خدماتنا</h1>
				<div class="left_pnl extra">
					<p>نحن نعلم أنه لا يوجد طالبان متشابهان ، فلماذا يجب أن تكون دراستهما؟ تم تصميم خدماتنا بشكل كامل لتلبية الاحتياجات الفردية لكل طالب من طلابنا سواء على مدار الساعة أو في مكان سكني طويل الأجل في المملكة المتحدة أو في الخارج.</p>
					<p>يتمتع مدرسينا بخبرة دولية استثنائية ، حيث عملوا في جميع أنحاء العالم من موسكو إلى سنغافورة ودبي إلى هونغ كونغ وعلى اليخوت الخاصة في موناكو. ونتيجة لذلك ، فقد اعتادوا على تخطيط الرسوم الدراسية لتغيير الجداول الزمنية. للحصول على خدماتنا الدولية ، يرجى الاطلاع على صفحتنا الدولية المخصصة.</p>
					<p>يتم اختيار جميع المدرسين لدينا بعناية ، وفحصهم وإجراء مقابلات معهم من قبل كل من نيل وجيمس لضمان مشاركة قيمنا وشغفنا بالتعلم. جميع المعلمين لدينا يحملون درجة البكالوريوس ممتازة مع معظم الحصول على مؤهلات الدراسات العليا بدءا من الماجستير إلى الدكتوراه و PGCSEs  كجزء من إجراءات التدقيق لدينا ، يجب أن يكون جميع معلمينا لدينا حاصلين على شهادة DBS محسّنة محدثة وأن يقدموا مراجع من العملاء السابقين.</p>
					<h2>المدارس</h2>
					<p>بداية من مرحلة  11+ ومن الاختبارات المسبقة ، يمكن لمدرسينا توجيه الطلاب من خلال عملية التعلم وتقديم المشورة للآباء حول إجراءات التقديم للمدارس في جميع أنحاء البلاد. ومن ثم يمكن إشراك عائلاتنا من المعلمين في كل خطوة على الطريق من 11+ و 13+ إلى امتحان الدخول المشترك ، وصولاً إلى GCSEs ، A-level و BACCalaureate الدولية.</p>
					<p>يعمل مدرسينا بشكل مستمر على ضمان مسايرة أحدث المواصفات والتغييرات ومتطلبات لوحة الاختبار. يضمن ذلك للطلاب التعلم بفعالية وكفاءة للتميز في يوم الامتحان</p>
					<p>نحن فخورون بالعمل عن كثب مع المدارس ، حيث نقدم للمعلمين خلال يوم التقويم وحتى المساء المبكر العمل على أساس فردي وجماعي. تستخدم المدارس معلمينا لجلسات المراجعة المسبقة المكثفة لمعالجة أي نقاط ضعف للموضوع من الفنون إلى العلوم ، ولتحسين تقنيات الاختبار والثقة.</p>
					<p>هل تعلم أننا نقدم أيضًا رسومًا دراسية عبر Skype؟ مثالية لمن هم في المناطق النائية أو ربما مع طفل بعيد عن المدرسة الداخلية ويحتاجون إلى القليل من المساعدة الإضافية ، تسمح دروس Skype الخاصة بنا لمعلمينا بمساعدة الطلاب في جميع المواقع.</p>
					<p>يشمل مدرسينا مجموعة واسعة من المواضيع بما في ذلك ، على سبيل المثال لا الحصر ،الرياضيات والأدب الإنجليزي واللغة والجغرافيا والكيمياء والبيولوجيا والفيزياء.</p>
					<h2>امتحانات القبول</h2>
					<p>لدينا مجموعة من المعلمين ذوي الخبرة العالية لديهم ثروة من المعرفة في توجيه العقول الشابة من خلال مجموعة من امتحانات القبول. من امتحانات القبول في جامعة أكسفورد وكامبريدج قبل 11 يومًا ، يمكن لمدرسينا أن يكونوا موجودين في كل خطوة على الطريق لمساعدة طفلك خلال هذه اللحظات الأساسية.</p>
					<p>يتمتع المعلمون لدينا بخبرة عالية في توجيه الطلاب من خلال جميع الاختبارات ، وبناء الثقة والمعرفة على طول الطريق.</p>
					<p>يمكن أن تبدو تطبيقات الجامعة شاقة ، لكن يمكن لمعلمينا وخريجيها السابقين المساعدة في اختيار الدورة التدريبية ، والبيانات الشخصية ، واختبارات القبول في Oxbridge والامتحانات الخاصة بالدورة LNAT ، و BMAT ، و STEP ، و TSA.</p>
					<h2>جامعة</h2>
					<p>بالنسبة لطلاب الجامعات ، يمتلك Think Tutors فريقًا متخصصًا من المدرسين القادرين على توجيه الطلاب. فمنذ بداية أول عام على مستوى البكالوريوس حتى نهاية الدراسات العليا ، يستطيع المعلمون الخبراء العمل مع الطلاب لإيجاد حل يعمل حول طاولة وقتهم. نحن قادرون على تقديم الرسوم الدراسية لأي موضوع ويمكن أن نقدم التوجيه مع مراجعة الامتحان والتدقيق اللغوي للبرامج الدراسية والأطروحات.</p>				
					</div>
				<div class="right_pnl">
					<div class="color_box">
						<h3>جميع مجالات الموضوع<br>بما في ذلك</h3>
						<ul>
							<li>قبل الاختبار  11+</li>
							<li>مدخل مشترك</li>
							<li>GCSE / IGCSE</li>
							<li>AS / A2 المستوى</li>
							<li>البكالوريا الدولية</li>
							<li>تطبيقات الجامعة بما في ذلك Oxbridge</li>
							<li>المرحلة الجامعية</li>
							<li>دراسات عليا</li>
							<li>الامتحانات المهنية (القانون ، الطب)</li>
						</ul>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</section>
	<section>
		<div class="light-color">
			<div class="wrapper">
				<div class="both_pnl">
					<div class="left">
						<h2>معدلات</h2>
						<p>تبدأ أسعارنا من 80 جنيهاً إسترلينياً في الساعة على أساس الرسوم الدراسية من شخص إلى شخص في المملكة المتحدة. سيتم مناقشة الإقامات السكنية طويلة الأجل على أساس فردي. يرجى الاطلاع على صفحتنا الدولية لمعدلات الرسوم الدراسية في جميع أنحاء العالم. نحن نرسل الفاتورة في نهاية كل شهر ولا نتقاضى أي رسوم مقدمة لخدماتنا.</p>
					</div>
					<div class="right">
						<img src="images/4sm.jpg" alt="">
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>	
	</section>
	<section>
	<div class="process_pnl">
			<div class="wrapper">
				<h2>العملية</h2>
				<div class="pro_pnl">
				<div class="pro_three" data-aos="fade-up" data-aos-duration="1200">
					<img src="images/number-03.png" alt="">
					<p>3. يتم إرسال مجموعة مختارة من ملفات تعريف المعلم إليك ، مع تحديد المؤهلات ، والتوظيف السابق ، والشهادات ، وتأكيد DBS المحسن المحدث وسيرتك الذاتية.</p>
				</div>
				<div class="pro_three" data-aos="fade-up" data-aos-duration="1100">
					<img src="images/number-02.png" alt="">
					<p>2. يلتقي جيمس ونيل مع مجموعة مختارة بعناية من المدرسين لمناقشة أهداف الطلاب وتوقعاتهم.</p>
				</div>
				<div class="pro_three" data-aos="fade-up" data-aos-duration="1000">
					<img src="images/number-01.png" alt="">
					<p>1. نناقش احتياجات الطلاب وتطلعاتهم الأكاديمية ، إما عبر الهاتف أو Skype أو وجهًا لوجه ، بغض النظر عن الموقع.</p>
				</div>
				<div class="clear"></div>
				<div class="pro_three" data-aos="fade-up" data-aos-duration="1500">
					<img src="images/number-06.png" alt="">
					<p>6. يبقى جيمس ونيل على اتصال وثيق مع كل من المعلم والعميل طوال فترة الدراسة ، وهما متاحان على مدار 24 ساعة في اليوم ، 7 أيام في الأسبوع.</p>
				</div>
				<div class="pro_three" data-aos="fade-up" data-aos-duration="1400">
					<img src="images/number-05.png" alt="">
					<p>5. بمجرد تأكيد المعلم يتم إنشاء خطة تعليمية مخصصة وتفصيلية مع معالم شهرية محددة ويتم الإبلاغ عنها.</p>
				</div>
				<div class="pro_three" data-aos="fade-up" data-aos-duration="1300">
					<img src="images/number-04.png" alt="">
					<p>4. إذا رغبت ، يمكن ترتيب اجتماع تمهيدي وجهًا لوجه أو مكالمة جماعية.</p>
				</div>
				<div class="clear"></div>
				</div>
			</div>
		</div>
	</section>
	<section dir="ltr">
		<div class="review_slide blue">
				<div class="wrapper">
					  <div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Our tutor was exceptional, showing the ability to convey<br>the course content in a simple yet concise manner,<br>making it easy to pick up and remember.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>A stroke of brilliance.</h4>
								<h5>Chris, father of BSc Geography dissertation student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Thank you once again for the support<br>and guidance that you and Sebastian gave to our students,<br>it definitely did have a positive impact.</h4>
								<h5>Head of Sixth Form</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>I now feel confident to take my exams and would like<br>to thank them for their patience and commitment<br>towards achieving my goal.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Very quick to reply to our initial search for a geography<br>tutor. They clearly have excellent knowledge of the<br>subject and the current curriculum.</h4>
								<h5>Andrea, mother of A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>James knows a lot about University<br>testing procedures and was able to give<br>advice on a difficult course.</h4>
								<h5>Rhea, mother of BSc Astro Geochemistry student</h5>
							</div>
							</div>
						</div>
					<div class="swiper-pagination"></div>
				  </div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<?php include 'arfooter_contact-form.php';?>
	</main>
<?php include 'artestimonial.php';?>
<?php include 'arfooter.php' ?>
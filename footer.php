<footer>
<link rel=“stylesheet” href=“https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css“>
<link href="css/responsive.css" rel="stylesheet" type="text/css">
   <div class=“wrapper”>
       <div class=“copy”>
        <p>
       <div class="right_pnl footermiddle">
       <a href="https://addressbook.tatler.com/united-kingdom/london/education/think-tutors" target="blank"><img class="tatlerlogo" src="images/tatlerlogo.png" alt="Tatler Address Book | Corporate Member"></a>
      <img class="tutorassociationlogo" src="images/TACMLogo.png" alt="The Tutor's Association | Corporate Member">
      <a href="https://spears500.com/profile/SPAC2051/think-tutors/" target="blank"><img class="spearslogo" src="images/spearslogo.png" alt="Spears 500 | Top Recommended"></a>
</div>
			<div class="right_pnl">
      <ul>
			<li><a href="https://www.facebook.com/thinktutors/" target="_blank"><i class="fa fa-facebook-square" style="font-size:18px;" aria-hidden="true"></i> Think Tutors &zwnj; &zwnj;</a></p>
			<li><a href="https://www.instagram.com/thinktutors/" target="_blank"><i class="fa fa-instagram" style="font-size:18px;" aria-hidden="true"></i> thinktutors &zwnj; &zwnj;</a></li>
      <li><a href="https://twitter.com/think_tutors" target="_blank"><i class="fa fa-twitter" style="font-size:18px;" aria-hidden="true"></i> @Think_Tutors &zwnj; &zwnj;</a></li>
      <li><a href="https://www.linkedin.com/company/think-tutors-limited/" target="_blank"><i class="fa fa-linkedin" style="font-size:18px;" aria-hidden="true"></i> Think-Tutors-Limited &zwnj; &zwnj;</a></li>
      <li><a href="https://thinktutors.com/data-privacy-policy.php" target="_blank"><i style="font-size:18px;" aria-hidden="true"></i> Data Privacy Policy &zwnj; &zwnj;</a></li>
      <li><a href="https://thinktutors.com/safeguarding-children-policy.php" target="_blank"><i style="font-size:18px;" aria-hidden="true"></i> Safeguarding Children Policy &zwnj; &zwnj;</a></li>

            </ul>
            <p>© <?php echo date("Y"); ?> Think Tutors All rights reserved.</p>
            </div>
        </div>
        </div>
   </div>
</footer>
<div class="back-to-top"><i class="fa fa-angle-up" aria-hidden="true"></i></div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/plugins/ScrollToPlugin.min.js"></script>
<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>
<script type='text/javascript' src='https://maps.google.com/maps/api/js?key=AIzaSyDdl5-iNRjYLxsM6eYjYjspvDfathb7GGo'></script>
<script type="text/javascript" src="js/intlTelInput.js"></script>
<script type="text/javascript" src="js/mobile-menu.js"></script>  
<script type="text/javascript" src="js/equalheight.js"></script>
<script type="text/javascript" src="https://imagesloaded.desandro.com/imagesloaded.pkgd.js"></script>
<script type="text/javascript" src="js/swiper.min.js"></script>
<script src="https://rawgit.com/desandro/masonry/v2.1.08/jquery.masonry.js" type="application/javascript"></script>
<script type="text/javascript" src="js/jssocials.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="js/parsley.js"></script>
<script>
	/*
	$(function(){
		var $window = $(window);		//Window object
		var scrollTime = 0.3;			//Scroll time
		var scrollDistance = 170;		//Distance. Use smaller value for shorter scroll and greater value for longer scroll
		$window.on("mousewheel DOMMouseScroll", function(event){
			event.preventDefault();	
			var delta = event.originalEvent.wheelDelta/120 || -event.originalEvent.detail/3;
			var scrollTop = $window.scrollTop();
			var finalScroll = scrollTop - parseInt(delta*scrollDistance);
			TweenMax.to($window, scrollTime, {
			scrollTo : { y: finalScroll, autoKill:true },
				ease: Power1.easeOut,	//For more easing functions see https://api.greensock.com/js/com/greensock/easing/package-detail.html
				autoKill: true,
				overwrite: 5							
			});					
		});
	});*/
</script>
<script type='text/javascript'>
	$(function() {
	  $('.fix a[href*=#]').on('click', function(e) {
		e.preventDefault();
		$('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top - 0}, 500, 'linear');
	  });
	});
</script>
<script type='text/javascript'>
	$('.blog_panl').imagesLoaded( function() {
		var $container = $('.blog_panl.iscotop .blog_left .blog_left_inner').masonry({
			itemSelector: '.blog_panl.iscotop .blog_left .blog_two',
		});
	});
</script>

<script>
function myFunction() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}
</script>

<script type='text/javascript'>
	function init_map(){
		var image = 'images/map-marker.png';
		
		var myOptions = {
				zoom:13,
				center:new google.maps.LatLng(51.5101385,-0.147421),
				icon: image,
				styles: [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#2d5068"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#2d5068"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
],
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			
			map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
			marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(51.5101385,-0.147421)});
			google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});
			infowindow.open(map,marker);}
			google.maps.event.addDomListener(window, 'load', init_map);
</script>
<script type="text/javascript">
	AOS.init({
		easing: 'ease-out-back',
		duration: 1000
	});
	if ($('.back-to-top').length) {
    var scrollTrigger = 100, // px
        backToTop = function () {
            var scrollTop = $(window).scrollTop();
            if (scrollTop > scrollTrigger) {
                $('.back-to-top').addClass('show');
            } else {
                $('.back-to-top').removeClass('show');
            }
        };
    backToTop();
    $(window).on('scroll', function () {
        backToTop();
    });
    $('.back-to-top').on('click', function (e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: 0
        }, 700);
    });
}
</script>
<script type="text/javascript">
    $("#phone").intlTelInput({
      allowDropdown: true,
      autoHideDialCode: false,
      autoPlaceholder: "on",
      formatOnDisplay: true,
      nationalMode: false, 
    });
</script>
<script type="text/javascript">
    var swiper = new Swiper('.review_slide .swiper-container', {
		loop: true,
		autoplay: true,
		slidesPerView: 1,
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
		},
    });
	var swiper = new Swiper('.testi_reviews .swiper-container', {
	loop: true,
		autoplay: true,
		slidesPerView: 1,
	  pagination: {
		el: '.swiper-pagination',
		  clickable: true,
	  },
	});
</script>
<script type="text/javascript">
$(window).scroll(function() {
	if ($(this).scrollTop() > 30){  
		$('.menu_section').addClass("sticky");
	  }
	  else{
		$('.menu_section').removeClass("sticky");
	  }
	if ($(this).scrollTop() > 200){  
		$('.menu_section').addClass("hide-header");
	  }
	  else{
		$('.menu_section').removeClass("hide-header");
	  }
});
</script>
<script type="text/javascript">
	$(window).load(function(){
		equalheight('.review_slide .review h4');
	});
	$(window).resize(function(){
		equalheight('.review_slide .review h4');
	});
</script>
<script type="text/javascript">
	$("#share").jsSocials({
		shares: ["facebook", "twitter", "googleplus", "pinterest", "linkedin", "xing", "email"],
		showLabel: false,
	    showCount: false,
	});
</script>
<script>
	$( "#datepicker" ).datepicker({
    yearRange: '1918:2018',
    dateFormat: 'dd-mm-yy',
    changeMonth: true,
    changeYear: true
});
  </script>
 <script type="text/javascript">
	$(function () {
	  var $sections = $('.form-section');
	
	  function navigateTo(index) {
		// Mark the current section with the class 'current'
		$sections
		  .removeClass('current')
		  .eq(index)
			.addClass('current');
		// Show only the navigation buttons that make sense for the current section:
		$('.form-navigation .previous').toggle(index > 0);
		var atTheEnd = index >= $sections.length - 1;
		$('.form-navigation .next').toggle(!atTheEnd);
		$('.form-navigation [type=submit]').toggle(atTheEnd);
	  }
	
	  function curIndex() {
		// Return the current index by looking at which section has the class 'current'
		return $sections.index($sections.filter('.current'));
	  }
	
	  // Previous button is easy, just go back
	  $('.form-navigation .previous').click(function() {
		navigateTo(curIndex() - 1);
	  });
	
	  // Next button goes forward iff current block validates
	  $('.form-navigation .next').click(function() {
		$('.demo-form').parsley().whenValidate({
		  group: 'block-' + curIndex()
		}).done(function() {
		  navigateTo(curIndex() + 1);
		});
	  });
	
	  // Prepare sections by setting the `data-parsley-group` attribute to 'block-0', 'block-1', etc.
	  $sections.each(function(index, section) {
		$(section).find(':input').attr('data-parsley-group', 'block-' + index);
	  });
	  navigateTo(0); // Start at the beginning
	});
</script> 
</body>
</html>
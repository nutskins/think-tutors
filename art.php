<?php $title= "Art Tutor | London & Internationally | Think Tutors"; ?>
<?php $metadescription= "We provide expert private tuition to students at some of the leading art, design, fashion and film schools in London and across the UK.";?>
<?php $page = "services"; include 'header.php' ?>
<main>
	<section>
		<div class="banner" style=" background-image:url(images/3tuition.jpg)">
		<div class="title"><h1>Art</h1></div>
		</div>
	</section>
	<section>
		<div class="int_content">
			<div class="wrapper">
                    <p>We work with students at some of the leading art, design, fashion and film schools in London and internationally. Our tutors can provide guidance on a wide range of topics, from artistic techniques, knowledge of artistic theory and History of Art. Other areas include:</p>
                    <ul>
                        <li>Establishing the creative process</li>
                        <li>Interpreting briefs and responses</li>
                        <li>Assisting with creative ideas</li>
                        <li>Providing industry expertise and knowledge</li>
                        <li>Critiques of work</li>
                        <li>Constructive feedback and guidance</li>
                        <li>Portfolio guidance</li>
                    </ul>
                    <p>Our tutors are fully flexible to suit your schedule and can take you or your child to galleries across the world which has helped many of our students seed inspiration for their own creative pieces. Alternatively, if you or your family are travelling, which is often the case, online tuition is available which can be just as effective. </p>
                    <p>Whether you�re undertaking a specialised course or doing a degree in fine art, our elite tutors can offer you the support and guidance you need to develop your creativity and progress your artistic career.</p>
                    <p>Our art tutors are the very best in their field, having undertaken extensive artistic and creative training, and most have worked in creative industries for many years, including advertising, fashion and the film industry.</p>
				<div class="clear">

				</div>
		</div>
	</section>
	
	
		<section>
		<div class="review_slide blue">
				<div class="wrapper">
					  <div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Our tutor was exceptional, showing the ability to convey<br>the course content in a simple yet concise manner,<br>making it easy to pick up and remember.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Thank you once again for the support<br>and guidance that you and Sebastian gave to our students,<br>it definitely did have a positive impact.</h4>
								<h5>Head of Sixth Form</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
                            <img src="images/ttquotewhite.svg" alt="" >
								<h4>A stroke of brilliance.</h4>
								<h5>Chris, father of BSc Geography dissertation student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>I now feel confident to take my exams and would like<br>to thank them for their patience and commitment<br>towards achieving my goal.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Very quick to reply to our initial search for a geography<br>tutor. They clearly have excellent knowledge of the<br>subject and the current curriculum.</h4>
								<h5>Andrea, mother of A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>James knows a lot about University<br>testing procedures and was able to give<br>advice on a difficult course.</h4>
								<h5>Rhea, mother of BSc Astro Geochemistry student</h5>
							</div>
							</div>
						</div>
					<div class="swiper-pagination"></div>
				  </div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<?php include 'footer_contact-form.php';?>
	</main>

<?php include 'footer.php' ?>
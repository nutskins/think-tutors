<?php //include config
require_once('../includes/config.php');

//if not logged in redirect to login page
if(!$user->is_logged_in()){ header('Location: login.php'); }
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Admin - Add Vacancy</title>
  <link rel="stylesheet" href="../style/normalize.css">
  <link rel="stylesheet" href="../style/main.css">
</head>
<body>

<div id="wrapper">

	<?php include('menu.php');?>
	<p><a href="./vacancies.php">Vacancies Admin Index</a></p>

	<h2>Add Vacancy</h2>

	<?php

	//if form has been submitted process it
	if(isset($_POST['submit'])){

		//collect form data
		extract($_POST);

		//very basic validation
		if($vacancies_Title ==''){
			$error[] = 'Please enter the title.';
		}

		if($vacancies_Location ==''){
			$error[] = 'Please enter the location.';
		}

		if($vacancies_Start ==''){
			$error[] = 'Please enter the start date.';
		}

		if($vacancies_Duration ==''){
			$error[] = 'Please enter the duration.';
		}

		if($vacancies_Salary ==''){
			$error[] = 'Please enter the salary.';
		}

		if(!isset($error)){

			try {
				
				require_once  "../bulletproof/src/bulletproof.php";

				$image = new Bulletproof\Image($_FILES);
				
				$filename = explode( '.', $_FILES['vacancies_Image']['name'] );
				
				if($image["vacancies_Image"]){
					// define allowed mime types to upload
					$image->setName($filename[0]);
					$image->setMime(array('jpeg', 'gif', 'png', 'jpeg', 'jpg'));
					$image->setLocation('../uploads', 777);
					$upload = $image->upload();
					
					if($upload){
						$vacancies_Image = $upload->getFullPath(); // uploads/cat.gif
					}else{
						echo $image["error"]; 
					}
				}

				$vacancies_Slug = slug($vacancies_Title);

				//insert into database
				$stmt = $db->prepare('INSERT INTO vacancies (vacancies_Title,vacancies_Location,vacancies_Slug,vacancies_Image,vacancies_Content,vacancies_Start,vacancies_Duration,vacancies_Salary,vacancies_Date) VALUES (:vacancies_Title, :vacancies_Location, :vacancies_Slug, :vacancies_Image, :vacancies_Content, :vacancies_Start, :vacancies_Duration, :vacancies_Salary, :vacancies_Date)') ;
				$stmt->execute(array(
					':vacancies_Title' => $vacancies_Title,
					':vacancies_Location' => $vacancies_Location,
					':vacancies_Slug' => $vacancies_Slug,
					':vacancies_Image' => $vacancies_Image,
					':vacancies_Content' => $vacancies_Content,
					':vacancies_Start' => $vacancies_Start,
					':vacancies_Duration' => $vacancies_Duration,
					':vacancies_Salary' => $vacancies_Salary,
					':vacancies_Date' => date('Y-m-d H:i:s')
				));
				$vacancies_Id = $db->lastInsertId();

				//redirect to index page
				header('Location: vacancies.php?action=added');
				exit;

			} catch(PDOException $e) {
			    echo $e->getMessage();
			}

		}

	}

	//check for any errors
	if(isset($error)){
		foreach($error as $error){
			echo '<p class="error">'.$error.'</p>';
		}
	}
	?>

	<form action='' method='post' enctype="multipart/form-data">

		<p><label>Title</label><br />
		<input type='text' name='vacancies_Title' value='<?php if(isset($error)){ echo $_POST['vacancies_Title'];}?>'></p>

		<p><label>Location</label><br />
		<input type='text' name='vacancies_Location' value='<?php if(isset($error)){ echo $_POST['vacancies_Location'];}?>'></p>

		<p><label>Description</label><br />
        <input type='text' name='vacancies_Content' value='<?php if(isset($error)){ echo $_POST['vacancies_Content'];}?>'></p>
        
        <p><label>Featured Image</label><br />
        <input type="file" name="vacancies_Image" accept="image/*"/><?php if(isset($error)){ echo $_POST['vacancies_Image'];}?></p>

		<p><label>Start</label><br />
        <input type='text' name='vacancies_Start' value='<?php if(isset($error)){ echo $_POST['vacancies_Start'];}?>'></p>

		<p><label>Duration</label><br />
        <input type='text' name='vacancies_Duration' value='<?php if(isset($error)){ echo $_POST['vacancies_Duration'];}?>'></p>

		<p><label>Salary</label><br />
        <input type='text' name='vacancies_Salary' value='<?php if(isset($error)){ echo $_POST['vacancies_Salary'];}?>'></p>

		<p><input type='submit' name='submit' value='Submit'></p>
	</form>
</div>
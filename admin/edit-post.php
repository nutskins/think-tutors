<?php //include config
require_once('../includes/config.php');

//if not logged in redirect to login page
if(!$user->is_logged_in()){ header('Location: login.php'); }
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Admin - Edit Post</title>
  <link rel="stylesheet" href="../style/normalize.css">
  <link rel="stylesheet" href="../style/main.css">
  <script src="https://cdn.tiny.cloud/1/gg5hwj398e6pm4ptkoz8w4c38lqo2tglbfvnmuu81bb1obhu/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
</head>
<body>
<script>
tinymce.init({
  selector: 'textarea',  // change this value according to the HTML
  toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | outdent indent'
});
    </script>


<div id="wrapper">

	<?php include('menu.php');?>
	<p><a href="./">Blog Admin Index</a></p>

	<h2>Edit Post</h2>


	<?php

	//if form has been submitted process it
	if(isset($_POST['submit'])){

		//collect form data
		extract($_POST);

		//very basic validation
		if($postID ==''){
			$error[] = 'This post is missing a valid id!.';
		}

		if($postTitle ==''){
			$error[] = 'Please enter the title.';
		}

		if($postCont ==''){
			$error[] = 'Please enter the content.';
		}

		if(!isset($error)){

			try {
				require_once  "../bulletproof/src/bulletproof.php";

				$image = new Bulletproof\Image($_FILES);
				
				$filename = explode( '.', $_FILES['featuredImage']['name'] );
				
				if($image["featuredImage"]){
					// define allowed mime types to upload
					$image->setName($filename[0]);
					$image->setMime(array('jpeg', 'gif', 'png', 'jpeg'));
					$image->setLocation('../uploads', 777);
					$upload = $image->upload(); 
					
					if($upload){
						$featuredImage = $upload->getFullPath(); // uploads/cat.gif
					}else{
						$errorimage = $image["error"]; 
					}
				}

				$postSlug = slug($postTitle);
	
				if(!isset($errorimage)){
					//insert into database
					$stmt = $db->prepare('UPDATE blog_posts_seo SET postTitle = :postTitle, postSlug = :postSlug, postMetaTitle = :postMetaTitle, postMetaDesc = :postMetaDesc, postMetaKey = :postMetaKey, postCont = :postCont, featuredImage = :featuredImage WHERE postID = :postID') ;
					$stmt->execute(array(
						':postTitle' => $postTitle,
						':postSlug' => $postSlug,
						':postMetaTitle' => $postMetaTitle,
						':postMetaDesc' => $postMetaDesc,
						':postMetaKey' => $postMetaKey,
						':postCont' => $postCont,
						':featuredImage' => $featuredImage,
						':postID' => $postID
					));
				}else{
					//insert into database
					$stmt = $db->prepare('UPDATE blog_posts_seo SET postTitle = :postTitle, postSlug = :postSlug, postMetaTitle = :postMetaTitle, postMetaDesc = :postMetaDesc, postMetaKey = :postMetaKey, postCont = :postCont WHERE postID = :postID') ;
					$stmt->execute(array(
						':postTitle' => $postTitle,
						':postSlug' => $postSlug,
						':postMetaTitle' => $postMetaTitle,
						':postMetaDesc' => $postMetaDesc,
						':postMetaKey' => $postMetaKey,
						':postCont' => $postCont,
						':postID' => $postID
					));
				}

				//delete all items with the current postID
				$stmt = $db->prepare('DELETE FROM blog_post_cats WHERE postID = :postID');
				$stmt->execute(array(':postID' => $postID));

				if(is_array($catID)){
					foreach($_POST['catID'] as $catID){
						$stmt = $db->prepare('INSERT INTO blog_post_cats (postID,catID)VALUES(:postID,:catID)');
						$stmt->execute(array(
							':postID' => $postID,
							':catID' => $catID
						));
					}
				}
				//redirect to index page
				//header('Location: index.php?action=updated');
				//exit;

			} catch(PDOException $e) {
			    echo $e->getMessage();
			}
		}
	}
	?>


	<?php
	//check for any errors
	if(isset($error)){
		foreach($error as $error){
			echo $error.'<br />';
		}
	}

		try {

			$stmt = $db->prepare('SELECT postID, postTitle, postMetaTitle, postMetaDesc, postMetaKey, postCont, featuredImage FROM blog_posts_seo WHERE postID = :postID') ;
			$stmt->execute(array(':postID' => $_GET['id']));
			$row = $stmt->fetch(); 

		} catch(PDOException $e) {
		    echo $e->getMessage();
		}

	?>

	<form action='' method='post' enctype="multipart/form-data">
		<input type='hidden' name='postID' value='<?php echo $row['postID'];?>'>

		<p><label>Title</label><br />
		<input type='text' name='postTitle' value='<?php echo $row['postTitle'];?>'></p>

		<p><label>Content</label><br />
		<textarea name='postCont' cols='60' rows='10'><?php echo $row['postCont'];?></textarea></p>
        
        <p><label>Featured Image</label><br />
        <input type="file" name="featuredImage" accept="image/*"/></p>
        <?php if($row['featuredImage'] != ''){ ?>
		<p><img src="<?php echo $row['featuredImage'];?>" style="max-width:100px; max-height:100px;" alt="" /></p>
        <?php }?>

		<fieldset>
			<legend>Categories</legend>

			<?php

			$stmt2 = $db->query('SELECT catID, catTitle FROM blog_cats ORDER BY catTitle');
			while($row2 = $stmt2->fetch()){

				$stmt3 = $db->prepare('SELECT catID FROM blog_post_cats WHERE catID = :catID AND postID = :postID') ;
				$stmt3->execute(array(':catID' => $row2['catID'], ':postID' => $row['postID']));
				$row3 = $stmt3->fetch(); 

				if($row3['catID'] == $row2['catID']){
					$checked = 'checked=checked';
				} else {
					$checked = null;
				}

			    echo "<input type='checkbox' name='catID[]' value='".$row2['catID']."' $checked> ".$row2['catTitle']."<br />";
			}

			?>

		</fieldset>
        
        <h3>SEO</h3>
        
        <p><label>Meta Title</label><br />
        <input type='text' name='postMetaTitle' value='<?php echo htmlspecialchars($row['postMetaTitle'], ENT_QUOTES);?>'></p>
        
        <p><label>Meta Description</label><br />
        <input type='text' name='postMetaDesc' value='<?php echo htmlspecialchars($row['postMetaDesc'], ENT_QUOTES);?>'></p>
        
        <p><label>Meta Keywords</label><br />
        <input type='text' name='postMetaKey' value='<?php echo htmlspecialchars($row['postMetaKey'], ENT_QUOTES);?>'></p>

		<p><input type='submit' name='submit' value='Update'></p>

	</form>
</div>
</body>
</html>	

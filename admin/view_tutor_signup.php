<?php //include config
require_once('../includes/config.php');

//if not logged in redirect to login page
if(!$user->is_logged_in()){ header('Location: login.php'); }
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Admin - View Tutor Signup Form Entries</title>
  <link rel="stylesheet" href="../style/normalize.css">
  <link rel="stylesheet" href="../style/main.css">
</head>
<body>

<div id="wrapper">

	<?php include('menu.php');?>
	<p><a href="./tutor_signup.php">Back</a></p>

	<?php
	//check for any errors
	if(isset($error)){
		foreach($error as $error){
			echo $error.'<br />';
		}
	}

		try {

			$stmt = $db->prepare('SELECT id, title, first_name, last_name, date_of_birth, email_address, contact_number, address_line_1, address_line_2, town, county, post_code, country, employment_status, personal_transport, secondary, college, undergraduate, masters, phd, relevant, dbs, pcge, availibility, start_date, forseeable, commentsn, identification, qualifications, dbspic,vacancy, date FROM tutor_signup WHERE id = :id') ;
			$stmt->execute(array(':id' => $_GET['id']));
			$row = $stmt->fetch(); 

		} catch(PDOException $e) {
		    echo $e->getMessage();
		}

	?>
    	<h2>ID: <?php echo $row['id'];?></h2>
		<input type='hidden' name='id' value='<?php echo $row['id'];?>'>

		<p><b>Name</b>: <?php echo $row['title'];?> <?php echo $row['first_name'];?> <?php echo $row['last_name'];?></p>
        
        <p><b>Date of Birth</b>: <?php echo $row['date_of_birth'];?></p>
        
        <p><b>Email</b>: <?php echo $row['email_address'];?></p>
        
        <p><b>Phone</b>: <?php echo $row['contact_number'];?></p>
        
        <p><b>Address Line</b>: <?php echo $row['address_line_1'];?> <?php echo $row['address_line_2'];?></p>
        
        <p><b>Town</b>: <?php echo $row['town'];?></p>
        
        <p><b>County</b>: <?php echo $row['county'];?></p>
        
        <p><b>Post Code</b>: <?php echo $row['post_code'];?></p>
        
        <p><b>Country</b>: <?php echo $row['country'];?></p>
        
        <p><b>Employment Status</b>: <?php echo $row['employment_status'];?></p>
        
        <p><b>Personal Transport</b>: <?php echo $row['personal_transport'];?></p>
        
        <p><b>Secondary</b>: <?php echo $row['secondary'];?></p>
        
        <p><b>Sixth Form / College</b>: <?php echo $row['college'];?></p>
        
        <p><b>Undergraduate</b>: <?php echo $row['undergraduate'];?></p>
        
        <p><b>Masters</b>: <?php echo $row['masters'];?></p>
        
        <p><b>PhD</b>: <?php echo $row['phd'];?></p>
        
        <p><b>Relevant Employment History</b>: <?php echo $row['relevant'];?></p>
        
        <p><b>DBS</b>: <?php echo $row['dbs'];?></p>
        
        <p><b>PCGE</b>: <?php echo $row['pcge'];?></p>
        
        <p><b>Availability</b>: <?php echo $row['availibility'];?></p>
         
        <p><b>Start Date</b>: <?php echo $row['start_date'];?></p>
          
        <p><b>Forseeable Unavailability</b>: <?php echo $row['forseeable'];?></p>
        
		<p><b>Comments</b>: <?php echo $row['commentsn'];?></p>
        
        <p><b>Vacancy</b>: <?php echo $row['vacancy'];?></p>
        
		<p><b>Identification</b>: <br><?php echo '<img src="../uploads/'.$row['identification'].'" alt="">'; ?></p>
        
        <p><b>Qualifications</b>: <br><?php echo '<img src="../uploads/'.$row['qualifications'].'" alt="">'; ?></p>
        
        <p><b>DBS</b>: <br><?php echo '<img src="../uploads/'.$row['dbspic'].'" alt="">'; ?></p>
        
        <p><b>Date</b>: <?php echo $row['date'];?></p>

</div>
</body>
</html>
<?php //include config
require_once('../includes/config.php');

//if not logged in redirect to login page
if(!$user->is_logged_in()){ header('Location: login.php'); }
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Admin - Add Post</title>
  <link rel="stylesheet" href="../style/normalize.css">
  <link rel="stylesheet" href="../style/main.css">
  <script src="https://cdn.tiny.cloud/1/gg5hwj398e6pm4ptkoz8w4c38lqo2tglbfvnmuu81bb1obhu/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
</head>
<body>
<script>
tinymce.init({
  selector: 'textarea',  // change this value according to the HTML
  toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | outdent indent'
});
    </script>
</head>
<body>

<div id="wrapper">

	<?php include('menu.php');?>
	<p><a href="./">Blog Admin Index</a></p>

	<h2>Add Post</h2>

	<?php

	//if form has been submitted process it
	if(isset($_POST['submit'])){

		//collect form data
		extract($_POST);

		//very basic validation
		if($postTitle ==''){
			$error[] = 'Please enter the title.';
		}

		if($postCont ==''){
			$error[] = 'Please enter the content.';
		}

		if(!isset($error)){

			try {
				
				require_once  "../bulletproof/src/bulletproof.php";

				$image = new Bulletproof\Image($_FILES);
				
				$filename = explode( '.', $_FILES['featuredImage']['name'] );
				
				if($image["featuredImage"]){
					// define allowed mime types to upload
					$image->setName($filename[0]);
					$image->setMime(array('jpeg', 'gif', 'png', 'jpeg', 'jpg'));
					$image->setLocation('../uploads', 777);
					$upload = $image->upload();
					
					if($upload){
						$featuredImage = $upload->getFullPath(); // uploads/cat.gif
					}else{
						echo $image["error"]; 
					}
				}

				$postSlug = slug($postTitle);

				//insert into database
				$stmt = $db->prepare('INSERT INTO blog_posts_seo (postTitle,postSlug,postMetaTitle,postMetaDesc,postMetaKey,postCont,featuredImage,postDate) VALUES (:postTitle, :postSlug, :postMetaTitle, :postMetaDesc, :postMetaKey, :postCont, :featuredImage, :postDate)') ;
				$stmt->execute(array(
					':postTitle' => $postTitle,
					':postSlug' => $postSlug,
					':postMetaTitle' => $postMetaTitle,
					':postMetaDesc' => $postMetaDesc,
					':postMetaKey' => $postMetaKey,
					':postCont' => $postCont,
					':featuredImage' => $featuredImage,
					':postDate' => date('Y-m-d H:i:s')
				));
				$postID = $db->lastInsertId();

				//add categories
				if(is_array($catID)){
					foreach($_POST['catID'] as $catID){
						$stmt = $db->prepare('INSERT INTO blog_post_cats (postID,catID)VALUES(:postID,:catID)');
						$stmt->execute(array(
							':postID' => $postID,
							':catID' => $catID
						));
					}
				}

				//redirect to index page
				header('Location: index.php?action=added');
				exit;

			} catch(PDOException $e) {
			    echo $e->getMessage();
			}

		}

	}

	//check for any errors
	if(isset($error)){
		foreach($error as $error){
			echo '<p class="error">'.$error.'</p>';
		}
	}
	?>

	<form action='' method='post' enctype="multipart/form-data">

		<p><label>Title</label><br />
		<input type='text' name='postTitle' value='<?php if(isset($error)){ echo $_POST['postTitle'];}?>'></p>

		<p><label>Content</label><br />
		<textarea name='postCont' cols='60' rows='10'><?php if(isset($error)){ echo $_POST['postCont'];}?></textarea></p>
        
        <p><label>Featured Image</label><br />
        <input type="file" name="featuredImage" accept="image/*"/><?php if(isset($error)){ echo $_POST['featuredImage'];}?></p>

		<fieldset>
			<legend>Categories</legend>
			<?php
			$stmt2 = $db->query('SELECT catID, catTitle FROM blog_cats ORDER BY catTitle');
			while($row2 = $stmt2->fetch()){
				if(isset($_POST['catID'])){

					if(in_array($row2['catID'], $_POST['catID'])){
                       $checked="checked='checked'";
                    }else{
                       $checked = null;
                    }
				}
			    echo "<input type='checkbox' name='catID[]' value='".$row2['catID']."' $checked> ".$row2['catTitle']."<br />";
			}
			?>
		</fieldset>
        
        <h3>SEO</h3>

        <p><label>Meta Title</label><br />
        <input type='text' name='postMetaTitle' value='<?php if(isset($error)){ echo $_POST['postMetaTitle'];}?>'></p>
        
        <p><label>Meta Description</label><br />
        <input type='text' name='postMetaDesc' value='<?php if(isset($error)){ echo $_POST['postMetaDesc'];}?>'></p>
        
        <p><label>Meta Keywords</label><br />
        <input type='text' name='postMetaKey' value='<?php if(isset($error)){ echo $_POST['postMetaKey'];}?>'></p>

		<p><input type='submit' name='submit' value='Submit'></p>
	</form>
</div>
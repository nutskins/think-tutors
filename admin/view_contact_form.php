<?php //include config
require_once('../includes/config.php');

//if not logged in redirect to login page
if(!$user->is_logged_in()){ header('Location: login.php'); }
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Admin - View Contact Form Entries</title>
  <link rel="stylesheet" href="../style/normalize.css">
  <link rel="stylesheet" href="../style/main.css">
</head>
<body>

<div id="wrapper">

	<?php include('menu.php');?>
	<p><a href="./contact_form.php">Back</a></p>

	<?php
	//check for any errors
	if(isset($error)){
		foreach($error as $error){
			echo $error.'<br />';
		}
	}

		try {

			$stmt = $db->prepare('SELECT id, name, email, phone, comments, date FROM contact WHERE id = :id') ;
			$stmt->execute(array(':id' => $_GET['id']));
			$row = $stmt->fetch(); 

		} catch(PDOException $e) {
		    echo $e->getMessage();
		}

	?>
    	<h2>ID: <?php echo $row['id'];?></h2>
		<input type='hidden' name='id' value='<?php echo $row['id'];?>'>

		<p><b>Name</b>: <?php echo $row['name'];?></p>
        
        <p><b>Email</b>: <?php echo $row['email'];?></p>
        
        <p><b>Phone</b>: <?php echo $row['phone'];?></p>
        
        <p><b>Comments / Questions</b>: <?php echo $row['comments'];?></p>
        
        <p><b>Date</b>: <?php echo $row['date'];?></p>

</div>
</body>
</html>
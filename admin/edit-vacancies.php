<?php //include config
require_once('../includes/config.php');

//if not logged in redirect to login page
if(!$user->is_logged_in()){ header('Location: login.php'); }
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Admin - Edit Vacancy</title>
  <link rel="stylesheet" href="../style/normalize.css">
  <link rel="stylesheet" href="../style/main.css">
</head>
<body>

<div id="wrapper">

	<?php include('menu.php');?>
	<p><a href="./vacancies.php">Vacancies Admin Index</a></p>

	<h2>Edit Vacancy</h2>


	<?php

	//if form has been submitted process it
	if(isset($_POST['submit'])){

		//collect form data
		extract($_POST);

		//very basic validation
		if($vacancies_Title ==''){
			$error[] = 'Please enter the title.';
		}

		if($vacancies_Location ==''){
			$error[] = 'Please enter the location.';
		}

		if($vacancies_Start ==''){
			$error[] = 'Please enter the start date.';
		}

		if($vacancies_Duration ==''){
			$error[] = 'Please enter the duration.';
		}

		if($vacancies_Salary ==''){
			$error[] = 'Please enter the salary.';
		}

		if(!isset($error)){

			try {
				require_once  "../bulletproof/src/bulletproof.php";

				$image = new Bulletproof\Image($_FILES);
				
				$filename = explode( '.', $_FILES['vacancies_Image']['name'] );
				
				if($image["vacancies_Image"]){
					// define allowed mime types to upload
					$image->setName($filename[0]);
					$image->setMime(array('jpeg', 'gif', 'png', 'jpeg'));
					$image->setLocation('../uploads', 777);
					$upload = $image->upload();
					if($upload){
						$vacancies_Image = $upload->getFullPath(); // uploads/cat.gif
					}else{
						$errorimage = $image["error"]; 
					}
				}

				$vacancies_Slug = slug($vacancies_Title);

				if(!isset($errorimage)){
					//insert into database
					$stmt = $db->prepare('UPDATE vacancies SET vacancies_Title = :vacancies_Title, vacancies_Location = :vacancies_Location, vacancies_Slug = :vacancies_Slug, vacancies_Image = :vacancies_Image, vacancies_Content = :vacancies_Content, vacancies_Start = :vacancies_Start, vacancies_Duration = :vacancies_Duration, vacancies_Salary = :vacancies_Salary WHERE vacancies_Id = :vacancies_Id') ;
					$stmt->execute(array(
						':vacancies_Title' => $vacancies_Title,
						':vacancies_Location' => $vacancies_Location,
						':vacancies_Slug' => $vacancies_Slug,
						':vacancies_Image' => $vacancies_Image,
						':vacancies_Content' => $vacancies_Content,
						':vacancies_Start' => $vacancies_Start,
						':vacancies_Duration' => $vacancies_Duration,
						':vacancies_Salary' => $vacancies_Salary,				
						':vacancies_Id' => $vacancies_Id
					));
				}else{
					//insert into database
					$stmt = $db->prepare('UPDATE vacancies SET vacancies_Title = :vacancies_Title, vacancies_Location = :vacancies_Location, vacancies_Slug = :vacancies_Slug, vacancies_Content = :vacancies_Content, vacancies_Start = :vacancies_Start, vacancies_Duration = :vacancies_Duration, vacancies_Salary = :vacancies_Salary WHERE vacancies_Id = :vacancies_Id') ;
					$stmt->execute(array(
						':vacancies_Title' => $vacancies_Title,
						':vacancies_Location' => $vacancies_Location,
						':vacancies_Slug' => $vacancies_Slug,
						':vacancies_Content' => $vacancies_Content,
						':vacancies_Start' => $vacancies_Start,
						':vacancies_Duration' => $vacancies_Duration,
						':vacancies_Salary' => $vacancies_Salary,				
						':vacancies_Id' => $vacancies_Id
					));
					
				}

				//redirect to index page
				//header('Location: vacancies.php?action=updated');
				//exit;

			} catch(PDOException $e) {
			    echo $e->getMessage();
			}
		}
	}
	?>


	<?php
	//check for any errors
	if(isset($error)){
		foreach($error as $error){
			echo $error.'<br />';
		}
	}

		try {

			$stmt = $db->prepare('SELECT vacancies_Id, vacancies_Title, vacancies_Location, vacancies_Slug, vacancies_Image, vacancies_Content, vacancies_Start, vacancies_Duration, vacancies_Salary FROM vacancies WHERE vacancies_Id = :vacancies_Id') ;
			$stmt->execute(array(':vacancies_Id' => $_GET['id']));
			$row = $stmt->fetch(); 

		} catch(PDOException $e) {
		    echo $e->getMessage();
		}

	?>

	<form action='' method='post' enctype="multipart/form-data">
		<input type='hidden' name='vacancies_Id' value='<?php echo $row['vacancies_Id'];?>'>

		<p><label>Title</label><br />
		<input type='text' name='vacancies_Title' value='<?php echo $row['vacancies_Title'];?>'></p>

		<p><label>Location</label><br />
		<input type='text' name='vacancies_Location' value='<?php echo $row['vacancies_Location'];?>'></p>

		<p><label>Description</label><br />
        <input type='text' name='vacancies_Content' value='<?php echo $row['vacancies_Content'];?>'></p>
        
        <p><label>Featured Image</label><br />
        <input type="file" name="vacancies_Image" accept="image/*"/><?php echo $row['vacancies_Image'];?></p>
        <?php if($row['vacancies_Image'] != ''){ ?>
		<p><img src="<?php echo $row['vacancies_Image'];?>" style="max-width:100px; max-height:100px;" alt="" /></p>
        <?php }?>

		<p><label>Start</label><br />
        <input type='text' name='vacancies_Start' value='<?php echo $row['vacancies_Start'];?>'></p>

		<p><label>Duration</label><br />
        <input type='text' name='vacancies_Duration' value='<?php echo $row['vacancies_Duration'];?>'></p>

		<p><label>Salary</label><br />
        <input type='text' name='vacancies_Salary' value='<?php echo $row['vacancies_Salary'];?>'></p>

		<p><input type='submit' name='submit' value='Update'></p>

	</form>
</div>
</body>
</html>	

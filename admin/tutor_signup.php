<?php
//include config
require_once('../includes/config.php');

//if not logged in redirect to login page
if(!$user->is_logged_in()){ header('Location: login.php'); }

//show message from add / edit page
if(isset($_GET['delpost'])){ 

	$stmt = $db->prepare('DELETE FROM tutor_signup WHERE id = :id') ;
	$stmt->execute(array(':id' => $_GET['delpost']));

	header('Location: tutor_signup.php?action=deleted');
	exit;
} 

?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Admin Tutor Signup Form Entries</title>
  <link rel="stylesheet" href="../style/normalize.css">
  <link rel="stylesheet" href="../style/main.css">
  <script language="JavaScript" type="text/javascript">
  function delpost(id, title)
  {
	  if (confirm("Are you sure you want to delete '" + title + "'"))
	  {
	  	window.location.href = 'tutor_signup.php?delpost=' + id;
	  }
  }
  </script>
</head>
<body>

	<div id="wrapper">

	<?php include('menu.php');?>

	<?php 
	//show message from add / edit page
	if(isset($_GET['action'])){ 
		echo '<h3>Post '.$_GET['action'].'.</h3>'; 
	} 
	?>

	<table>
	<tr>
	    <th>ID</th>
		<th>Name</th>
        <th>Email</th>
        <th>Country</th>
		<th>Date</th>
		<th>Action</th>
	</tr>
	<?php
		try {

			$stmt = $db->query('SELECT id, title, first_name, last_name, email_address, country, date FROM tutor_signup ORDER BY id DESC');
						
			while($row = $stmt->fetch()){
				
				echo '<tr>';
				echo '<td>'.$row['id'].'</td>';
				echo '<td>'.$row['title'].' '.$row['first_name'].' '.$row['last_name'].'</td>';
				echo '<td>'.$row['email_address'].'</td>';
				echo '<td>'.$row['country'].'</td>';
				echo '<td>'.date('jS M Y', strtotime($row['date'])).'</td>';
				?>
				<td>
					<a href="view_tutor_signup.php?id=<?php echo $row['id'];?>">View</a> | 
					<a href="javascript:delpost('<?php echo $row['id'];?>','<?php echo $row['name'];?>')">Delete</a>
				</td>
				
				<?php 
				echo '</tr>';
			}

		} catch(PDOException $e) {
		    echo $e->getMessage();
		}
	?>
	</table>
	</div>
</body>
</html>
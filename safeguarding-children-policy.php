<?php $title= "Privacy Policy | Think Tutors"; ?>
<?php $metadescription= "We know that the welfare of children is paramount. Click to find out more about the policies we have in place for safeguarding all children.";?>
<?php $page = "services"; include 'header.php' ?>
<main>
	<section>
		<div class="banner" style=" background-image:url(images/2aboutus.jpg)">
		<div class="title"><h1>Safeguarding Children Policy</h1></div>
		</div>
	</section>
	<section>
		<div class="int_content">
			<div class="wrapper">
			<p style="text-align: center;"><strong>THINK TUTORS LIMITED</strong></p>
<p style="text-align: center;">&nbsp;</p>
<p style="text-align: center;"><strong>SAFEGUARDING CHILDREN POLICY</strong></p>
<p style="text-align: center;">&nbsp;</p>
<p style="text-align: left;"><strong>1 Introduction</strong></p>
<p style="text-align: left;">1 Think Tutors Limited is a for-profit organisation run by:</p>
<p style="text-align: left;">Think Tutors Limited</p>
<p style="text-align: left;">2 Think Tutors Limited is based at:</p>
<p style="text-align: left;">Berkeley Square House</p>
<p style="text-align: left;">35 Berkeley Square</p>
<p style="text-align: left;">Mayfair</p>
<p style="text-align: left;">London</p>
<p style="text-align: left;">W1J 5BF UK</p>
<p style="text-align: left;">3 Think Tutors Limited is managed by a management team (the Team). One of the management team has particular responsibility for safeguarding children.</p>
<p style="text-align: left;">4 The Team has adopted this Safeguarding Children Policy and expects every adult working or helping at Think Tutors Limited to support it and comply with it. Consequently, this policy shall apply to all staff, managers, trustees, directors, volunteers, students or anyone working on behalf of Think Tutors Limited.</p>
<p style="text-align: left;">&nbsp;</p>
<p style="text-align: left;"><strong>2 Purpose of the Policy</strong></p>
<p style="text-align: left;">2.1 Think Tutors Limited (&ldquo;we&rdquo;/&ldquo;us&rdquo;/&ldquo;our&rdquo; etc) is committed to protecting the privacy of your personal data collected in the course of our business. We are a service provider based at Berkeley Square House, 35 Berkeley Square, Mayfair, London W1J 5BF and can be contacted on +44(0) 207 117 2835 or <a href="mailto:info@thinktutors.com">info@thinktutors.com</a>.</p>
<p style="text-align: left;">1 This policy is intended to protect children and young people who receive any service from us, including those who are the children of adults who may receive services from us.</p>
<p style="text-align: left;">2 As an organisation we believe that no child or young person should experience abuse or harm and are committed to the protection of children and young people and this policy is intended to provide guidance and overarching principles to those who represent us as volunteers or staff, to guide our approach to child protection and safeguarding.</p>
<p style="text-align: left;"><strong>3 The risks to children</strong></p>
<p style="text-align: left;">Nearly every child grows up in a safe and happy environment and it is important not to exaggerate or overestimate the dangers. Nevertheless, there are situations where children need protection including:</p>
<p style="text-align: left;"><ul>
<li>Sexual abuse</li>
<li>Grooming</li>
<li>Physical and emotional abuse and neglect</li>
<li>Domestic violence</li>
<li>Inappropriate supervision by staff or volunteers</li>
<li>Bullying, cyber bullying, acts of violence and aggression within our schools and campuses</li>
<li>Victimisation</li>
<li>Self-harm</li>
<li>Unsafe environments and activities</li>
<li>Crime</li>
<li>Exploitation</li>
</ul><p>
<p style="text-align: left;"><strong>4 Universality of Protection</strong></p>
<p style="text-align: left;">We recognise that:</p>
<ul>
<li>the welfare of the child is paramount</li>
<li>all children regardless of race, gender, religious belief, disability, age, sexual orientation or identity have a right to equal protection from harm</li>
<li>some children are more vulnerable to harm as a result of their circumstances, prior experiences, communication needs or level of dependency</li>
<li>working with children, young people, their parents and/or guardians, carers or other agencies is essential to protecting their wellbeing.</li>
</ul>
</p>
<p style="text-align: left;"><strong>5 Safeguarding children at events / activities</strong></p>
<p style="text-align: left;">1 There are three kinds of events/activities:</p>
<p style="text-align: left;">1.1 those open to adults and children of all ages,</p>
<p style="text-align: left;">1.2 those for children accompanied by a 'parent',</p>
<p style="text-align: left;">1.3 those for unaccompanied children, which are sometimes run alongside other events/activities.</p>
<p style="text-align: left;">2 At events and activities open to all ages, children under 16 must be accompanied throughout by an adult over the age of 18 who not only brings the child but also takes the child home again afterwards. Young people aged 16 or 17 may attend unaccompanied if they bring the written consent and mobile telephone number of one of their parents.</p>
<p style="text-align: left;">3 At events and activities for children accompanied by a 'parent', children under 16 must be supervised throughout the event by an adult over the age of 18 who not only brings the child to the event but also takes the child home again afterwards. If a lone adult brings more than one child, then the children will have to stay together, so that the one adult can supervise them. Young people aged 16 or 17 may attend unaccompanied if they bring the written consent and mobile telephone number of one of their parents.</p>
<p style="text-align: left;">4 At events and activities for unaccompanied children, children under the age of 16 must be enrolled by a responsible adult before being left with the event leader. The enrolment must record the child's name, age and address and the names and addresses of the child's parents, plus the parents' mobile telephone numbers. Young people aged 16 or 17 may attend unaccompanied if they bring the written consent and mobile telephone number of one of their parents.</p>
<p style="text-align: left;">5 Both event and activities are to be defined broadly to include any occasions where Think Tutors Limited will be providing a service.</p>
<p style="text-align: left;"><strong>6 Disclosure and barring</strong></p>
<p style="text-align: left;">1 Think Tutors Limited offers the following activities for children:</p>
<p style="text-align: left;">a. Tuition</p>
<p style="text-align: left;">b. Mentoring</p>
<p style="text-align: left;">c. Homeschooling</p>
<p style="text-align: left;">d. Advisory.</p>
<p style="text-align: left;">2 Some of our activities may therefore require adult participants or adult leaders to undergo DBS and/or police checks under the Safeguarding Vulnerable Groups Act 2006. The required level of checking (if any) will broadly reflect the degree and frequency of unsupervised access given to other people's children.</p>
<p style="text-align: left;">3 The Team will take very seriously any allegation of impropriety on the part of any member of Think Tutors Limited. A member of Think Tutors Limited who discovers anything amiss should get in touch immediately with the following:</p>
<p style="text-align: left;">Mr James Mitchell</p>
<p style="text-align: left;">4 The Team will review the allegation and the likely risk to children and, if appropriate, will consider banning the member from future events or revoking his or her membership or both, but only in full accordance with the rules and procedures of Think Tutors Limited.</p>
<p style="text-align: left;"><strong>7 Health and safety aspects of safeguarding children</strong></p>
<p style="text-align: left;">1 Before starting any event for unaccompanied children, the Team will carry out a risk assessment and then take steps to minimise all risks to health and safety. Parents and children will be made aware of any particular risks and of the steps to be taken to minimise those risks. The Team will keep a record of all risk assessments.</p>
<p style="text-align: left;">2 Sufficient adults must be present at any event for unaccompanied children to enable one adult to deal with any emergency while another adult supervises the children not directly affected by the emergency.</p>
<p style="text-align: left;"><strong>8 Policy on the prevention of bullying</strong></p>
<p style="text-align: left;">We will not tolerate the bullying of children either by adults or by other children. If any incident of child-on-child bullying should arise at a Think Tutors Limited event, those involved will be separated immediately and the parents of the children involved will be asked to deal with the matter. The Team will review all incidents of child-on-child bullying and assess the likely future risk to children. If appropriate, the Team will consider banning a child from future events, but only in full accordance with the rules and procedures of Think Tutors Limited. Allegations of adults bullying children will be dealt with under paragraph 6.3 above.</p>
<p style="text-align: left;"><strong>9 Photographing children</strong></p>
<p style="text-align: left;">1. 	No photos will be taken or published of any child attending an event or activity unless prior written permission is sought from a person with parental responsibility. If any person has any concerns regarding any person taking photos at an event or activity, that person should contact Think Tutors Limited immediately.</p>
<p style="text-align: left;"><strong>10 Managing behaviour, discipline and acceptable restraint</strong></p>
<p style="text-align: left;">1 Adults supervising children at Think Tutors Limited events must never use any form of corporal punishment. If physical restraint is absolutely necessary to prevent injury to any person or to prevent serious damage to property, then the minimum necessary restraint may be used � but for that purpose only.</p>
<p style="text-align: left;">2 Unacceptable behaviour at Think Tutors Limited events for unaccompanied children will generally be stopped by separating the children from each other and from the group. The miscreants will be suitably supervised and will be returned as soon as possible to the care of their parents.</p>
<p style="text-align: left;">3 Think Tutors Limited may apply a further disciplinary sanction; namely the banning of the child from one or more future events over the following 18 months. Any such sanction would be determined and applied by the following officer: Mr Neil Ridley</p>
<p style="text-align: left;">4 A parent who is aggrieved by this ban may appeal to Think Tutors Limited who will hear the views of all relevant persons. The decision of Think Tutors Limited is then final.</p>
<p style="text-align: left;"><strong>11 Other Policies</strong></p>
<p style="text-align: left;">This safeguarding policy should be read together with the following policies and resources:</p>
<ul>
<li>Safeguarding procedures</li>
<li>Safeguarding policy</li>
<li>Data privacy policy</li>
<li>Code of conduct</li>
<li>Behaviour code for children and young people</li>
<li>Safer recruitment policy</li>
<li>Online safety policy</li>
<li>Anti-bullying policy</li>
<li>Managing complaints procedure</li>
<li>Whistleblowing procedure</li>
<li>Health and safety at work policy</li>
<li>Adult to child supervision ratios Policy</li>
</ul>
</p>
<p style="text-align: left;"><strong>12 Legal Framework</strong></p>
<p style="text-align: left;">This policy has been drawn up in accordance with the following:</p>
<ul>
<li>Children Act 1989</li>
<li>United Convention of the Rights of the Child 1991</li>
<li>General Data Protection Regulation</li>
<li>Human Rights Act 1998</li>
<li>Sexual Offences Act 1998</li>
<li>Children Act 2004</li>
<li>Safeguarding Vulnerable Groups Act 2006</li>
<li>Protection of Freedoms Act 2012</li>
<li>Children and Families Act 2014</li>
<li>Information sharing: advice for practitioners providing safeguarding services</li>
<li>Special educational needs and disability (SEND) code of practice form 2014</li>
<li>Working together to safeguard children (2017)</li>
</ul>

</div>
                   


                    <div class="clear">

				</div>
		</div>
	</section>
	
	
		<section>
		<div class="review_slide blue">
				<div class="wrapper">
					  <div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Our tutor was exceptional, showing the ability to convey<br>the course content in a simple yet concise manner,<br>making it easy to pick up and remember.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Thank you once again for the support<br>and guidance that you and Sebastian gave to our students,<br>it definitely did have a positive impact.</h4>
								<h5>Head of Sixth Form</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
                            <img src="images/ttquotewhite.svg" alt="" >
								<h4>A stroke of brilliance.</h4>
								<h5>Chris, father of BSc Geography dissertation student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>I now feel confident to take my exams and would like<br>to thank them for their patience and commitment<br>towards achieving my goal.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Very quick to reply to our initial search for a geography<br>tutor. They clearly have excellent knowledge of the<br>subject and the current curriculum.</h4>
								<h5>Andrea, mother of A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>James knows a lot about University<br>testing procedures and was able to give<br>advice on a difficult course.</h4>
								<h5>Rhea, mother of BSc Astro Geochemistry student</h5>
							</div>
							</div>
						</div>
					<div class="swiper-pagination"></div>
				  </div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<?php include 'footer_contact-form.php';?>
	</main>

<?php include 'footer.php' ?>
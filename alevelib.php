<?php $title= "A-Level & IB Tutors | Think Tutors"; ?>
<?php $metadescription= "We provide expert private tuition for all A-Level & IB subjects, from science to the arts, allowing all of our students to unlock their full potential.";?>
<?php $page = "services"; include 'header.php' ?>
<main>
	<section>
		<div class="banner" style=" background-image:url(images/3tuition.jpg)">
		<div class="title"><h1>A-Level / International Baccalaureate</h1></div>
		</div>
	</section>
	<section>
		<div class="int_content">
			<div class="wrapper">
					<p>A-Level and IB courses can come as quite a shock to many students and families as there is a significant step-up in the level of complexity required for coursework and exams. There is also more emphasis on self-study and developing one’s own ideas beyond the taught curriculum including the introduction to academic papers. An intentional challenge, as success will lead to reading at a top university in the UK or internationally. To both drive success and alleviate stress during this period we recommend having full time tutors, starting with two hours per week in each subject over the course of two years to help your son or daughter stay focused and calm. We only provide tutors who are elites in their field having at least an undergraduate degree and more often a post graduate degree in their chosen subject. Each tutor also has years of experience in guiding students through coursework and exam preparation, always providing enthusiastic and engaging sessions so each student can excel and reach their full potential.</p>
					<p>As part of A-Levels there are now an increasing number of schools offering The Extended Project Qualification (EPQ), which is a more sophisticated piece of research and coursework independent of any A-Level subject. The EPQ gives students the opportunity to write an extended piece of work (minimum 6000 words) on a subject of their choice, which must be fully referenced using academic papers. The EPQ provides vital experience in planning, implementing and reflecting on a piece of work which is more akin to undergraduate study. On completion of the written work the student must also defend their content and ideas in a presentation or viva to ensure they have well-constructed arguments and have critically thought through their content. If an A grade is achieved in the EPQ, then many universities offer a grade reduction in A-Level entry requirements. For example if the grade offer for a university place was originally AAA, it could be reduced to AAB.</p>
					<p>The International Baccalaureate (IB) has a well-founded reputation for being more difficult than A-Levels, both in the exams and with coursework. Containing six subject groups, with a core module comprised of Theory of Knowledge, an extended essay and a Creativity, Action and Service module, the IB is a comprehensive and rewarding educational path to take. Of note is the 4000 word extended essay on a subject matter of their choice. It needs to be something that the student is deeply interested in, as it will take time, dedication and perseverance to achieve a great result. The idea of the extended essay is to prepare students for undergraduate style essay writing and research so it does put them at an advantage upon starting university as many A-Level students will not have been exposed to such a significant piece of writing unless they have undertaken the EPQ. If you do have any questions or concerns with regards to the IB, then please do get in touch as we are able to provide an IB mentor to help guide you through the process.</p>
				<div class="clear">

				</div>
		</div>
	</section>
	
	
		<section>
		<div class="review_slide blue">
				<div class="wrapper">
					  <div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Our tutor was exceptional, showing the ability to convey<br>the course content in a simple yet concise manner,<br>making it easy to pick up and remember.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Thank you once again for the support<br>and guidance that you and Sebastian gave to our students,<br>it definitely did have a positive impact.</h4>
								<h5>Head of Sixth Form</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
                            <img src="images/ttquotewhite.svg" alt="" >
								<h4>A stroke of brilliance.</h4>
								<h5>Chris, father of BSc Geography dissertation student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>I now feel confident to take my exams and would like<br>to thank them for their patience and commitment<br>towards achieving my goal.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Very quick to reply to our initial search for a geography<br>tutor. They clearly have excellent knowledge of the<br>subject and the current curriculum.</h4>
								<h5>Andrea, mother of A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>James knows a lot about University<br>testing procedures and was able to give<br>advice on a difficult course.</h4>
								<h5>Rhea, mother of BSc Astro Geochemistry student</h5>
							</div>
							</div>
						</div>
					<div class="swiper-pagination"></div>
				  </div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<?php include 'footer_contact-form.php';?>
	</main>

<?php include 'footer.php' ?>
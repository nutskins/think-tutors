<?php

if (isset($_POST["submit"])) {
    echo "<script>$(document).ready(function(){
        $('html, body').animate({scrollTop: $('#contact').offset().top}, 100);});
        </script>";

    // Import validation library
    require("validation-2.3.3.php");

    // Rules for validation
    $rules = array();
    $rules[] = "required,name,Name field is required.";
	$rules[] = "required,email,Email field is required.";
	$rules[] = "valid_email,email,Please enter a valid email address.";
	$rules[] = "required,phone,Phone field is required.";
    $rules[] = "required,comment,Comments / Questions field is required.";
    $rules[] = "required,footer-data,Please indicate that you have read and agree to our Data Privacy Policy.";
    
    $errors = validateFields($_POST, $rules);

    // Validate captcha
    $post_data = http_build_query(
        array(
            "secret" => "6Lfkkl4UAAAAAIT1-k4GCKmKDldnRehul7SzsF8D",
            "response" => $_POST["g-recaptcha-response"],
            "remoteip" => $_SERVER["REMOTE_ADDR"]
        )
    );
    $opts = array("http" =>
        array(
            "method"  => "POST",
            "header"  => "Content-type: application/x-www-form-urlencoded",
            "content" => $post_data
        )
    );
    $context  = stream_context_create($opts);
    $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
    $result = json_decode($response);

    // Check if captcha was valid
    if (!$result->success) {
        $errors[] = "The reCAPTCHA submission failed."; // Incorrect captcha solution
    }

    // Check for errors
    if (!empty($errors)) {
        $fields = $_POST;
    } else { // No errors :)
        include("conn.php");

        $name = $_POST['name'];
		$email = $_POST['email'];
		$phone = $_POST['phone'];
		$comment = $_POST['comment'];
		$date = $_POST['date'];
		
		$bodycontent  = 'Name: '.$name .'<br>';
		$bodycontent .= 'Email: '.$email.'<br>';
		$bodycontent .= 'Phone: '.$phone.'<br>';
		$bodycontent .= 'Comments / Questions: '.$comment.'<br>';
		$sender_message = $name.','.'<br>'.
		
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		
		$webemail = 'mail@gmail.com';
		
		$headers .= "From: ".$name." <".$webemail.">\n";
		$toemail = "thinktutors1@gmail.com";
		$subject = "Contact Form Submitted!";
		$sql = "insert into contact (name, email, phone, comments, date) VALUES ('$name', '$email', '$phone', '$comment', '$date')";
		if (mysqli_query ($conn , $sql)) {
			//echo "Successfully To Added Detail";
		}else{
			//echo "Error";
		}
		mysqli_close($conn);
		if (mail($toemail, $subject, $bodycontent, $headers)) {
			$msg = 'Thanks for getting in touch. One of our team will review your message and be in touch in due course.';
		} else {
			$msg = 'Your message could not be sent due to an unforeseen error - please try again, or alternatively, email info@thinktutors.com with your query.';
			//echo 'Mailer Error: ' . $mail->ErrorInfo;
        }
    }
}

?>

<section>
    <div class="contact_section" id="contact">
        <div class="wrapper">
            <div class="contact_left">
                <h3>ابقى على تواصل</h3>
                <p>للاستفسار عن خدماتنا الدراسية ، يرجى ملء نموذج الاتصال وتضمين بعض التفاصيل حول الرسوم الدراسية المطلوبة.</p>
                <p>سنرد على جميع الاستفسارات في غضون 24 ساعة.</p>
                <p>يمكن للمدرسين المستقبليين إكمال نموذج التسجيل الخاص بالمعلم.</p>
                <p style="text-align:right" dir="ltr"><a href="mailto:info@thinktutors.org;">info@thinktutors.com</a><br>
                <a href="tel:00442071172835;">+44 (0) 207 117 2835</a></p>
            </div>
            <div class="form_section">
                <!-- Removed two <br> -->
                <form action=""  method="post">
                    <?php
                        if (!empty($errors)) {
                            echo "<div class='error' style='color:#003f70;'>Please fix the following errors:\n<ul>";
                            
                            foreach ($errors as $error) {
                                echo "<li>$error</li>\n";
                            }

                            echo "</ul></div>";
                        }
                    ?>
                    <div class="row first_row">
                        <div class="col-sm-4 first_col">
                            <div class="form-group">
                                <input type="hidden" name="date" value="<?php echo date('Y-m-d H:i:s'); ?>" >
                                <label class="control-label">Name <span class="field_required" style="color:#ee0000;">*</span></label>
                                <div class="">
                                    <input type="text" class=" form-control" name="name" value="<?php if(isset($fields["name"])){ echo $fields["name"];}?>" >
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 ">
                            <div class="form-group">
                                <label class="control-label">Email <span class="field_required" style="color:#ee0000;">*</span></label>
                                <div class="">
                                    <input class="form-control" name="email" value="<?php if(isset($fields["email"])){ echo $fields["email"];}?>" />
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 last_col">
                            <div class="form-group">
                                <label class="control-label">Phone <span class="field_required" style="color:#ee0000;">*</span></label>
                                <div class="">
                                    <input id="phone" type="tel" class="form-control" name="phone" value="<?php if(isset($fields["phone"])){ echo $fields["phone"];}?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-sm-12 single">
                            <div class="form-group">
                                <label class="control-label">Comments / Questions <span class="field_required" style="color:#ee0000;">*</span></label>
                                <div class="">
                                    <textarea name="comment" class="form-control" ><?php if(isset($fields["comment"])){ echo $fields["comment"];}?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 single">
                            <div dir="ltr" class="form-group">                                
                                <label style="font-family: 'Hind Vadodara', sans-serif;">
                                    <input style="-webkit-appearance: checkbox; width: 18px; height: 18px; margin: 0; position: absolute;" type="checkbox" name="footer-data" <?php if(isset($fields["footer-data"])) echo "checked"; ?>>
                                    <span style="font-size: 14px; font-weight: 500; color: #78828c; display: inline-block; vertical-align: top; margin-top: -4px; padding-left: 30px; line-height: 25px;">By ticking this box you agree to our receipt and use of your data as set out in our <a href="https://thinktutors.com/data-privacy-policy.php">Data Privacy Policy</a></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 single">
                            <div class="form-group">
                                <div class="g-recaptcha" data-sitekey="6Lfkkl4UAAAAAOu53yXbM8HpKgiocGozfJ1Q6dDj"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row last_row">
                        <div class="col-sm-12 single">
                            <div class="form-group">
                                <div class="submit">
                                    <input class="btn btn-default" type="submit" name="submit" value="Send" />
                                </div>
                                <?php if($msg){ echo $msg;}?>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>
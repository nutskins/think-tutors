<?php $title= "الصفحة الرئيسية | Think Tutors"; ?>
<?php $metadescription= "Our tuition is fully tailored to the needs of each student to improve confidence, deliver high results and obtain success in school or university entry.";?>
<?php include 'arheader.php' ?>
<main>
		<section>
			<div class="banner" style="background-image:url(images/1-min.png);">
			</div>
		</section>
		<section>
				<div class="wrapper">
					<div>
							
					<p style="text-align:justify">مهمتنا بسيطة. لتقديم خدمات دروس مخصصة تُمكّن طلابنا من الوصول إلى كامل إمكاناتهم. تقديم دروس خاصة غير مسبوقة عبر المملكة المتحدة وعلى المستوى الدولي ، في Think Tutors ، نحن فخورون بمساعدة طلابنا على الوصول إلى التميز الأكاديمي.</p>
					<p style="text-align:justify">نحن نعلم أنه لا يوجد طالبان متشابهان ، فلماذا يجب أن تكون دراستنا؟ لهذا السبب نقوم بتخصيص خدماتنا لتلبية الاحتياجات الفريدة لكل طالب نعمل معه ، سواء كان ذلك لتحسين الثقة ، أو تحقيق نتائج عالية أو تحقيق النجاح في الالتحاق بالمدارس أو الجامعة.</p>
					<p style="text-align:justify">في Think Tutors ، نوفر المرونة للعمل حول الجدول الزمني الخاص بك ، وتوفير الرسوم الدراسية على مدار الساعة أو لفترة أطول كجزء من التبديل السكني في المملكة المتحدة أو على المستوى الدولي.</p>
					</div>
						<div class="clear"></div>
					</div>
			</div>
		</section>
		<section>
			<div class="light-color">
				<div class="wrapper">
					<div class="both_pnl">
						<div class="right">
							<h2>موصى عليه</h2>
							<p>نحن نقدم المدرسين الذين تم إختيارهم بشكل كامل ، والذين يتناسبون مع الاحتياجات والظروف الفردية لكل طالب من طلابنا. ونحن نفخر على إقامة علاقات وثيقة مع الأسر والمدارس والمعلمين لدينا ، والحفاظ على اتصال متكرر طوال مدة الدراسة. درس جميع معلمينا أفضل الجامعات حتى يكونوا مجهزين بشكل جيد لتوجيه طلابنا من خلال عملية التعليم بأكملها ، من 11 مرحلة ما قبل الاختبار إلى درجة الدراسات العليا.</p>
							<p>يقع في جميع أنحاء لندن وسوراي وبيركشاير وأبعد من ذلك في المملكة المتحدة وأوروبا وبقية العالم ، والمعلمون لدينا هم الأفضل حقًا في مجالاتهم.</p>
						</div>
						<div class="left">
							<img class="hide_img" src="images/bespoke_right_img.jpg" alt="">
						</div>
						<div class="clear"></div>
					</div>
				</div>
			</div>
		</section>
		<section>
			<div class="three_sec">
				<div class="wrapper">
					<div class="section">
						<div class="three_pnl">
							<img src="images/school.png" alt="school">
							<h3>مدرسة</h3>
							<p>Oتغطي دراستنا الخاصة المصممة خصيصًا جميع مواد التعليم الثانوي والعالي من 11 مرحلة ما قبل الاختبار إلى المستوى العام ، وشهادة الثانوية العامة ، والمستوى المتقدم. يضمن المعلمون الخبيرون التابعون لنا مساؤهم مع أحدث المواصفات والتعديلات ومتطلبات لوحة الامتحان لتبسيط التعليم وتقديم أفضل النتائج الممكنة لطلابنا على طول الطريق</p>
							<div class="read_more">
								<a href="our-services.php">اقرأ أكثر</a>
							</div>
						</div>
						<div class="three_pnl">
							<img src="images/university.png" alt="university">
							<h3>جامعة</h3>
								<p>يمكن تصميم خدمة جامعتنا لتلبية متطلباتك. نحن نعمل مع الطلاب لتوفير رسوم ثابتة ومنتظمة طوال العام الدراسي وإعطاء توجيه مكثف ومركّز حول مهمة أو امتحانات محددة. معلمو الجامعة لدينا هم من النخب الأكاديمية في مجالات تخصصهم. نحن نوجه طلابنا للتوصل إلى حل يعمل بحق لهم ، حول جدولهم الزمني ولتلبية متطلباتهم الفردية.<br><br></p>
								<div class="read_more">
								<a href="our-services.php">اقرأ أكثر</a>
							</div>
						</div>
						<div class="three_pnl">
							<img src="images/international.png" alt="international">
							<h3>دوليا</h3>
							<p>يقدم مدرسو Think Tutors دروسًا في جميع أنحاء المملكة المتحدة وحول العالم. العديد من عملائنا يسافرون ويعملون دوليًا ويستفيدون من خدماتنا التعليمية العالمية لتوفير دروس بدوام كامل أثناء التنقل. لقد عمل مدرسينا حول العالم ويستخدمون لتخطيط الرسوم الدراسية للوفاء بالجداول الزمنية المتغيرة<br><br></p>
							<div class="read_more">
								<a href="international-tutoring.php">اقرأ أكثر</a>
							</div>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			</div>
		</section>
		<section>
			<div class="light-color">
				<div class="wrapper">
					<div class="both_pnl">
						<div class="right">
							<img class="hide_img" src="images/alevels.png" alt="">
						</div>
						<div class="left">
							<h2>التميز الأكاديمي</h2>
							<p>على مر السنين ، قمنا ببناء شبكة قوية ومكثفة من المعلمين الموهوبين والمميزين الذين يمكنهم تقديم الدعم على جميع مستويات النظام التعليمي. وقد حصل جميع معلمينا على درجة البكالوريوس ممتازة كحد أدنى ، مع متابعة معظمهم للدراسات العليا في مجالات تخصصهم. ونتيجة لذلك ، فإنهم يعرفون مباشرة ما يتطلبه السعي لتحقيق التميز الأكاديمي. يتسم معلمونا بالسرعة والمهارة في تحديد الفجوات في فهم الطلاب والتركيز على حلها لتحقيق نتائج عالية.</p>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			</div>
		</section>
		<section dir="ltr">
			<div class="review_slide">
				<div class="wrapper">
					  <div class="swiper-container">
						<div class="swiper-wrapper">
						  <div class="swiper-slide">
							<div class="review">
								<img src="images/ttquoteblue.svg" alt="" >
								<h4> <br>A stroke of brilliance.</h4>
								<h5>Chris, father of BSc Geography dissertation student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquoteblue.svg" alt="" >
								<h4>Thank you once again for the support<br> and guidance that you and Sebastian gave to our students,<br> it definitely did have a positive impact.</h4>
								<h5>Head of Sixth Form</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquoteblue.svg" alt="" >
								<h4>Our tutor was exceptional, showing the ability to convey<br> the course content in a simple yet concise manner,<br> making it easy to pick up and remember.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquoteblue.svg" alt="" >
								<h4>I now feel confident to take my exams and would like<br> to thank them for their patience and commitment<br> towards achieving my goal.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquoteblue.svg" alt="" >
								<h4>Very quick to reply to our initial search for a geography<br> tutor. They clearly have excellent knowledge of the<br> subject and the current curriculum.</h4>
								<h5>Andrea, mother of A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquoteblue.svg" alt="" >
								<h4>James knows a lot about University<br> testing procedures and was able to give<br> advice on a difficult course.</h4>
								<h5>Rhea, mother of BSc Astro Geochemistry student</h5>
							</div>
							</div>
						</div>
					<div class="swiper-pagination"></div>
				  </div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<section>
			<div class="owner_detail">
				<div class="wrapper">
					<div class="panel">
						<div class="left_image"><img src="images/james.png" alt=""></div>
						<div class="middle_content">
							<h2>جيمس و نيل</h2>
							<p>أسس جيمس و نيل Think Tutors في عام 2014 ولديهم أكثر من 10 سنوات من الخبرة في تدريس الطلاب من 11 مرحلة ما قبل الاختبار إلى الدراسات العليا. حصل كلاهما على درجة البكالوريوس (مع مرتبة الشرف) من جامعتين بريطانيتين رائدتين قبل مواصلة تعليمهما من خلال الحصول على درجة الماجستير في مجالات تخصصهما. يعمل جيمس و نيل بشكل وثيق مع كل طالب / عائلة ويمكن الاتصال بهما على مدار 24 ساعة في اليوم ، 7 أيام في الأسبوع.</p>
						</div>
						<div class="right_image"><img src="images/neil.png" alt=""></div>
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<?php include 'arfooter_contact-form.php';?>
	</main>
<?php include 'artestimonial.php';?>
<?php include 'arfooter.php' ?>
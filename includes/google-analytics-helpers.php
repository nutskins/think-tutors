<?php

//Generate UUID
function gaGenerateUUID() {
	return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
		mt_rand(0, 0xffff), mt_rand(0, 0xffff),
		mt_rand(0, 0xffff),
		mt_rand(0, 0x0fff) | 0x4000,
		mt_rand(0, 0x3fff) | 0x8000,
		mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
	);
}

//Parse the GA Cookie
function gaParseCookie() {
	if (isset($_COOKIE['_ga'])) {
		list($version, $domainDepth, $cid1, $cid2) = explode('.', $_COOKIE["_ga"], 4);
		$contents = array('version' => $version, 'domainDepth' => $domainDepth, 'cid' => $cid1 . '.' . $cid2);
		$cid = $contents['cid'];
	} else {
		$cid = gaGenerateUUID();
	}
	return $cid;
}

function sendGaAnalyticsEvent($clientId, $eventCategory, $eventAction, $eventLabel) {
    $trackingCode = "UA-70820220-3";
    
    // GA curl
    $req = curl_init('https://www.google-analytics.com/collect');

    curl_setopt_array($req, array(
        CURLOPT_POST => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_POSTFIELDS =>
        'v=1&t=event&tid=' . $trackingCode. '&cid=' . $clientId . '&ec=' . urlencode($eventCategory) . '&ea=' . urlencode($eventAction) . '&el=' . urlencode($eventLabel)
    ));
    
    // Send the request
    $response = curl_exec($req);
}
?>
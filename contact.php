<?php

require_once('./includes/google-analytics-helpers.php');

if(isset($_POST['submit'])) {
    // Import validation library
    require("validation-2.3.3.php");
    

    // Rules for validation
    $rules[] = "required,name,First Name field is required.";
	$rules[] = "required,email,Email field is required.";
	$rules[] = "valid_email,email,Please enter a valid email address.";
	$rules[] = "required,phone,Phone field is required.";
    $rules[] = "required,comment,Comments / Questions URL field is required.";
    $rules[] = "required,contact-data,Please indicate that you have read and agree to our Data Privacy Policy.";
    
    $errors = validateFields($_POST, $rules);

    // Validate captcha
    $post_data = http_build_query(
        array(
            "secret" => "6Lfkkl4UAAAAAIT1-k4GCKmKDldnRehul7SzsF8D",
            "response" => $_POST["g-recaptcha-response"],
            "remoteip" => $_SERVER["REMOTE_ADDR"]
        )
    );
    $opts = array("http" =>
        array(
            "method"  => "POST",
            "header"  => "Content-type: application/x-www-form-urlencoded",
            "content" => $post_data
        )
    );
    $context  = stream_context_create($opts);
    $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
    $result = json_decode($response);

    // Check if captcha was valid
    if (!$result->success) {
        $errors[] = "The reCAPTCHA submission failed."; // Incorrect captcha solution
    }

    // Check for errors
    if (!empty($errors)) {
        $fields = $_POST;
    } else { // No errors :)
        
        include("conn.php");

        $name = $_POST['name'];
		$email = $_POST['email'];
		$phone = $_POST['phone'];
		$comment = $_POST['comment'];
		$date = $_POST['date'];
		
		$bodycontent  = 'Name: '.$name .'<br>';
		$bodycontent .= 'Email: '.$email.'<br>';
		$bodycontent .= 'Phone: '.$phone.'<br>';
		$bodycontent .= 'Comments / Questions: '.$comment.'<br>';
		$sender_message = $name.','.'<br>'.
		
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		
		$webemail = 'contact@thinktutors.com';
		
		$headers .= "From: ".$name." <".$webemail.">\n";
		$toemail = "thinktutors1@gmail.com";
		$subject = "Contact Form Submitted!";
		$sql = "insert into contact (name, email, phone, comments, date) VALUES ('$name', '$email', '$phone', '$comment', '$date')";
		if (mysqli_query ($conn , $sql)) {
			//echo "Successfully To Added Detail";
			
			// Calculate the current URL
			$canonicalUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			$canonicalUrl = strtolower($canonicalUrl);
			$canonicalUrl = rtrim($canonicalUrl,"/");
			
			sendGaAnalyticsEvent(gaParseCookie(), "Primary Goals", "Contact Form Submission", $canonicalUrl);
		}else{
			//echo "Error";
		}
		mysqli_close($conn);
		if (mail($toemail, $subject, $bodycontent, $headers)) {
			$msg = 'Contact Form Submitted!';
		} else {
			$msg = 'Message could not be sent.';
			//echo 'Mailer Error: ' . $mail->ErrorInfo;
		}
    }
}

?>

<?php 

// Calculate the full URL
$fullUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$fullUrl = strtolower($fullUrl);

// Redirect contact page to non-.php url
if($fullUrl == "https://thinktutors.com/contact.php") {
  	header("Location: https://thinktutors.com/contact" ,TRUE, 301);
	exit;
}

?>

<?php $title= "Contact Us | Think Tutors"; ?>
<?php $metadescription= "Contact us if you would like more information on expert tuition, mentoring, homeschooling and admissions advisory across the UK & internationally.";?>
<?php $metakeywords= "";?>
<?php $page = "contact"; include 'header.php' ?>
<main>
	<section>
		<div class="map">
			<div style='overflow:hidden;height:400px;width:100%;position: relative;'><div id='gmap_canvas' style='height:410px;width:100%;'></div><div></div><div></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style></div>
		</div>
	</section>
	<section>
		<div class="contact_section top_pad">
				<div class="wrapper">
                	<div class="three-sec">
                        <div class="three_pnl">
                            <h3 class="mar-top">Bespoke</h3>
                            <p>We provide fully vetted, handpicked professional tutors and mentors that match the individual needs and circumstances of our prospective students/families.</p>
                            <h3>Dedicated</h3>
                            <p>We work closely with every student/family and are contactable 24 hours a day, 7 days a week.</p>
                            <h3>Academic Excellence</h3>
                            <p>We have built a network of highly talented professional tutors and mentors that can offer support at all levels of the educational system, from 7/8 Plus to postgraduate degrees.</p>
                            <h3>International</h3>
                            <p>Think Tutors delivers tuition throughout the UK and around the world. Many of our clients travel and work internationally so utilise our global tuition service to provide full time tuition whilst on the move.</p>
                        </div>
                        <div class="three_pnl">
                            <h3 class="mar-top">Contact Us</h3>
                            <p>If you have an enquiry with regards to tuition, <a href="./mentoring.php">mentoring</a> or admissions advice then please complete the online form and we will be in touch shortly. For pricing enquiries, please visit our <a href="./fees.php">fee's</a> page, if you still have further questions, you can contact us using our online form.</p>
							<p>If you are a <a href="./workwithus.php">prospective tutor</a> and think you would fit within our network, we'd love to hear from you.</p>
                            <p>We will respond to all enquiries within 24 hours.</p>
                            <p><a href="mailto:info@thinktutors.com;">info@thinktutors.com</a><br><a href="tel:00442071172835">+44 (0) 207 117 2835</a></p>
                            <h3>Office</h3>
                            <p>Berkeley Square House<br> 35 Berkeley Square<br> Mayfair<br> London<br> W1J 5BF<br> UK</p>
                        </div>
                        <div class="three_pnl" id="contact">
                            <div class="form_section">
                            <form action="" method="POST">
								<?php    
									if (!empty($errors)) {
									  echo "<div class='error' style='color:#003f70;'>Please fix the following errors:\n<ul>";
									  foreach ($errors as $error)
										echo "<li>$error</li>\n";

									  echo "</ul></div>";
									}
								?>
								<div class="row single">
                                <div class="col-sm-12 single">
                                    <div class="form-group">
                                        <label class="control-label">Name <span class="field_required" style="color:#ee0000;">*</span></label>
                                        <div class="">
                                            <input type="text" name="name" value="<?php if(isset($fields["name"])){ echo $fields["name"];}?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Email <span class="field_required" style="color:#ee0000;">*</span></label>
                                        <div class="">
                                            <input type="email" name="email" value="<?php if(isset($fields["email"])){ echo $fields["email"];}?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Phone <span class="field_required" style="color:#ee0000;">*</span></label>
                                        <div class="">
                                            <input id="phone" type="tel" class=" form-control" name="phone" value="<?php if(isset($fields["phone"])){ echo $fields["phone"];}?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Comments / Questions <span class="field_required" style="color:#ee0000;">*</span></label>
                                        <div class="">
                                            <textarea name="comment" class="form-control" rows="7"><?php if(isset($fields["comment"])){ echo $fields["comment"];}?></textarea>
                                        </div>
                                        <div class="form-group">                                
                                            <label style="font-family: 'Hind Vadodara', sans-serif;">
                                                <input style="-webkit-appearance: checkbox; width: 18px; height: 18px; margin: 0; position: absolute;" type="checkbox" name="contact-data" <?php if(isset($fields["contact-data"])) echo "checked"; ?>>
                                                <span style="font-size: 14px; font-weight: 500; color: #78828c; display: inline-block; vertical-align: top; margin-top: -4px; padding-left: 30px; line-height: 25px;">By ticking this box you agree to our receipt and use of your data as set out in our <a href="https://thinktutors.com/data-privacy-policy.php">Data Privacy Policy</a></span>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <div class="g-recaptcha" data-sitekey="6Lfkkl4UAAAAAOu53yXbM8HpKgiocGozfJ1Q6dDj"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="submit">
											<input type="hidden" name="date" value="<?php echo date('Y-m-d'); ?>" />
                                            <input class="btn btn-default" type="submit" name="submit" value="Send" />
                                        </div>
                                        <?php if($msg){ echo $msg;}?>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="clear"></div>
                        </div>
                        </div>
                    </div>
				<div class="clear"></div>
			</div>
		</div>
	</section>
</main>

<?php include 'footer.php' ?>
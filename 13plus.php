<?php $title= "13 Plus Tutors | London & Internationally | Think Tutors"; ?>
<?php $metadescription= "We provide expert private tuition for 13 Plus, allowing all of our students to unlock their full potential.";?>
<?php $page = "services"; include 'header.php' ?>
<main>
	<section>
		<div class="banner" style=" background-image:url(images/3tuition.jpg)">
		<div class="title"><h1>Common Entrance at 13 Plus</h1></div>
		</div>
	</section>
	<section>
		<div class="int_content">
			<div class="wrapper">
                    <p>The 13 Plus examination is taken in Year 8 for entry into independent Senior Schools at Year 9. The test will vary from school to school with some using the ISEB examination format, but others use a unique exam. At minimum, each school entry will include English, Maths and Science components. Some schools may require a broader understanding, with the test involving Geography, History, Religious Studies, Classics or Modern Languages at differing levels in addition to the core subjects.</p>
                    <p>As we are experienced in the requirements for each school, we provide school specific tuition so your child will master the subject matter that will be expected of them in the test, this way there are no surprises. We are also happy to provide advice on which schools will best suit your son or daughter�s abilities and interests. </p>
                    <p>As with the 11 Plus exam, the key to success is to start early, up to 12 months prior to the exam, so your child has time to build confidence in the examination material but also the examination structure.  Quite often children are taught the subject matter at school but have not been given exposure to the style of exam questions faced during the 13 Plus test. One of our expert tutors will quickly build a rapport and identify any learning gaps your child may have. Concurrently the tutor will work with your child to build confidence and familiarity in the examination questions themselves by working through numerous example questions and mock tests. By the time of the exam we want each child to be relaxed, confident and completely familiar with the exam so it just feels like another friendly tuition session.</p>
					<p>Schools which use the ISEB Common Entrance at 13 Plus include:</p><br>
					<div class="schoolsrow">
						<div class="schoolscolumn">
                    <ul>
                    <li>Abingdon School</li>
<li>Academic International School</li>
<li>Aldenham School</li>
<li>Ampleforth College</li>
<li>Ardingly College</li>
<li>Ballard Senior School</li>
<li>Barnard Castle School</li>
<li>Battle Abbey School</li>
<li>Bede's School</li>
<li>Bedford School</li>
<li>Benenden School</li>
<li>Berkhamsted Boys</li>
<li>Bethany School</li>
<li>Bishop's Stortford College</li>
<li>Bloxham School</li>
<li>Blundell's School</li>
<li>Bootham School</li>
<li>Bradfield College</li>
<li>Brighton College</li>
<li>Brookhouse Secondary School</li>
<li>Bryanston School</li>
<li>Canford School</li>
<li>Caterham School</li>
<li>Charterhouse</li>
<li>Cheltenham College</li>
<li>Cheltenham Ladies' College</li>
<li>City London Freemen's School</li>
<li>City of London School</li>
<li>Claires Court School</li>
<li>Claremont Fan Court School</li>
<li>Claremont Senior School</li>
<li>Clayesmore Senior School</li>
<li>Clifton College</li>
<li>Cokethorpe School</li>
<li>Cranleigh School</li>
<li>Dauntsey's School</li>
<li>Dean Close School</li>
<li>Denstone College</li>
<li>Dover College</li>
<li>d'Overbroeck's College</li>
<li>Downe House School</li>
<li>Downside School</li>
<li>Eastbourne College</li>
<li>Ellesmere College</li>
<li>Epsom College College</li>
<li>Eton College</li>
<li>Ewell Castle School</li>
<li>Felsted School</li>
<li>Fettes College</li>
<li>Framlingham College</li>
<li>Fulham Senior</li>
<li>Giggleswick School</li>
<li>Glenalmond College</li>
<li>Godolphin School</li>
<li>Gordonstoun School</li>
<li>Gresham's School</li>
<li>Haileybury School</li>
<li>Halliford School</li>
<li>Hampton Court</li>
<li>Hampton School</li>
<li>Harrodian Senior School</li>
<li>Harrow School</li>
<li>Headington School</li>
<li>Heathfield School </li>
<li>Highgate School </li>
<li>Hillcrest Secondary School</li>
<li>Hurstpierpoint College</li>
<li>Ibstock Place School </li>
<li>Ipswich School</li>
<li>Kent College</li>
<li>Kimbolton School</li>
<li>King Edward's School</li>
<li>Kingham Hill School</li>
<li>King's College</li>
<li>King's College School</li>
<li>King's School</li>
<li>Kingswood School</li>
<li>Lancing College</li>
<li>Langley School</li>
<li>Leighton Park School</li>
<li>Leweston School</li>
</ul>
</div>
<div class="schoolscolumn">
	<ul>
<li>Lord Wandsworth College</li>
<li>Loretto School</li>
<li>Loughborough Grammar School</li>
<li>Magdalen College School</li>
<li>Malvern College</li>
<li>Malvern St James School</li>
<li>Marlborough College</li>
<li>Mayfield School</li>
<li>Merchant Taylors' School</li>
<li>Merchiston Castle School</li>
<li>Mill Hill School </li>
<li>Millfield School </li>
<li>Milton Abbey School</li>
<li>Monkton Combe School</li>
<li>Monmouth School</li>
<li>Moreton Hall School</li>
<li>Mount Kelly</li>
<li>Oakham School</li>
<li>Oratory School</li>
<li>Oundle School</li>
<li>Our Lady's Convent Senior School</li>
<li>Pangbourne College</li>
<li>Peponi School</li>
<li>Pocklington School</li>
<li>Portsmouth Grammar School</li>
<li>Prior Park College</li>
<li>Queen Anne's School</li>
<li>Queen Margaret's School</li>
<li>Radley College</li>
<li>Reed's School</li>
<li>Reigate Grammar School</li>
<li>Repton School</li>
<li>Roedean School</li>
<li>Rossall School</li>
<li>Royal Grammar School</li>
<li>Royal Hospital School</li>
<li>Royal Russell School </li>
<li>Rugby School</li>
<li>Rye St Antony School</li>
<li>S. Anselm's College</li>
<li>Seaford College</li>
<li>Sedbergh School</li>
<li>Sevenoaks School </li>
<li>Sherborne Girls School</li>
<li>Sherborne School</li>
<li>Shrewsbury School </li>
<li>St Andrew's Senior School</li>
<li>St Columba's College</li>
<li>St Edmund's School </li>
<li>St Edmund's School </li>
<li>St Edward's School</li>
<li>St George's College </li>
<li>St George's School </li>
<li>St James Senior Boys' School </li>
<li>St John's School Epsom Road</li>
<li>St Lawrence College</li>
<li>St Mary's School</li>
<li>St Paul's School </li>
<li>St Peter's School</li>
<li>St Swithun's School</li>
<li>Stamford Endowed School</li>
<li>Stonyhurst College</li>
<li>Stowe School</li>
<li>Strathallan School </li>
<li>Sutton Valence School </li>
<li>Taunton School</li>
<li>Tonbridge School</li>
<li>Trinity School</li>
<li>Tudor Hall School</li>
<li>University College School</li>
<li>Uppingham School</li>
<li>Warminster School </li>
<li>Wellington College </li>
<li>Wellington School </li>
<li>Wells Cathedral School </li>
<li>Westminster School </li>
<li>Wetherby Senior School </li>
<li>Woodbridge School</li> 
<li>Worth School </li>
<li>Wrekin College</li>
<li>Wycombe Abbey School</li>
</ul>
</div>
</div>

				<div class="clear">

				</div>
		</div>
	</section>
	
	
		<section>
		<div class="review_slide blue">
				<div class="wrapper">
					  <div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Our tutor was exceptional, showing the ability to convey<br>the course content in a simple yet concise manner,<br>making it easy to pick up and remember.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Thank you once again for the support<br>and guidance that you and Sebastian gave to our students,<br>it definitely did have a positive impact.</h4>
								<h5>Head of Sixth Form</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
                            <img src="images/ttquotewhite.svg" alt="" >
								<h4>A stroke of brilliance.</h4>
								<h5>Chris, father of BSc Geography dissertation student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>I now feel confident to take my exams and would like<br>to thank them for their patience and commitment<br>towards achieving my goal.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Very quick to reply to our initial search for a geography<br>tutor. They clearly have excellent knowledge of the<br>subject and the current curriculum.</h4>
								<h5>Andrea, mother of A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>James knows a lot about University<br>testing procedures and was able to give<br>advice on a difficult course.</h4>
								<h5>Rhea, mother of BSc Astro Geochemistry student</h5>
							</div>
							</div>
						</div>
					<div class="swiper-pagination"></div>
				  </div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<?php include 'footer_contact-form.php';?>
	</main>

<?php include 'footer.php' ?>
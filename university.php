<?php $title= "University Tutors | Think Tutors"; ?>
<?php $metadescription= "We provide expert private tuition for all university subjects, allowing all of our students to unlock their full potential.";?>
<?php $page = "services"; include 'header.php' ?>
<main>
	<section>
		<div class="banner" style=" background-image:url(images/3tuition.jpg)">
		<div class="title"><h1>University</h1></div>
		</div>
	</section>
	<section>
		<div class="int_content">
			<div class="wrapper">
					<p>The step from A-Level to university can be significant and at first daunting as there is a focus on self-study away from lectures, where students are expected to read around their subject in order to form their own ideas. Our university tutors and mentors are able to ease students through this transitional period, assisting them with the techniques and mindset required to study at this enhanced level. All of our university tutors and mentors hold a post-graduate degree in their chosen subject, with many also having relevant industry experience. Our tutors are passionate for their subject and can help to bridge the gaps in any learning, proofread coursework and provide guidance with dissertations. Our mentoring service can also help students with pastoral support, timetabling supervision and generally be a shoulder to lean on. If you are also considering staying within university for a post-graduate degree then our advisors can also advise on courses, scholarships and funding. Many of our tutors also assist with post-graduate studies, including masters and PhDs.</p>
					<p>Towards the end of university thoughts naturally turn to life after university. This is always a challenging time as the competition for jobs in the modern world is intensifying. Our careers advisors are available to assist with any questions you may have, from help with a CV, writing a covering letter or how to conduct an interview to land that dream job. We can also provide advice on what your potential career involves, from day-to-day activities, salary expectations and opportunities for travel. Our extensive network also enables us to connect you with relevant industry professionals, often leading to internships, placements and more.</p>
				<div class="clear">

				</div>
		</div>
	</section>
	
	
		<section>
		<div class="review_slide blue">
				<div class="wrapper">
					  <div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Our tutor was exceptional, showing the ability to convey<br>the course content in a simple yet concise manner,<br>making it easy to pick up and remember.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Thank you once again for the support<br>and guidance that you and Sebastian gave to our students,<br>it definitely did have a positive impact.</h4>
								<h5>Head of Sixth Form</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
                            <img src="images/ttquotewhite.svg" alt="" >
								<h4>A stroke of brilliance.</h4>
								<h5>Chris, father of BSc Geography dissertation student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>I now feel confident to take my exams and would like<br>to thank them for their patience and commitment<br>towards achieving my goal.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Very quick to reply to our initial search for a geography<br>tutor. They clearly have excellent knowledge of the<br>subject and the current curriculum.</h4>
								<h5>Andrea, mother of A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>James knows a lot about University<br>testing procedures and was able to give<br>advice on a difficult course.</h4>
								<h5>Rhea, mother of BSc Astro Geochemistry student</h5>
							</div>
							</div>
						</div>
					<div class="swiper-pagination"></div>
				  </div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<?php include 'footer_contact-form.php';?>
	</main>

<?php include 'footer.php' ?>
<?php $title= "University Admissions | London & Internationally | Think Tutors"; ?>
<?php $metadescription= "Our consultants are experts in University admissions, providing guidance on university choices, assessments and personal statements.";?>
<?php $page = "services"; include 'header.php' ?>
<main>
	<section>
		<div class="banner" style=" background-image:url(images/5admissions.jpg)">
		<div class="title"><h1>University Admissions</h1></div>
		</div>
	</section>
	<section>
		<div class="int_content">
			<div class="wrapper">
                    <p>In the UK all applications for full-time university degree courses must be made through the Universities and Colleges Admissions Service (UCAS). This includes Oxford and Cambridge, but please see our dedicated page on <a href="https://thinktutors.com/oxbridgeadmissions">Oxbridge Admissions</a> for more detailed information on this process. UCAS applications open in September for the following academic year and close in mid-late January, except for Oxbridge, Dentistry, Veterinary or Medicine applications which usually close in mid-October. Applications involve students and families providing the following information:</p>
                    <ul>
                        <li>Personal details.</li>
                        <li>The courses you wish to apply for. We recommend exploring different options and attending as many open days as possible during Year 12, as each university will provide a unique experience. Our admissions consultants can provide experienced advice on the universities that best match your child’s abilities and interests. </li>
                        <li>Full details of your current qualifications and also the ones you are currently studying for, e.g. A-Levels/IB.</li>
                        <li>A personal statement of up to 4000 characters.</li>
                        <li>An academic reference from a teacher, tutor or professional contact. If we have provided your son or daughter with home schooling or tuition then we are more than happy to give a reference. </li>
                    </ul>
                    <p>The personal statement requires careful work and is a vital part of the whole application. Ultimately, it’s your child’s chance to convey their ambitions, experience and skills. The same personal statement is used for all of their applications, so if your child is choosing a variety of courses, it’s worth ensuring it contains common themes and experience that relate to all courses. Our admissions consultants can guide a student through the personal statement to ensure that it fully captures all of their skills and motivations for applying. </p>
                    <p>Once your child has submitted their application to UCAS, they’ll need to wait for each institution to make their decision. The UCAS Track Portal will keep them updated and notified of any correspondence for their chosen institutions. It’s important to note that applicants are unable to reply to any offers until a decision has been made from all of the institutions. Institutions will make one of three decisions:</p>                 
                    <ul>
                        <li>Offer a place:</li>
                        <ul>
                            <li>Conditional - will have stipulations that will need to be met for the place to be guaranteed, usually obtaining certain grades in each subject.</li>
                            <li>Unconditional - will have no conditions attached, and is usually only given if the grades required are already in place, for example if you are applying in your gap year.  </li>
                        </ul>
                        <li>Invitation to interview before making a final decision.</li>
                        <li>Advise of an unsuccessful application.</li>
                    </ul>
                    <p>As stated, your child can only respond to offers made once they’ve heard back from all of their chosen institutions. They will need to confirm their firm choice and their insurance choices, usually with a lower grade offer. Then it is a wait until the August results day to find out which university places are then guaranteed.  </p>
				<div class="clear">

				</div>
		</div>
	</section>
	
	
		<section>
		<div class="review_slide blue">
				<div class="wrapper">
					  <div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Our tutor was exceptional, showing the ability to convey<br>the course content in a simple yet concise manner,<br>making it easy to pick up and remember.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Thank you once again for the support<br>and guidance that you and Sebastian gave to our students,<br>it definitely did have a positive impact.</h4>
								<h5>Head of Sixth Form</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
                            <img src="images/ttquotewhite.svg" alt="" >
								<h4>A stroke of brilliance.</h4>
								<h5>Chris, father of BSc Geography dissertation student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>I now feel confident to take my exams and would like<br>to thank them for their patience and commitment<br>towards achieving my goal.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Very quick to reply to our initial search for a geography<br>tutor. They clearly have excellent knowledge of the<br>subject and the current curriculum.</h4>
								<h5>Andrea, mother of A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>James knows a lot about University<br>testing procedures and was able to give<br>advice on a difficult course.</h4>
								<h5>Rhea, mother of BSc Astro Geochemistry student</h5>
							</div>
							</div>
						</div>
					<div class="swiper-pagination"></div>
				  </div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<?php include 'footer_contact-form.php';?>
	</main>

<?php include 'footer.php' ?>
<?php $title= "International Tutoring Online | Think Tutors"; ?>
<?php $metadescription= "If your family is based outside of the UK or travels frequently for business or pleasure then we can provide an expert international tutor from our elite network.";?>
<?php $page = "services"; include 'header.php' ?>
<main>
	<section>
		<div class="banner" style=" background-image:url(images/6international.jpg)">
		<div class="title"><h1>International Tuition</h1></div>
		</div>
	</section>
	<section>
		<div class="int_content">
			<div class="wrapper">
                    <p>If your family is based outside of the UK or travels frequently for business or pleasure then we can provide an expert international tutor from our elite and discreet network. All of our international tutors hold at least an undergraduate degree and have many years of experience in teaching their subject matter. Our tutors first build a rapport with your child and then work on bridging any gaps in their learning, always providing tuition in a friendly, engaging and enthusiastic manner, to really bring the subject alive. Quite often children have been taught much of the key learnings at school but are lacking in confidence. Over time our tutor can work with your child to improve their confidence and nurture their innate abilities to unlock their full potential. </p>
                    <p>We will tailor each international assignment to suit your family and will coordinate all travel and accommodation logistics. All of our international tutors are experienced in international assignments and understand that discretion is paramount. Many tutors in our elite network are multilingual, in languages including Russian, Arabic, Mandarin and French. A number of our tutors hold competent crew/day skipper or higher qualifications and are therefore more than comfortable with yacht based travel.</p>
                    <p>If you would like more information on UK school or university admissions then please visit our dedicated page or contact us for more details. </p>
				<div class="clear">

				</div>
		</div>
	</section>
	
	
		<section>
		<div class="review_slide blue">
				<div class="wrapper">
					  <div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Our tutor was exceptional, showing the ability to convey<br>the course content in a simple yet concise manner,<br>making it easy to pick up and remember.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Thank you once again for the support<br>and guidance that you and Sebastian gave to our students,<br>it definitely did have a positive impact.</h4>
								<h5>Head of Sixth Form</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
                            <img src="images/ttquotewhite.svg" alt="" >
								<h4>A stroke of brilliance.</h4>
								<h5>Chris, father of BSc Geography dissertation student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>I now feel confident to take my exams and would like<br>to thank them for their patience and commitment<br>towards achieving my goal.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Very quick to reply to our initial search for a geography<br>tutor. They clearly have excellent knowledge of the<br>subject and the current curriculum.</h4>
								<h5>Andrea, mother of A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>James knows a lot about University<br>testing procedures and was able to give<br>advice on a difficult course.</h4>
								<h5>Rhea, mother of BSc Astro Geochemistry student</h5>
							</div>
							</div>
						</div>
					<div class="swiper-pagination"></div>
				  </div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<?php include 'footer_contact-form.php';?>
	</main>

<?php include 'footer.php' ?>
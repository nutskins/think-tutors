<?php require('includes/config.php'); ?>
<?php $title= 'Archive :- Month '.$_GET['month'].' - Year '.$_GET['year']; ?>
<?php include 'header.php' ?>
<main>
	<section>
		<div class="banner_internal blog" style="background-image: url(images/uncode-default-back.jpeg)">
			<table>
				<tr>
					<td>
						<h1><?php if($_GET['month']){?>Month : <?php echo $_GET['month'];?><?php } ?><?php if($_GET['year']){?> Year : <?php echo $_GET['year'];?><?php } ?></h1>
					</td>
				</tr>
			</table>
			<div class="fix">
			<a href="#blog"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
		</div>
		</div>
	</section>
	<section>
		<div class="blog_panl iscotop" id="blog">
			<div class="wrapper">
				<div class="blog_left">
				<?php
                    try {
    
                        //collect month and year data
                        $month = $_GET['month'];
                        $year = $_GET['year'];
    
                        //set from and to dates
                        $from = date('Y-m-01 00:00:00', strtotime("$year-$month"));
                        $to = date('Y-m-31 23:59:59', strtotime("$year-$month"));
    
    
                        $pages = new Paginator('4','p');
    
                        $stmt = $db->prepare('SELECT postID FROM blog_posts_seo WHERE postDate >= :from AND postDate <= :to');
                        $stmt->execute(array(
                            ':from' => $from,
                            ':to' => $to
                        ));
    
                        //pass number of records to
                        $pages->set_total($stmt->rowCount());
    
                        $stmt = $db->prepare('SELECT postID, postTitle, postSlug, postCont, postDate FROM blog_posts_seo WHERE postDate >= :from AND postDate <= :to ORDER BY postID DESC '.$pages->get_limit());
                        $stmt->execute(array(
                            ':from' => $from,
                            ':to' => $to
                        ));
                        while($row = $stmt->fetch()){
    
                                echo '<h1><a href="'.$row['postSlug'].'">'.$row['postTitle'].'</a></h1>';
                                echo '<p>Posted on '.date('jS M Y H:i:s', strtotime($row['postDate'])).' in ';
    
                                    $stmt2 = $db->prepare('SELECT catTitle, catSlug	FROM blog_cats, blog_post_cats WHERE blog_cats.catID = blog_post_cats.catID AND blog_post_cats.postID = :postID');
                                    $stmt2->execute(array(':postID' => $row['postID']));
    
                                    $catRow = $stmt2->fetchAll(PDO::FETCH_ASSOC);
    
                                    $links = array();
                                    foreach ($catRow as $cat)
                                    {
                                        $links[] = "<a href='c-".$cat['catSlug']."'>".$cat['catTitle']."</a>";
                                    }
                                    echo implode(", ", $links);
    
                                echo '</p>';
								echo '<p>'.substr(strip_tags($row['postCont']),0,250).'</p>';
                                echo '<p><a href="'.$row['postSlug'].'">Read More</a></p>';
                        }
    
                        echo $pages->page_links("a-$month-$year&");
    
                    } catch(PDOException $e) {
                        echo $e->getMessage();
                    }
                ?>
				</div>
                <div class="blog_right extra_pad">
                    <div class="widget">
                        <h3>Recent Posts</h3>
                        <ul>
                        <?php
                        $stmt = $db->query('SELECT postTitle, postSlug FROM blog_posts_seo ORDER BY postID DESC LIMIT 5');
                        while($row = $stmt->fetch()){
                            echo '<li><a href="'.$row['postSlug'].'">'.$row['postTitle'].'</a></li>';
                        }
                        ?>
                        </ul>
                    </div>
                    <div class="widget extra_sty">
                        <h3>Categories</h3>
                        <ul>
                        <?php
                        $stmt = $db->query('SELECT catTitle, catSlug FROM blog_cats ORDER BY catID DESC');
                        while($row = $stmt->fetch()){
                            echo '<li><a href="c-'.$row['catSlug'].'">'.$row['catTitle'].'</a></li>';
                        }
                        ?>
                        </ul>
                    </div>
                </div>
				<div class="clear"></div>
			</div>
		</div>
	</section>
</main>
<?php include 'footer.php' ?>
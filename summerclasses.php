<?php $title= "Summer Courses | London & Internationally | Think Tutors"; ?>
<?php $metadescription= "We provide expert summer classes to our students online. Find out more about our prices, dates and how the classes will run.";?>
<?php $page = "services"; include 'header.php' ?>
<main>
	<section>
		<div class="banner" style=" background-image:url(images/summer-classes.jpeg)">
		<div class="title"><h1>Summer Classes</h1></div>
		</div>
	</section>
	<section>
		<div class="int_content">
			<div class="wrapper">
					<p>Summer 2021 is a time for students advancing into Year 11 and 13 to consolidate their learning, refresh their minds and restore their motivation before the crucial academic year to come. Think Tutors’ online Summer Classes are designed by our expert education advisors to not only drive results, but also inspire our students to reach their full potential. Please see our <a href="https://thinktutors.com/uploads/Think%20Tutors%20-%20Summer%20School%20Prospectus.pdf" target="_blank">prospectus</a> for more information.</p>
					<p><b>The Journey</b></p>
					<p>Your child will be placed in a small group (maximum 10) befitting to their subject choices, learning style and academic level. We are hoping to run classes for all subjects, at both GCSE and A-Level for students going into Year 11 and 13.</p>
					<p>Our classes will take place twice per day, from 09:00 to 12:00 and from 14:00 to 17:00 on Monday to Friday during the following blocks:</p>
					<p><b>Block 1:</b></p>
					<p>Week 1: August 2nd – 6th</p>
						<p>Week 2: August 9th – 13th 2021</p>
					<p><b>Block 2:</b></p>
					<p>Week 1: August 16th – 20th </p>
						<p>Week 2: August 23rd – 27th 2021</p>
					<p><b>Our Tutors</b></p>
					<p>Think Tutors' rigorous selection process ensures that our tutors are unrivalled in their tuition experience, have excellent academic backgrounds and have passed our safeguarding assessments. Predominantly graduates of Oxbridge and Cambridge, they routinely utilise their elite subject knowledge to provide specialist guidance for their students. Even in a small group setting, our tutors are known for getting to know every student in order to provide a tailored and personable approach to suit individual learning styles.</p>
					<p><b>Pricing</b></p>
					<ul>
						<li>£120 (inclusive of VAT) for one, three-hour class. </li>
						<li>£540 (inclusive of VAT) for five, three-hour classes.</li>
						<li>£1000 (Inclusive of VAT) for ten, three-hour classes.</li>
</ul>
<p>Please fill out this <a href="https://forms.monday.com/forms/0b544f5c4b6936a4d31c32f0252a3ff2?r=use1" target="_blank"><b>form</b></a> to register your interest. A member of the team will shortly be in touch once you have done this. Places are limited so please contact us at <a href="mailto:info@thinktutors.com;">info@thinktutors.com</a> to check availability.</p>
				<div class="clear">

				</div>
		</div>
	</section>
	
	
		<section>
		<div class="review_slide blue">
				<div class="wrapper">
					  <div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Our tutor was exceptional, showing the ability to convey<br>the course content in a simple yet concise manner,<br>making it easy to pick up and remember.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Thank you once again for the support<br>and guidance that you and Sebastian gave to our students,<br>it definitely did have a positive impact.</h4>
								<h5>Head of Sixth Form</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
                            <img src="images/ttquotewhite.svg" alt="" >
								<h4>A stroke of brilliance.</h4>
								<h5>Chris, father of BSc Geography dissertation student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>I now feel confident to take my exams and would like<br>to thank them for their patience and commitment<br>towards achieving my goal.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Very quick to reply to our initial search for a geography<br>tutor. They clearly have excellent knowledge of the<br>subject and the current curriculum.</h4>
								<h5>Andrea, mother of A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>James knows a lot about University<br>testing procedures and was able to give<br>advice on a difficult course.</h4>
								<h5>Rhea, mother of BSc Astro Geochemistry student</h5>
							</div>
							</div>
						</div>
					<div class="swiper-pagination"></div>
				  </div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<?php include 'footer_contact-form.php';?>
	</main>

<?php include 'footer.php' ?>
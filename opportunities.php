<?php require('includes/config.php'); ?>
<?php $title= "A range of tutoring roles available for enthusiastic tutors | Think Tutors"; ?>
<?php $metadescription= "Think Tutors operates throughout the UK and internationally and we are constantly on the lookout for vibrant and enthusiastic tutors to join our network.";?>
<?php $page = "vacancies"; include 'header.php' ?>
<main>
	<section>
		<div class="banner" style="background-image:url(images/Vacancies-min.png);">
		</div>
	</section>
	<section>
		<div class="int_content vac_pad">
			<div class="wrapper">
				<div>
					<h1>Opportunities</h1>
					<p>Think Tutors operates throughout the UK and internationally and we are constantly on the lookout for vibrant, knowledgeable and enthusiastic tutors to join our network. Please take a look at the current opportunities below and we look forward to hearing from you soon.</p>
					<p>If there are no suitable opportunities at this time, don’t worry, simply complete our <a href='tutor_signup.php'>Tutor Sign Up Form</a> and we will be in touch when a suitable opportunity arises.</p>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</section>
	<section>
		<div class="vacanci_pnl">
			<div class="wrapper">
				<div class="vac_cont">
					<?php
						try {
							$stmt = $db->query('SELECT vacancies_Id FROM vacancies');
							$stmt = $db->query('SELECT vacancies_Id, vacancies_Title, vacancies_Location, vacancies_Slug, vacancies_Image, vacancies_Content, vacancies_Start, vacancies_Duration, vacancies_Salary, vacancies_Date FROM vacancies ORDER BY vacancies_Id');
							$count = 1;
							while($row = $stmt->fetch()){ ?>
								<div class="two_pnl"<?php if($count>2){echo "style='margin-top: 50px;'";}?>>
							<?php echo '<div class="box">';
										echo '<img src="'.str_replace("../","",$row['vacancies_Image']).'" alt="">';
										echo '<h3>'.$row['vacancies_Location'].'</h3>';
										echo '<p>'.$row['vacancies_Content'].'</p>';
										echo '<p><strong>Start:</strong> '.$row['vacancies_Start'].'</p>';
										echo '<p><strong>Duration:</strong> '.$row['vacancies_Duration'].'</p>';
										echo '<p><strong>Contract Rate:</strong> '.$row['vacancies_Salary'].'</p>';
										echo '<p>To enquire, please complete the Tutor Sign Up Form referencing this position in the comments and we will get back to you shortly.</p>';
										echo '<a class="btn" href="tutor-signup.php?vacany='.$row['vacancies_Location'].'">Apply Now</a>';
								echo '	</div>
								</div>';
								if($count%2==0 || $count==$stmt->rowCount()){echo '<div class="clear"></div>';}
							$count++;
							}
						} catch(PDOException $e) {
							echo $e->getMessage();
						}
					?>
				</div>
			</div>
		</div>
	</section>
	<?php include 'footer_contact-form.php';?>
	</main>

<?php include 'footer.php' ?>
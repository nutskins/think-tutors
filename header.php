<?php error_reporting(0);?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo $title; ?></title>
	<link rel="icon" href="images/favicon.png" type="image/gif" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Neuton:400,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Hind+Vadodara:300,400,500,600" rel="stylesheet">
	<link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
    
    <link type="text/css" rel="stylesheet" href="css/jssocials.css" />
    <link rel="stylesheet" href="css/parsley.css">
	<link href="css/mobile-menu.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="css/swiper.min.css">
	<link rel="stylesheet" href="css/intlTelInput.css">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/responsive.css" rel="stylesheet" type="text/css">
    <?php if($metadescription){ ?><meta name="description" content="<?php echo $metadescription; ?>" /><?php }?>
    
    <?php if($metakeywords){ ?><meta name="keywords" content="<?php echo $metakeywords; ?>" /><?php }?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.js"></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-70820220-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-70820220-3');
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NP89G22');</script>
<!-- End Google Tag Manager -->

<!-- Canonical URL <?php 

// Calculate the current URL
$canonicalUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$canonicalUrl = strtolower($canonicalUrl);

// Remove any trailing slash
$canonicalUrl = rtrim($canonicalUrl,"/");

// If the current page is a paginated news page, then remove the 'p' parameter from the url.
if(strpos($canonicalUrl, 'news.php?p=') !== false) {
  $canonicalUrl = preg_replace('/(&|\?)p=[^&]*$/', '', $canonicalUrl);
  $canonicalUrl = preg_replace('/(&|\?)p=[^&]*&/', '$1', $canonicalUrl);
}

?> -->
	
<link rel="canonical" href="<?php echo $canonicalUrl; ?>" />

<!-- noindex -->
<?php 

// Calculate the full URL
$fullUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$fullUrl = strtolower($fullUrl);

// If the current page is a paginated news page, then add a tag to noindex the page
if(strpos($fullUrl, 'news.php?p=') !== false) {
  echo '<meta name="robots" content="noindex">';
}
?>
<!-- End noindex -->

</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NP89G22"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	<header>
		<div class="top_bar">
			<div class="wrapper">
				<ul>
					<li>
					<!--<a href="https://thinktutors.com/arindex.php" dir="ltr">موقع عربي &nbsp;&nbsp; <i class="fa fa-angle-right fa-dropdown"></i></a>-->
					<a href="https://wa.me/+447561160497">&nbsp;&nbsp; WhatsApp &nbsp;&nbsp; </a><a href="tel:+442071172835">&nbsp;&nbsp; +44 (0) 207 117 2835 &nbsp;&nbsp; </a><a href="mailto:info@thinktutors.com">&nbsp;&nbsp; info@thinktutors.com &nbsp;&nbsp;<i class="fa fa-angle-right fa-dropdown"></i></a></li>
				</ul>
				<div class="clear"></div>
			</div>
		</div>
		<!--<div class="menu_section">
			<div class="hwrapper">
				<div class="logo">
					<a href="index.php"><img src="images/logo.svg" alt="Company Logo" /></a>
				</div>-->
				<!--<div class="menu">
					<ul>
						<li class="<?php if($page=='services'){echo 'current_page_item';} ?>"><a href="our-services.php">Services</a></li>
						<li class="<?php if($page=='international'){echo 'current_page_item';} ?>"><a href="international-tutoring.php">International</a></li>
						<li class="<?php if($page=='vacancies'){echo 'current_page_item';} ?>"><a href="opportunities.php">Opportunities</a></li>
						<li class="<?php if($page=='tutor'){echo 'current_page_item';} ?>"><a href="tutor-signup.php">Tutor SignUp</a></li>
						<li class="<?php if($page=='news'){echo 'current_page_item';} ?>"><a href="news.php">News</a></li>
						<li class="blue_btn"><a href="contact.php">Contact Us</a></li>
					</ul>
				</div>
				<div class="right_pnl">
					<ul>
						<li><a href="https://www.facebook.com/thinktutors/" target="_blank"><i class="fa fa-facebook-square" style="font-size:20px;" aria-hidden="true"></i></a></li>
						<li><a href="https://www.instagram.com/thinktutors/" target="_blank"><i class="fa fa-instagram" style="font-size:20px;" aria-hidden="true"></i></a></li>
						<li><a href="https://twitter.com/think_tutors" target="_blank"><i class="fa fa-twitter" style="font-size:20px;" aria-hidden="true" title="Follow us on Twitter"></i></a></li>
					</ul>
				</div>
				<div class="clear"></div>-->
			</div>
		</div>

	</header>
		

		
	<div class="topnav" id="myTopnav">
	<div class="menu_section">
			<div class="hwrapper">
				<div class="logo">
					<a href="./"><img src="images/logo.svg" alt="Company Logo"/></a>
				</div>
			</div>
		</div>

		<a href="./">HOME</a>
  <div class="dropdown">
    <button class="dropbtn">ABOUT US
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="./whoweare.php">Who We Are</a>
      <a href="./workwithus.php">Work With Us</a>
      <a href="./fees.php">Fees</a>
    </div>
  </div>

  <div class="dropdown">
    <button class="dropbtn">TUITION
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
	<a href="./youngeryears.php">Younger Years</a>
	  <a href="./78plus.php">7/8 Plus</a>
	  <a href="./11plus.php">11 Plus</a>
	  <a href="./isebpretest.php">ISEB Pre-Test</a>
	  <a href="./13plus.php">13 Plus</a>
	  <a href="./gcse.php">GCSE/IGCSE</a>
	  <a href="./alevelib.php">A-Level/IB</a>
	  <a href="./university.php">University</a>
	  <a href="./art.php">Art</a>
	  <a href="./online.php">Online</a>
	  <a href="./mentoring.php">Mentoring</a>
	  <a href="./eastercourses.php">Easter Courses</a>
	  <a href="./summerclasses.php">Summer Classes</a>
    </div>
  </div>

  <a href="./homeschooling.php">HOMESCHOOLING</a>

  <div class="dropdown">
    <button class="dropbtn">ADMISSIONS ADVISORY
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="./assessment.php">Assessment</a>
      <a href="./schoolplacements.php">School Placements</a>
	  <a href="./universityadmissions.php">University Admissions</a>
	  <a href="./oxbridgeadmissions.php">Oxbridge Admissions</a>
    </div>
  </div>

  <div class="dropdown">
    <button class="dropbtn">INTERNATIONAL
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="./internationaltuition.php">Tuition</a>
      <a href="./internationalhomeschooling.php">Homeschooling</a>
	  <a href="./internationaladmissions.php">Admissions</a>
	</div>
</div>


  <a href="news.php">NEWS</a>
  
  <a href="./contact.php">CONTACT</a>

  
  <a href="javascript:void(0);" style="font-size:12px;" class="icon" onclick="myFunction()">&#9776;</a>
</div>

	<!--<div class="mobile_menu">
        <nav id='cssmenu'>
            <div class="button"></div>
			<ul>
				<li class="<?php if($page=='services'){echo 'current_page_item';} ?>"><a href="our-services.php">Services <i class="fa fa-angle-right fa-dropdown"></i></a></li>
				<li class="<?php if($page=='international'){echo 'current_page_item';} ?>"><a href="international-tutoring.php">International <i class="fa fa-angle-right fa-dropdown"></i></a></li>
				<li class="<?php if($page=='vacancies'){echo 'current_page_item';} ?>"><a href="opportunities.php">Opportunities <i class="fa fa-angle-right fa-dropdown"></i></a></li>
				<li class="<?php if($page=='tutor'){echo 'current_page_item';} ?>"><a href="tutor-signup.php">Tutor Sign Up <i class="fa fa-angle-right fa-dropdown"></i></a></li>
				<li class="<?php if($page=='news'){echo 'current_page_item';} ?>"><a href="news.php">News <i class="fa fa-angle-right fa-dropdown"></i></a></li>
				<li class="<?php if($page=='home'){echo 'current_page_item';} ?>"><a href="arindex.php">موقع عربي <i class="fa fa-angle-right fa-dropdown"></i></a></li>
				<li class="blue_btn"><a href="contact.php">Contact Us <i class="fa fa-angle-right fa-dropdown"></i></a></li>
				<li><a href="tel:00442071172835;">Call Now <i class="fa fa-angle-right fa-dropdown"></i></a></li>
				<li><a href="mailto:info@thinktutors.com">info@thinktutors.com <i class="fa fa-angle-right fa-dropdown"></i></a></li>
			</ul>
        </nav>
    </div>-->
	<!--<div class="top_pad"></div>-->
		 
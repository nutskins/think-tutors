<?php $title= "School Placements | London & Internationally | Think Tutors"; ?>
<?php $metadescription= "Our consultants are experts and can give suitable advice on Pre-Prep, Prep & Senior School admissions for the London area.";?>
<?php $page = "services"; include 'header.php' ?>
<main>
	<section>
		<div class="banner" style=" background-image:url(images/5admissions.jpg)">
		<div class="title"><h1>School Placements</h1></div>
		</div>
	</section>
	<section>
		<div class="int_content">
			<div class="wrapper">
                    <p>There are a few major milestones in life and deciding on your child’s destination for learning is certainly one of them. It can be quite an overwhelming decision as there are many different schools in London, the UK and Internationally, each being unique, with its own ethos, history, expectations and entry requirements. There are many prestigious and well known day and boarding schools in the UK and internationally with an intense selection process. Equally there are many other great schools which are less well known and we can advise on. We have guided many families through this process either for Pre-Prep, Prep or Senior School and provide experienced advice based on your child’s abilities and interests. Their interests are a key factor, be it sport, music, art or debate, it is important that your chosen school allows these extra-curricular interests to flourish. Each school will also have a different atmosphere, some taking a more formal approach and others relatively relaxed. Every child is unique and will respond differently depending on the learning environment and style. Through our assessments we can gauge which schools would be suitable for your child’s style of learning. Ultimately, resulting in a happy, confident and academically fulfilled child.</p>
                    <p>Once we have worked with you to choose a school for your child, unless you have already set your sights on a particular school, we will work with you to identify the requirements and secure a place, providing a wealth of additional support along the way. Often this involves introducing one of our expert tutors to help bridge any gaps in your child’s learning and provide specific support for entrance examinations and interviews. We can also provide the initial introduction to the school’s Admission Team and set up any visits to ensure that a great first impression is made.</p>
				<div class="clear">

				</div>
		</div>
	</section>
	
	
		<section>
		<div class="review_slide blue">
				<div class="wrapper">
					  <div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Our tutor was exceptional, showing the ability to convey<br>the course content in a simple yet concise manner,<br>making it easy to pick up and remember.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Thank you once again for the support<br>and guidance that you and Sebastian gave to our students,<br>it definitely did have a positive impact.</h4>
								<h5>Head of Sixth Form</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
                            <img src="images/ttquotewhite.svg" alt="" >
								<h4>A stroke of brilliance.</h4>
								<h5>Chris, father of BSc Geography dissertation student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>I now feel confident to take my exams and would like<br>to thank them for their patience and commitment<br>towards achieving my goal.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Very quick to reply to our initial search for a geography<br>tutor. They clearly have excellent knowledge of the<br>subject and the current curriculum.</h4>
								<h5>Andrea, mother of A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>James knows a lot about University<br>testing procedures and was able to give<br>advice on a difficult course.</h4>
								<h5>Rhea, mother of BSc Astro Geochemistry student</h5>
							</div>
							</div>
						</div>
					<div class="swiper-pagination"></div>
				  </div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<?php include 'footer_contact-form.php';?>
	</main>

<?php include 'footer.php' ?>
<?php error_reporting(0);?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="rtl">
<head>
<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo $title; ?></title>
	<link rel="icon" href="images/favicon.png" type="image/gif" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Neuton:400,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Hind+Vadodara:300,400,500,600" rel="stylesheet">
	<link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
    
    <link type="text/css" rel="stylesheet" href="css/jssocials.css" />
    <link rel="stylesheet" href="css/parsley.css">
	<link href="css/mobile-menu.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="css/swiper.min.css">
	<link rel="stylesheet" href="css/intlTelInput.css">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/responsive.css" rel="stylesheet" type="text/css">
    <?php if($metadescription){ ?><meta name="description" content="<?php echo $metadescription; ?>" /><?php }?>
    
    <?php if($metakeywords){ ?><meta name="keywords" content="<?php echo $metakeywords; ?>" /><?php }?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.js"></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116291027-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-116291027-1');
</script>

<!-- Hotjar Tracking Code for http://thinktutors.com/ -->
	<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:891656,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>

<meta name="robots" content="noindex">

</head>
<body>
	<header>
		<div class="top_bar">
			<div class="wrapper">
				<ul>
					<li><a href="https://thinktutors.com/">English site &nbsp;&nbsp;</a>
					<a href="mailto:info@thinktutors.com"> &nbsp;&nbsp; info@thinktutors.com &nbsp;&nbsp;</a></li>
				</ul>
				<div class="clear"></div>
			</div>
		</div>
		<div class="menu_section">
			<div class="hwrapper">
				<div class="logo">
					<a href="arindex.php"><img src="images/logo.png" alt="" /></a>
				</div>
				<div class="menu">
					<ul>
						<li class="<?php if($page=='services'){echo 'current_page_item';} ?>" style="font-size:20px"><a href="arour-services.php">خدماتنا</a></li>
						<li class="<?php if($page=='international'){echo 'current_page_item';} ?>" style="font-size:20px"><a href="arinternational-tutoring.php">دولي</a></li>
						<li class="<?php if($page=='contact'){echo 'current_page_item';} ?>" style="font-size:20px"><a href="arcontact.php">ابقى على تواصل</a></li>
					</ul>
				</div>
				<div class="right_pnl">
					<ul>
						<li><a href="https://www.facebook.com/thinktutors/" target="_blank"><i class="fa fa-facebook-square" style="font-size:20px;" aria-hidden="true"></i></a></li>
						<li><a href="https://www.instagram.com/thinktutors/" target="_blank"><i class="fa fa-instagram" style="font-size:20px;" aria-hidden="true"></i></a></li>
						<li><a href="https://twitter.com/think_tutors" target="_blank"><i class="fa fa-twitter" style="font-size:20px;" aria-hidden="true" title="Follow us on Twitter"></i></a></li>
					</ul>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</header>
	<div class="mobile_menu">
        <nav id='cssmenu'>
            <div class="button"></div>
			<ul>
			<li class="<?php if($page=='services'){echo 'current_page_item';} ?>"><a href="arour-services.php">خدماتنا <i class="fa fa-angle-right fa-dropdown"></i></a></li>
			<li class="<?php if($page=='international'){echo 'current_page_item';} ?>"><a href="arinternational-tutoring.php">دولي<i class="fa fa-angle-right fa-dropdown"></i></a></li>
			<li class="<?php if($page=='contact'){echo 'current_page_item';} ?>"><a href="arcontact.php">ابقى على تواصل<i class="fa fa-angle-right fa-dropdown"></i></a></li>
			<li class="<?php if($page=='home'){echo 'current_page_item';} ?>"><a href="index.php">English Site<i class="fa fa-angle-right fa-dropdown"></i></a></li>
			</ul>
        </nav>
    </div>
	<div class="top_pad"></div>
		 
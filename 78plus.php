<?php $title= "7/8 Plus Tutors | London & Internationally | Think Tutors"; ?>
<?php $metadescription= "We provide expert private tuition for 7 & 8 Plus, allowing all of our students to unlock their full potential.";?>
<?php $page = "services"; include 'header.php' ?>
<main>
	<section>
		<div class="banner" style=" background-image:url(images/3tuition.jpg)">
		<div class="title"><h1>7 & 8 Plus</h1></div>
		</div>
	</section>
	<section>
		<div class="int_content">
			<div class="wrapper">
					<p>The 7 or 8 Plus exams are used for entry into Lower Prep School at years 3 or 4 respectively and are an important milestone in a child’s academic life. Each independent school will have a slightly different test so we are able to tailor the tuition around the schools you have selected for entry and we can provide experienced advice on the schools best suited for your child. The test will usually consist of maths, English, verbal reasoning and an informal interview. The 8 Plus is similar to the 7 Plus, but a higher level of sophistication in maths and English is expected at the latter age. The tests are usually carried out in the Spring term ready for September entry. However, a few schools test as early as November. </p>
					<p>Our approach with the early years is to make the tuition fun and engaging so your child is excited and inquisitive when it comes to learning. Whilst having fun your child will be learning the test material and will be exposed to the style of questions that will appear in the exam. This will ensure that your son or daughter is completely comfortable on the day of the test with no reason to panic. </p>
					<p>Schools which accept entrance at 7 or 8 Plus include:</p><br>
					<div class="schoolscolumn">
					<ul>
						<li>Alleyn’s Junior School</li>
						<li>Bancroft’s Prep School</li>
						<li>Bancroft’s School</li>
						<li>Bute House</li>
						<li>City of London School for Girls</li>
						<li>Dragon School</li>
						<li>Dulwich College Junior School</li>
						<li>Dulwich Prep London</li>
						<li>Eltham College</li>
						<li>Forest Prep School</li>
						<li>Haberdashers’ Aske’s Boys’ and Girls’ Junior Schools</li>
						<li>Hampton School</li>
						<li>Highgate</li>
						<li>James Allens’ Girls’ School</li>
						<li>King’s College Wimbledon</li>
						</ul>
</div>
<div class="schoolscolumn">
	<ul>
						<li>Lady Eleanor Holles</li>
						<li>Lambrook</li>
						<li>Latymer Prep School</li>
						<li>Magdalen College School</li>
						<li>Manchester Grammar School</li>
						<li>North London Collegiate School</li>
						<li>South Hampstead High School</li>
						<li>St Mary’s</li>
						<li>St Paul’s Junior School</li>
						<li>Sussex House</li>
						<li>The Perse Prep</li>
						<li>Westminster Cathedral Choir School</li>
                        <li>Westminster Under School</li>
</ul>
</div>

				<div class="clear">

				</div>
		</div>
	</section>
	
	
		<section>
		<div class="review_slide blue">
				<div class="wrapper">
					  <div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Our tutor was exceptional, showing the ability to convey<br>the course content in a simple yet concise manner,<br>making it easy to pick up and remember.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Thank you once again for the support<br>and guidance that you and Sebastian gave to our students,<br>it definitely did have a positive impact.</h4>
								<h5>Head of Sixth Form</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
                            <img src="images/ttquotewhite.svg" alt="" >
								<h4>A stroke of brilliance.</h4>
								<h5>Chris, father of BSc Geography dissertation student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>I now feel confident to take my exams and would like<br>to thank them for their patience and commitment<br>towards achieving my goal.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Very quick to reply to our initial search for a geography<br>tutor. They clearly have excellent knowledge of the<br>subject and the current curriculum.</h4>
								<h5>Andrea, mother of A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>James knows a lot about University<br>testing procedures and was able to give<br>advice on a difficult course.</h4>
								<h5>Rhea, mother of BSc Astro Geochemistry student</h5>
							</div>
							</div>
						</div>
					<div class="swiper-pagination"></div>
				  </div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<?php include 'footer_contact-form.php';?>
	</main>

<?php include 'footer.php' ?>
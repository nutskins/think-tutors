<?php require('includes/config.php'); ?>
<?php $title= "News | London & Internationally | Think Tutors"; ?>
<?php $metadescription= "See all of our latest news on: education updates, revision success, admissions advice and healthy learning tips.";?>
<?php $page = "news"; include 'header.php' ?>
<main>
	<section>
		<div class="banner" style="background-image:url(images/7news.jpg);">
		<div class="title"><h1>News</h1></div>
		</div>
	</section>
	<!--section>
		<div class="banner_internal" style="background-image: url(images/uncode-default-back.jpeg)">
			<table>
				<tr>
					<td>
						<h1>Bespoke Tuition Services</h1>
					</td>
				</tr>
			</table>
			<div class="fix">
			<a href="#blog"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
		</div>
		</div>
	</section-->
	<section>
		<div class="blog_panl iscotop" id="blog">
			<div class="wrapper">
				<div class="blog_left">
                	<?php
						try {
		
							$pages = new Paginator('4','p');
		
							$stmt = $db->query('SELECT postID FROM blog_posts_seo');
		
							//pass number of records to
							$pages->set_total($stmt->rowCount());
							echo '<div class="blog_left_inner">';
							$stmt = $db->query('SELECT postID, postTitle, postSlug, postMetaTitle, postMetaDesc, postMetaKey, postCont, featuredImage, postDate FROM blog_posts_seo ORDER BY postID DESC  '.$pages->get_limit());
							while($row = $stmt->fetch()){
								
								echo '<div class="blog_two"><a href="'.$row['postSlug'].'" class="signplus"><img src="'.str_replace("../","",$row['featuredImage']).'" alt=""></a>';
								echo '<div class="blog_cont">';
								echo '<h3><a href="'.$row['postSlug'].'">'.$row['postTitle'].'</a></h3>';
								echo '<div class="date">
								<span><i class="fa fa-clock-o" aria-hidden="true"></i> <a href="'.$row['postSlug'].'">'.date('jS M Y', strtotime($row['postDate'])).'</a></span>
								</div>';
								echo substr(strip_tags($row['postCont']),0,250);
								echo '<p><a class="blog_btn" href="'.$row['postSlug'].'">Read more <i class="fa fa-angle-right"></i></a></p><hr>';
								echo '<div class="autor">
										<img src="images/2ddebbe71f047a02a0adbd1e4a414201.png" alt=""> <span><a href="'.$row['postSlug'].'">by Think Tutors</a></span>
									</div></div>';
								echo '</div>';
							}
							echo '<div class="clear"></div>';
							echo '</div>';
							echo $pages->page_links();
		
						} catch(PDOException $e) {
							echo $e->getMessage();
						}
					?>
				</div>
				<div class="blog_right extra_pad">
					<div class="widget">
						<h3>Recent Posts</h3>
						<ul>
						<?php
                        $stmt = $db->query('SELECT postTitle, postSlug FROM blog_posts_seo ORDER BY postID DESC LIMIT 5');
                        while($row = $stmt->fetch()){
                            echo '<li><a href="'.$row['postSlug'].'">'.$row['postTitle'].'</a></li>';
                        }
                        ?>
                        </ul>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</section>
</main>
<?php include 'footer.php' ?>
<?php $title= "Fees | London & Internationally | Think Tutors"; ?>
<?php $metadescription= "Our fees for delivering expert tuition, mentoring, homeschooling and admissions advisory.";?>
<?php $page = "services"; include 'header.php' ?>
<main>
	<section>
		<div class="banner" style=" background-image:url(images/2aboutus.jpg)">
		<div class="title"><h1>Fees</h1></div>
		</div>
	</section>
	<section>
		<div class="int_content">
			<div class="wrapper">
					<p>All fees are inclusive of VAT where applicable. We do not charge any registration fees. The fees provided below are starting rates and are subject to the complexity and location of the work. </p>
                    <table style="margin-left: auto; margin-right: auto; font-size: 12px">
<tbody>
<tr style="height: 33px;">
<td style="height: 33px; width: 254px; vertical-align: bottom;">
<p class="p1"><strong><span class="s1">7/8 Plus</span></strong></p>
</td>
<td style="height: 33px; width: 240px; vertical-align: bottom;">
<p class="p1"><span class="s1"> &pound;130 per hour</span></p>
</td>
</tr>
<tr style="height: 33px;">
<td style="height: 33px; width: 254px; vertical-align: bottom;">
<p class="p1"><strong><span class="s1">Common Entrance 11 Plus</span></strong></p>
</td>
<td style="height: 33px; width: 240px; vertical-align: bottom;">
<p class="p1"><span class="s1">&pound;150 per hour</span></p>
</td>
</tr>
<tr style="height: 33px;">
<td style="height: 33px; width: 254px; vertical-align: bottom;">
<p class="p1"><strong><span class="s1">Common Entrance Pre-Test</span></strong></p>
</td>
<td style="height: 33px; width: 240px; vertical-align: bottom;">&pound;150 per hour</td>
</tr>
<tr style="height: 33px;">
<td style="height: 33px; width: 254px; vertical-align: bottom;">
<p class="p1"><strong><span class="s1">Common Entrance 13 Plus</span></strong></p>
</td>
<td style="height: 33px; width: 240px; vertical-align: bottom;">&pound;150 per hour</td>
</tr>
<tr style="height: 33px;">
<td style="height: 33px; width: 254px; vertical-align: bottom;">
<p class="p1"><strong><span class="s1">GCSE/IGCSE</span></strong></p>
</td>
<td style="height: 33px; width: 240px; vertical-align: bottom;">&pound;150 per hour</td>
</tr>
<tr style="height: 23px;">
<td style="height: 23px; vertical-align: bottom;"><strong>A-Level/IB</strong></td>
<td style="height: 23px; width: 240px; vertical-align: bottom;">
<p class="p1"><span class="s1">&pound;180 per hour</span></p>
</td>
</tr>
<tr style="height: 23px;">
<td style="height: 23px; width: 254px; vertical-align: bottom;"><strong>University</strong></td>
<td style="height: 23px; width: 240px; vertical-align: bottom;">
<p class="p1"><span class="s1">&pound;300 per hour</span></p>
</td>
</tr>
<tr style="height: 23px;">
<td style="height: 23px; width: 254px; vertical-align: bottom;"><strong>Mentoring</strong></td>
<td style="height: 23px; width: 240px; vertical-align: bottom;">
<p class="p1"><span class="s1">&pound;1,500 per month</span></p>
</td>
</tr>
<tr style="height: 33px;">
<td style="height: 33px; width: 254px; vertical-align: bottom;">
<p class="p1"><strong><span class="s1">Home Schooling</span></strong></p>
</td>
<td style="height: 33px; width: 240px; vertical-align: bottom;">
<p class="p1"><span class="s1">&pound;1,000 per day</span></p>
</td>
</tr>
<tr style="height: 33px;">
<td style="height: 33px; width: 254px; vertical-align: bottom;">
<p class="p1"><strong><span class="s1">Assessment</span></strong></p>
</td>
<td style="height: 33px; width: 240px; vertical-align: bottom;">
<p class="p1"><span class="s1">&pound;500 per assessment</span></p>
</td>
</tr>
<tr style="height: 33px;">
<td style="height: 33px; width: 254px; vertical-align: bottom;">
<p class="p1"><strong><span class="s1">School Placements</span></strong></p>
</td>
<td style="height: 33px; width: 240px; vertical-align: bottom;">&nbsp;</td>
</tr>
<tr style="height: 33px;">
<td style="height: 33px; width: 254px; vertical-align: bottom;">Nursery&nbsp;</td>
<td style="height: 33px; width: 240px;">&pound;3,000 per placement</td>
</tr>
<tr style="height: 22px;">
<td style="height: 22px; width: 254px; vertical-align: bottom;">Prep-School</td>
<td style="height: 22px; width: 240px; vertical-align: bottom;">&pound;7,000 per placement</td>
</tr>
<tr style="height: 23px;">
<td style="height: 23px; width: 254px; vertical-align: bottom;">Senior School</td>
<td style="height: 23px; width: 240px; vertical-align: bottom;">&pound;12,000 per placement</td>
</tr>
<tr style="height: 23px;">
<td style="height: 23px; width: 254px; vertical-align: bottom;">Sixth Form</td>
<td style="height: 23px; width: 240px; vertical-align: bottom;">&pound;15,000 per placement</td>
</tr>
<tr style="height: 23px;">
<td style="height: 23px; width: 254px; vertical-align: bottom;">
<p class="p1"><strong><span class="s1">University Admissions</span></strong></p>
</td>
<td style="height: 23px; width: 240px; vertical-align: bottom;">&pound;10,000 per placement</td>
</tr>
<tr style="height: 23px;">
<td style="height: 23px; width: 254px; vertical-align: bottom;">
<p class="p1"><strong><span class="s1">Oxbridge Admissions</span></strong></p>
</td>
<td style="height: 23px; width: 240px; vertical-align: bottom;">
<p class="p1"><span class="s1">&pound;20,000 per placement</span></p>
</td>
</tr>
<tr style="height: 23px;">
<td style="height: 23px; width: 254px; vertical-align: bottom;">
<p class="p1"><strong><span class="s1">International Tuition</span></strong></p>
</td>
<td style="height: 23px; width: 240px; vertical-align: bottom;">Available upon request</td>
</tr>
<tr style="height: 23px;">
<td style="height: 23px; width: 254px; vertical-align: bottom;">
<p class="p1"><strong><span class="s1">International Homeschooling</span></strong></p>
</td>
<td style="height: 23px; width: 240px; vertical-align: bottom;">Available upon request&nbsp;</td>
</tr>
<tr style="height: 23px;">
<td style="height: 23px; width: 254px; vertical-align: bottom;">
<p class="p1"><strong><span class="s1">International Admissions</span></strong></p>
</td>
<td style="height: 23px; width: 240px; vertical-align: bottom;">Available upon request</td>
</tr>
<tr style="height: 23px;">
<td style="height: 23px; width: 254px; vertical-align: bottom;">
<p class="p1"><strong><span class="s1">Education in Action</span></strong></p>
</td>
<td style="height: 23px; width: 240px; vertical-align: bottom;">Available upon request</td>
</tr>
<tr style="height: 23px;">
<td style="height: 23px; width: 254px; vertical-align: bottom;">
<p class="p1"><strong><span class="s1">The Bespoke Package</span></strong></p>
</td>
<td style="height: 23px; width: 240px; vertical-align: bottom;">&pound;350,000 per year per child</td>
</tr>
</tbody>
</table>
                    <div class="clear">

				</div>
		</div>
	</section>
	
	
		<section>
		<div class="review_slide blue">
				<div class="wrapper">
					  <div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Our tutor was exceptional, showing the ability to convey<br>the course content in a simple yet concise manner,<br>making it easy to pick up and remember.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Thank you once again for the support<br>and guidance that you and Sebastian gave to our students,<br>it definitely did have a positive impact.</h4>
								<h5>Head of Sixth Form</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
                            <img src="images/ttquotewhite.svg" alt="" >
								<h4>A stroke of brilliance.</h4>
								<h5>Chris, father of BSc Geography dissertation student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>I now feel confident to take my exams and would like<br>to thank them for their patience and commitment<br>towards achieving my goal.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Very quick to reply to our initial search for a geography<br>tutor. They clearly have excellent knowledge of the<br>subject and the current curriculum.</h4>
								<h5>Andrea, mother of A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>James knows a lot about University<br>testing procedures and was able to give<br>advice on a difficult course.</h4>
								<h5>Rhea, mother of BSc Astro Geochemistry student</h5>
							</div>
							</div>
						</div>
					<div class="swiper-pagination"></div>
				  </div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<?php include 'footer_contact-form.php';?>
	</main>

<?php include 'footer.php' ?>
<?php $title= "Privacy Policy | Think Tutors"; ?>
<?php $metadescription= "We explain why we collect personal information about our tutors, students, their parents and guardians, how we use it and how we keep it secure and your rights.";?>
<?php $page = "services"; include 'header.php' ?>
<main>
	<section>
		<div class="banner" style=" background-image:url(images/2aboutus.jpg)">
		<div class="title"><h1>Data and Privacy Policy</h1></div>
		</div>
	</section>
	<section>
		<div class="int_content">
			<div class="wrapper">
			<p style="text-align: center;"><strong>THINK TUTORS LIMITED</strong></p>
<p style="text-align: center;">&nbsp;</p>
<p style="text-align: center;"><strong>DATA&nbsp;PRIVACY STATEMENT (MLC)</strong></p>
<p style="text-align: center;">&nbsp;</p>
<p style="text-align: left;"><strong>1. ABOUT THIS POLICY</strong></p>
<p style="text-align: left;">1.1 This policy explains when and why we collect personal information about our tutors, students and their parents and guardians, how we use it and how we keep it secure and your rights in relation to it.</p>
<p style="text-align: left;">1.2 We may collect, use and store your personal data, as described in this Data Processing Policy and as described when we collect data from you.</p>
<p style="text-align: left;">1.3 We reserve the right to amend this Data Processing Policy from time to time without prior notice. You are advised to check our website <a href="https://thinktutors.com/">www.thinktutors.com</a> regularly for any amendments (but amendments will not be made retrospectively).</p>
<p style="text-align: left;">1.4 We will always comply (and be able to demonstrate our compliance) with relevant data protection legislation, including the General Data Protection Regulation (&ldquo;GDPR&rdquo;) when processing your personal data. Further details on the GDPR can be found at the Information Commissioner&rsquo;s Office website (<a href="https://ico.org.uk/">https://ico.org.uk/</a>). For the purposes of data protection legislation, we will be the &ldquo;controller&rdquo; or &ldquo;data controller&rdquo; of all personal data held in respect of this Policy.</p>
<p style="text-align: left;">&nbsp;</p>
<p style="text-align: left;"><strong>2. WHO ARE WE?</strong></p>
<p style="text-align: left;">2.1 Think Tutors Limited (&ldquo;we&rdquo;/&ldquo;us&rdquo;/&ldquo;our&rdquo; etc) is committed to protecting the privacy of your personal data collected in the course of our business. We are a service provider based at Berkeley Square House, 35 Berkeley Square, Mayfair, London W1J 5BF and can be contacted on +44(0) 207 117 2835 or <a href="mailto:info@thinktutors.com">info@thinktutors.com</a>.</p>
<p style="text-align: left;">&nbsp;</p>
<p style="text-align: left;"><strong>3. DATA PROCESSING</strong></p>
<p style="text-align: left;">3.1 In this Policy we use certain terms from the relevant data protection legislation;</p>
<p style="text-align: left; padding-left: 30px;">(a) &ldquo;data subject&rdquo; - anyone who can be identified from personal data;</p>
<p style="text-align: left; padding-left: 30px;">(b) &ldquo;controller/data controller&rdquo; - a business which holds personal data and decides how it should be processed;</p>
<p style="text-align: left; padding-left: 30px;">(c) &ldquo;processor/data processor&rdquo; - a business which holds personal data on behalf of a controller and processes it in accordance with the controller&rsquo;s instructions;</p>
<p style="text-align: left; padding-left: 30px;">(d) &ldquo;personal data&rdquo; - recorded information we hold about you from which you can be identified. It may include contact details, other personal information, photographs, expressions of opinion about you or indications as to our intentions about you;</p>
<p style="text-align: left; padding-left: 30px;">(e) &ldquo;special categories of personal data&rdquo; - personal data relating to your racial or ethnic origin, political opinions, religious or philosophical beliefs, trade union membership, genetic data, biometric data, health, sex life or sexual orientation;</p>
<p style="text-align: left; padding-left: 30px;">(f) &ldquo;processing&rdquo; - doing anything with personal data including collecting, using, storing, accessing, disclosing and destroying it.</p>
<p style="text-align: left;">&nbsp;</p>
<p style="text-align: left;"><strong>4. WHAT INFORMATION WE COLLECT AND WHY</strong></p>
</div>
<div class="wrapper">
<table style="width: 100%; font-size: 80%; float: left;" cellpadding="10px">
<tbody>
<tr style="height: 35px;">
<td style="width: 25%; height: 35px; vertical-align: top;">
<p><strong>Type of information</strong></p>
</td>
<td style="width: 25%; height: 35px; vertical-align: top;">
<p><strong>Purposes</strong></p>
</td>
<td style="width: 25%; height: 35px; vertical-align: top;">
<p><strong>Legal basis of processing</strong></p>
</td>
</tr>
<tr style="height: 59px;">
<td style="width: 25%; height: 59px; vertical-align: top;">
<p>Tutor&rsquo;s name, address, email address and telephone numbers.</p>
</td>
<td style="width: 25%; height: 59px; vertical-align: top;">
<p>Managing our relationship with the tutor.</p>
</td>
<td style="width: 25%; height: 59px; vertical-align: top;">
<p>Performing our contract with tutor.</p>
<p>&nbsp;</p>
</td>
</tr>
<tr style="height: 96px;">
<td style="width: 25%; height: 96px; vertical-align: top;">
<p>Tutor's identification evidence, proof of address, proof of qualifications, CV and enhanced DBS certificate.</p>
</td>
<td style="width: 25%; height: 96px; vertical-align: top;">
<p>Confirming background information about the tutor.</p>
</td>
<td style="width: 25%; height: 96px; vertical-align: top;">
<p>For the purposes of our legitimate interests in operating our business.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</td>
</tr>
<tr style="height: 35px;">
<td style="width: 25%; height: 35px; vertical-align: top;">
<p>Tutor&rsquo;s bank account details.</p>
</td>
<td style="width: 25%; height: 35px; vertical-align: top;">
<p>Arranging payment to the tutor.</p>
</td>
<td style="width: 25%; height: 35px; vertical-align: top;">
<p>Performing our contract with the tutor.</p>
</td>
</tr>
<tr style="height: 61px;">
<td style="width: 25%; height: 61px; vertical-align: top;">
<p>Name, address, email address and telephone numbers of student and, where relevant, their parent/guardian.</p>
</td>
<td style="width: 25%; height: 61px; vertical-align: top;">
<p>Managing our relationship with the student or their parent / guardian.</p>
</td>
<td style="width: 25%; height: 61px; vertical-align: top;">
<p>Performing our contract with the student or their parent / guardian.</p>
</td>
</tr>
<tr style="height: 61px;">
<td style="width: 25%; height: 61px; vertical-align: top;">
<p>Details of educational institution attended by student and information regarding the student&rsquo;s performance.</p>
</td>
<td style="width: 25%; height: 61px; vertical-align: top;">
<p>Providing the tutor with background information about the student.</p>
</td>
<td style="width: 25%; height: 61px; vertical-align: top;">
<p>Performing our contract with the student or their parent/guardian.</p>
</td>
</tr>
<tr style="height: 61px;">
<td style="width: 25%; height: 61px; vertical-align: top;">
<p>Account information for student logins to learning platforms.</p>
</td>
<td style="width: 25%; height: 61px; vertical-align: top;">
<p>Assisting the tutor in the provision of tutoring and providing background information to the tutor about the student&rsquo;s learning.</p>
</td>
<td style="width: 25%; height: 61px; vertical-align: top;">
<p>Performing our contract with the student or their parents/guardian.</p>
</td>
</tr>
<tr style="height: 48px;">
<td style="width: 25%; height: 48px; vertical-align: top;">
<p>Educational Psychology Reports</p>
</td>
<td style="width: 25%; height: 48px; vertical-align: top;">
<p>Providing the tutor with background information about the student.</p>
</td>
<td style="width: 25%; height: 48px; vertical-align: top;">
<p>Explicit consent.</p>
</td>
</tr>
<tr style="height: 48px;">
<td style="width: 25%; height: 48px; vertical-align: top;">
<p>Financial information</p>
</td>
<td style="width: 25%; height: 48px; vertical-align: top;">
<p>For the purpose of credit checks of the student or their parent / guardian.</p>
</td>
<td style="width: 25%; height: 48px; vertical-align: top;">
<p>For the purposes of our legitimate interests in operating our business.</p>
</td>
</tr>
</tbody>
</table>
</div>
<div class="wrapper">
<p style="text-align: left;">&nbsp;</p>
<p style="text-align: left;"><strong>5. HOW WE PROTECT YOUR PERSONAL DATA</strong></p>
<p style="text-align: left;">5.1 We will only process your personal data to the extent that it is necessary for the purposes specified in this Policy and we will keep the personal data we store about you accurate and up to date. Where it is inaccurate or out of date, it will be destroyed - please let us know if your personal details change or if you become aware of any inaccuracies in the personal data we hold about you.</p>
<p style="text-align: left;">5.2 We will not keep your personal data for longer than is necessary for the purposes specified in this Policy. Where it is no longer required, it will be pseudonymised, anonymised, destroyed or erased as appropriate.</p>
<p style="text-align: left;">5.3 We will never sell your personal data or make it available to any third parties without your prior consent (which you are free to withhold) except:</p>
<p style="text-align: left; padding-left: 30px;">(a) where we use a processor, in which case we will ensure that the processor complies with this Policy and all relevant data protection legislation;</p>
<p style="text-align: left; padding-left: 30px;">(b) where we are required to do so by law;</p>
<p style="text-align: left; padding-left: 30px;">(c) as set out in the table above</p>
<p style="text-align: left; padding-left: 30px;">(d) if we sell any part of our business or assets (in which case we may disclose your personal data confidentially to the prospective buyer as appropriate in accordance with our legitimate interests).</p>
<p style="text-align: left;">5.4 We have implemented generally accepted standards of technology and operational security in order to protect personal data from loss, misuse, or unauthorised alteration or destruction. We will notify you promptly in the event of any breach of your personal data which might expose you to serious risk.</p>
<p style="text-align: left;">&nbsp;</p>
<p style="text-align: left;"><strong>6. HOW LONG WE KEEP YOUR INFORMATION</strong></p>
<p style="text-align: left;">6.1 We will hold your personal data on our systems for three years and for as long afterwards as it is in our legitimate interests to do so or for as long as necessary to comply with our legal obligations. We will review your personal data every year to establish whether we are still entitled to process it. If we decide that we are not entitled to do so, we will stop processing your personal data except we will retain your personal data in an archived form in order to be able to comply with future legal obligations e.g. compliance with tax requirements and exemptions and the establishment, exercise or defence or legal claims.</p>
<p style="text-align: left;">6.2 We securely destroy all financial information once we have used it and no longer need it.</p>
<p style="text-align: left;">&nbsp;</p>
<p style="text-align: left;"><strong>7. YOUR RIGHTS</strong></p>
<p style="text-align: left;">7.1 You have the following rights in respect of your personal data held by us. Please send all requests in respect of these rights to us:</p>
<p style="text-align: left; padding-left: 30px;">(a) to access your personal data;</p>
<p style="text-align: left; padding-left: 30px;">(b) to be provided with information about how your personal data is processed (this information is set out in this Policy);</p>
<p style="text-align: left; padding-left: 30px;">(c) to have your personal data corrected where necessary (please contact us promptly should you become aware of any incorrect or out-of-date information); (d) to have your personal data erased in certain circumstances;</p>
<p style="text-align: left; padding-left: 30px;">(e) to object to or restrict how your personal data is processed;</p>
<p style="text-align: left; padding-left: 30px;">(f) to have your personal data transferred to yourself or to another business.</p>
<p style="text-align: left;">7.2 If you consider that we have not complied with this Policy or the relevant data protection legislation in respect of your personal data, you should raise the matter with us. Any such breach will be taken seriously and will be dealt with in accordance with the relevant data protection legislation.</p>
<p style="text-align: left;">7.3 You have the right to take any complaints about how we process your personal data to the Information Commissioner:</p>
<p style="text-align: left; padding-left: 30px;">Information Commissioner&rsquo;s Office</p>
<p style="text-align: left; padding-left: 30px;">Wycliffe House</p>
<p style="text-align: left; padding-left: 30px;">Water Lane</p>
<p style="text-align: left; padding-left: 30px;">Wilmslow</p>
<p style="text-align: left; padding-left: 30px;">Cheshire</p>
<p style="text-align: left; padding-left: 30px;">SK9 5AF</p>
<p style="text-align: left; padding-left: 30px;"><a href="https://ico.org.uk/make-a-complaint/">https://ico.org.uk/make-a-complaint/</a></p>
<p style="text-align: left; padding-left: 30px;">0303 123 1113.</p>
<p style="text-align: left;">7.4 For more details, please address any questions, comments and requests regarding our data processing practices to us.</p>
<p style="text-align: center;">&nbsp;</p>
</div>
                   


                    <div class="clear">

				</div>
		</div>
	</section>
	
	
		<section>
		<div class="review_slide blue">
				<div class="wrapper">
					  <div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Our tutor was exceptional, showing the ability to convey<br>the course content in a simple yet concise manner,<br>making it easy to pick up and remember.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Thank you once again for the support<br>and guidance that you and Sebastian gave to our students,<br>it definitely did have a positive impact.</h4>
								<h5>Head of Sixth Form</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
                            <img src="images/ttquotewhite.svg" alt="" >
								<h4>A stroke of brilliance.</h4>
								<h5>Chris, father of BSc Geography dissertation student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>I now feel confident to take my exams and would like<br>to thank them for their patience and commitment<br>towards achieving my goal.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Very quick to reply to our initial search for a geography<br>tutor. They clearly have excellent knowledge of the<br>subject and the current curriculum.</h4>
								<h5>Andrea, mother of A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>James knows a lot about University<br>testing procedures and was able to give<br>advice on a difficult course.</h4>
								<h5>Rhea, mother of BSc Astro Geochemistry student</h5>
							</div>
							</div>
						</div>
					<div class="swiper-pagination"></div>
				  </div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<?php include 'footer_contact-form.php';?>
	</main>

<?php include 'footer.php' ?>
<?php require('includes/config.php'); 

$stmt = $db->prepare('SELECT catID,catTitle FROM blog_cats WHERE catSlug = :catSlug');
$stmt->execute(array(':catSlug' => $_GET['id']));
$row = $stmt->fetch();

//if post does not exists redirect user.
if($row['catID'] == ''){
	header('Location: ./');
	exit;
}
?>
<?php $title = $row['catTitle']; ?>
<?php include 'header.php' ?>
<main>
	<section>
		<div class="banner_internal blog" style="background-image: url(images/uncode-default-back.jpeg)">
			<table>
				<tr>
					<td>
						<h1>Category : <?php echo $row['catTitle'];?></h1>
					</td>
				</tr>
			</table>
			<div class="fix">
			<a href="#blog"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
		</div>
		</div>
	</section>
	<section>
		<div class="blog_panl iscotop" id="blog">
			<div class="wrapper">
				<div class="blog_left">
					<?php	
                    try {
        
                        $pages = new Paginator('4','p');
        
                        $stmt = $db->prepare('SELECT blog_posts_seo.postID FROM blog_posts_seo, blog_post_cats WHERE blog_posts_seo.postID = blog_post_cats.postID AND blog_post_cats.catID = :catID');
                        $stmt->execute(array(':catID' => $row['catID']));
        
                        //pass number of records to
                        $pages->set_total($stmt->rowCount());
                        echo '<div class="blog_left_inner">';
                        $stmt = $db->prepare('
                            SELECT 
                                blog_posts_seo.postID, blog_posts_seo.postTitle, blog_posts_seo.postSlug, blog_posts_seo.featuredImage, blog_posts_seo.postCont, blog_posts_seo.postDate 
                            FROM 
                                blog_posts_seo,
                                blog_post_cats
                            WHERE
                                 blog_posts_seo.postID = blog_post_cats.postID
                                 AND blog_post_cats.catID = :catID
                            ORDER BY 
                                postID DESC
                            '.$pages->get_limit());
                        $stmt->execute(array(':catID' => $row['catID']));
                        while($row = $stmt->fetch()){
                            
                            echo '<div class="blog_two"><a href="'.$row['postSlug'].'" class="signplus"><img src="'.str_replace("../","",$row['featuredImage']).'" alt=""></a>';
                            echo '<div class="blog_cont">';
                            echo '<h3><a href="'.$row['postSlug'].'">'.$row['postTitle'].'</a></h3>';
                            echo '<div class="date"><span><i class="fa fa-clock-o" aria-hidden="true"></i> <a href="'.$row['postSlug'].'">'.date('jS M Y', strtotime($row['postDate'])).'</a></span> <span><i class="fa fa-envelope-o" aria-hidden="true"></i> ';
                            
                            $stmt2 = $db->prepare('SELECT catTitle, catSlug	FROM blog_cats, blog_post_cats WHERE blog_cats.catID = blog_post_cats.catID AND blog_post_cats.postID = :postID');
                                    $stmt2->execute(array(':postID' => $row['postID']));
        
                                    $catRow = $stmt2->fetchAll(PDO::FETCH_ASSOC);
        
                                    $links = array();
                                    foreach ($catRow as $cat){
                                        $links[] = "<a href='c-".$cat['catSlug']."'>".$cat['catTitle']."</a>";
                                    }
                                    echo implode(", ", $links);
                            echo '</span></div>';
                            echo substr(strip_tags($row['postCont']),0,250);
                            echo '<p><a class="blog_btn" href="'.$row['postSlug'].'">Read more <i class="fa fa-angle-right"></i></a></p><hr>';
                            echo '<div class="autor">
                                    <img src="images/2ddebbe71f047a02a0adbd1e4a414201.png" alt=""> <span><a href="'.$row['postSlug'].'">by Think Tutors</a></span>
                                </div></div>';
                            echo '</div>';
        
                        }
                        echo '<div class="clear"></div>';
                        echo '</div>';
                        echo $pages->page_links('c-'.$_GET['id'].'&');
        
                    } catch(PDOException $e) {
                        echo $e->getMessage();
                    }
                    ?>
                </div>
                <div class="blog_right extra_pad">
                    <div class="widget">
                        <h3>Recent Posts</h3>
                        <ul>
                        <?php
                        $stmt = $db->query('SELECT postTitle, postSlug FROM blog_posts_seo ORDER BY postID DESC LIMIT 5');
                        while($row = $stmt->fetch()){
                            echo '<li><a href="'.$row['postSlug'].'">'.$row['postTitle'].'</a></li>';
                        }
                        ?>
                        </ul>
                    </div>
                    <div class="widget extra_sty">
                        <h3>Categories</h3>
                        <ul>
                        <?php
                        $stmt = $db->query('SELECT catTitle, catSlug FROM blog_cats ORDER BY catID DESC');
                        while($row = $stmt->fetch()){
                            echo '<li><a href="c-'.$row['catSlug'].'">'.$row['catTitle'].'</a></li>';
                        }
                        ?>
                        </ul>
                    </div>
                </div>
				<div class="clear"></div>
			</div>
		</div>
	</section>
</main>
<?php include 'footer.php' ?>
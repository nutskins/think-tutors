<?php $title= "دولي | Think Tutors"; ?>
<?php $metadescription= "Our services are fully tailored to our students’ needs whether on an hourly basis or a long-term residential placement in the UK and overseas.";?>
<?php $page = "international-tutoring"; include 'arheader.php' ?>
<main>
	<section>
		<div class="banner" style="background-image:url(images/5-edit-v4.jpg);">
		</div>
	</section>
	<section>
		<div class="int_content">
			<div class="wrapper">
				<h1>الخدمات الدولية</h1>
				<div class="left_pnl extra">
					<p>تعمل Think Tutors على المستوى الدولي ، مما يجعل خدمات السفر أو الخدمة السكنية المتميزة والسرية للعائلات في جميع أنحاء العالم. من موسكو ، إلى سنغافورة ، دبي إلى هونغ كونغ وإلى اليخوت الخاصة في موناكو ، نحن قادرون على توفير كل من المعلمين الشخصيين على المدى القصير والطويل لضمان أن تعليم جميع الطلاب الطامحين لا ينقطع.</p>
					<h2>المدرسون المتنقلون</h2>
						<p>حضر العديد من معلمينا بعض المؤسسات الأكاديمية الرائدة في المملكة المتحدة بما في ذلك جامعة أكسفورد وجامعة كامبريدج. يمكن لمدرسينا تقديم خبرتهم ومعرفتهم للطلاب الذين يدرسون في المدارس حول العالم. يمتلك المعلمون لدينا أيضًا معرفة ممتازة بإجراءات تقديم الطلبات ومتطلباتها للمدارس والجامعات الدولية الرائدة.</p>		
				</div>
				<div class="right_pnl">
					<div class="color_box">
						<h3>جميع مجالات الموضوع<br>بما في ذلك:</h3>
						<ul>
							<li>11+ قبل الاختبار</li>
							<li>11+</li>
							<li>13+</li>
							<li>مدخل مشترك</li>
							<li>GCSE/ IGCSE</li>
							<li>AS / A2 المستوى</li>
							<li>البكالوريا الدولية</li>
							<li>تطبيقات الجامعة ، بما في ذلك Oxbridge</li>
							<li>المرحلة الجامعية</li>
							<li>دراسات عليا</li>
							<li>الامتحانات المهنية (القانون ، الطب)</li>
						</ul>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</section>
	<section>
		<div class="light-color">
			<div class="wrapper">
				<div class="both_pnl">
					<div class="left">
						<img src="images/international.png" alt="">
					</div>
					<div class="right">
						<br>
						<h2>معدلات</h2>
					<p>وتناقش معدلات الرسوم الدراسية على المدى الطويل (أكثر من أسبوع واحد) على أساس فردي. للحصول على خدمة دراسية مكثفة على المدى القصير ، تبدأ الأسعار من 500 جنيه إسترليني في اليوم.</p>		
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>	
	</section>
	<section>
		<div class="int_content">
			<div class="wrapper">
				<div class="both_pnl">
					<br>
					<div class="left">
						<h2>سري وخدمة متفوقة</h2>
						<p>نقوم بتفصيل خدماتنا الدولية منذ بداية المهمة حتى الانتهاء ، بما في ذلك جميع الخدمات اللوجستية الخاصة بالسفر والإقامة.</p>
						<p>. في حالة وجود حاجة ماسة للمعلم ، يمكننا تنظيم وترتيب مدرس لمغادرة المملكة المتحدة في غضون 48 ساعة.</p>
						<p>كما هو الحال مع خدمة التعليم البريطانية المفصلة ، يتم اختيار كل معلمينا بعناية ، وفحصهم وإجراء مقابلات معهم من قبل كل من نيل وجيمس لضمان مشاركة قيمنا وشغفنا للتعلم. جميع المعلمين لدينا يحملون درجة البكالوريوس ممتازة مع معظم الحصول على مؤهلات الدراسات العليا التي تتكون من الماجستير ، الدكتوراه أو PGCSEs.</p>
						<p>كجزء من إجراءات التدقيق لدينا ، يجب أن يكون جميع معلمينا لدينا حاصلين على شهادة DBS محسنة ومحدثة من العملاء السابقين.</p>
						<p>تتمتع شبكتنا من المدرسين المتجولين بخبرة دولية سابقة والعديد منهم يجيدون لغات متعددة ، باللغات الروسية والعربية والماندرين والفرنسية. هناك عدد من معلمينا يتولون طاقمًا كفؤًا في اليوم / قائدًا أو مؤهلات أعلى ، ولذلك فهم أكثر راحة مع السفر على أساس اليخت.</p>
					</div>
					<div class="right">
						<br>
						<br>
						<br>
						<img src="images/4sm.jpg" alt="">
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>	
	</section>
	
	<style>
.panel {
    padding: 0 18px;
    background-color: white;
    max-height: 0;
    overflow: hidden;
	transition: max-height 0.2s ease-out;
	text-align:center;
	}
</style>

<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
</script>
<section dir="ltr">
		<div class="review_slide blue">
				<div class="wrapper">
					  <div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Our tutor was exceptional, showing the ability to convey<br>the course content in a simple yet concise manner,<br>making it easy to pick up and remember.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>A stroke of brilliance.</h4>
								<h5>Chris, father of BSc Geography dissertation student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Thank you once again for the support<br>and guidance that you and Sebastian gave to our students,<br>it definitely did have a positive impact.</h4>
								<h5>Head of Sixth Form</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>I now feel confident to take my exams and would like<br>to thank them for their patience and commitment<br>towards achieving my goal.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Very quick to reply to our initial search for a geography<br>tutor. They clearly have excellent knowledge of the<br>subject and the current curriculum.</h4>
								<h5>Andrea, mother of A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>James knows a lot about University<br>testing procedures and was able to give<br>advice on a difficult course.</h4>
								<h5>Rhea, mother of BSc Astro Geochemistry student</h5>
							</div>
							</div>
						</div>
					<div class="swiper-pagination"></div>
				  </div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<?php include 'arfooter_contact-form.php';?>
	</main>
<?php include 'artestimonial.php';?>
<?php include 'arfooter.php' ?>
<?php $title= "International Homeschooling | Think Tutors"; ?>
<?php $metadescription= "If your family is based outside of the UK then we can provide an expert international homeschool tutor from our elite and discreet network.";?>
<?php $page = "services"; include 'header.php' ?>
<main>
	<section>
		<div class="banner" style=" background-image:url(images/6international.jpg)">
		<div class="title"><h1>International Homeschooling</h1></div>
		</div>
	</section>
	<section>
		<div class="int_content">
			<div class="wrapper">
                    <p>Taking your child out of school to be taught at home is an important decision and there are many reasons why families decide to do this. We have advised families through this decision, offering experienced insight in the advantages and disadvantages which homeschooling often brings. Ultimately your child’s health and happiness is of upmost importance to us, so if we really feel homeschooling isn’t the best option we will be sure to voice our concerns. If your family is based outside of the UK then we can provide an expert international tutor from our elite and discreet network to provide homeschooling.</p>
                    <p>We will tailor each international assignment to suit your family and will coordinate all travel and accommodation logistics. All of our international tutors are experienced in international assignments and understand that discretion is paramount. Many tutors in our elite network are multilingual, in languages including Russian, Arabic, Mandarin and French. </p>
                    <p>Homeschooling does bring many advantages; it opens up the possibility of broadening a child’s learning as there is no longer the need to rigidly follow a national curriculum if desired. This opens up the flexibility to discover more opportunities in art, music and sport that might otherwise have been held back in a school environment. Some children are also naturally gifted and are outperforming the pace of the school curriculum, in this case homeschooling really can cultivate that talent which might have been suppressed at school. Some students also struggle at school due to the innate pressures of that environment, be it socially or a lack of support from teachers. Being at home in a relaxed atmosphere can really allow the child’s confidence to build and unlock their real potential.</p>
                    <p>Another key advantage is that our homeschooling tutors will quickly adapt their teaching methods to suit your child, allowing them to learn at their own speed. If a child grasps a concept quickly, they can progress to the more challenging concepts. Being in a one-to-one environment also allows more content to be covered in a shorter period of time and anything that isn’t understood will be revisited until the student has really grasped and digested that piece of learning. Children are far more likely to ask parents or a trusted tutor questions or say they don’t understand something than putting their hand up in a traditional classroom-based environment. This can lead to interactive learning and highlights any areas that might need more attention, thus allowing the child to build their confidence in an area that might have been neglected in mainstream education.</p>
                    <p>If you would like more information on UK school or university admissions then please visit our dedicated page or contact us for more details. </p>
				<div class="clear">

				</div>
		</div>
	</section>
	
	
		<section>
		<div class="review_slide blue">
				<div class="wrapper">
					  <div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Our tutor was exceptional, showing the ability to convey<br>the course content in a simple yet concise manner,<br>making it easy to pick up and remember.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Thank you once again for the support<br>and guidance that you and Sebastian gave to our students,<br>it definitely did have a positive impact.</h4>
								<h5>Head of Sixth Form</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
                            <img src="images/ttquotewhite.svg" alt="" >
								<h4>A stroke of brilliance.</h4>
								<h5>Chris, father of BSc Geography dissertation student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>I now feel confident to take my exams and would like<br>to thank them for their patience and commitment<br>towards achieving my goal.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Very quick to reply to our initial search for a geography<br>tutor. They clearly have excellent knowledge of the<br>subject and the current curriculum.</h4>
								<h5>Andrea, mother of A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>James knows a lot about University<br>testing procedures and was able to give<br>advice on a difficult course.</h4>
								<h5>Rhea, mother of BSc Astro Geochemistry student</h5>
							</div>
							</div>
						</div>
					<div class="swiper-pagination"></div>
				  </div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<?php include 'footer_contact-form.php';?>
	</main>

<?php include 'footer.php' ?>
<?php require('includes/config.php');
$stmt = $db->prepare('SELECT postID, postTitle, postMetaTitle, postMetaDesc, postMetaKey, postCont, featuredImage, postDate FROM blog_posts_seo WHERE postSlug = :postSlug');
$stmt->execute(array(':postSlug' => $_GET['id']));
$row = $stmt->fetch();

$currentRow = $row['postID'];

//if post does not exists redirect user.
if($row['postID'] == ''){
	header('Location: ./');
	exit;
}
?>
<?php
	if($row['postMetaTitle'] != ''){
		$title= $row['postMetaTitle'];
	}else{
		$title= $row['postTitle'];
	}
?>
<?php $metadescription= $row['postMetaDesc']; ?>
<?php $metakeywords= $row['postMetaKey']; ?>
<?php include 'header.php' ?>
<main>
	<section>
		<div class="banner_internal blog" style="background-image:url(images/7news.jpg);">
			<table>
				<tr>
					<td>
						<h1 style="color:white;"><?php echo $row['postTitle']; ?></h1>
						<span style="color:white;"><?php echo date('jS M Y', strtotime($row['postDate'])); ?></span> 
					</td>
				</tr>
			</table>
		</div>
	</section>
	<section>
		<div class="blog_panl">
			<div class="wrapper">
				<div class="blog_left extra_pad">
                	<?php echo $row['postCont']; ?>
					<div class="social">
                    	<div id="share"></div>
					</div>
					<?php /*?><div class="comment">
						<h3>Add comment </h3>
						<div class="form">
							<textarea></textarea>
							<label>Name *</label>
							<input type="text">
							<label>Email *</label>
							<input type="text">
							<label>Website</label>
							<input type="text">
							<input type="submit" value="Post Comment">
						</div>
					</div><?php */?>
				</div>
				<div class="blog_right extra_right_pad">
					<div class="widget">
						<h3>Recent Posts</h3>
						<ul>
						<?php
                        $stmt = $db->query('SELECT postTitle, postSlug FROM blog_posts_seo ORDER BY postID DESC LIMIT 5');
                        while($row = $stmt->fetch()){
                            echo '<li><a href="'.$row['postSlug'].'">'.$row['postTitle'].'</a></li>';
                        }
                        ?>
                        </ul>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<div class="pagination">
			<div class="wrapper">
            	<?php
				$prevquery= $db->query('SELECT postID, postSlug FROM blog_posts_seo WHERE postID < '.$currentRow.' ORDER BY postID DESC LIMIT 1');
				while($prevrow = $prevquery->fetch())
				{
				$previd  = $prevrow['postSlug'];
				
				echo '<a class="prev" href="'.$previd.'"><i class="fa fa-angle-left"></i> <span>Prev</span></a>';
				}
				
				$nextquery= $db->query('SELECT postID, postSlug FROM blog_posts_seo WHERE postID > '.$currentRow.' ORDER BY postID ASC LIMIT 1');
				while($nextrow = $nextquery->fetch())
				{
				$nextid  = $nextrow['postSlug'];
				
				echo '<a class="next" href="'.$nextid.'"><span>Next</span> <i class="fa fa-angle-right"></i></a>';
				}
				?>
				<div class="clear"></div>
			</div>
		</div>
	</section>
</main>
<?php include 'footer.php' ?>
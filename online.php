<?php $title= "Online Tutoring | London & Internationally | Think Tutors"; ?>
<?php $metadescription= "We provide expert private tuition online for all subjects and levels, allowing all of our students to unlock their full potential.";?>
<?php $page = "services"; include 'header.php' ?>
<main>
	<section>
		<div class="banner" style=" background-image:url(images/3tuition.jpg)">
		<div class="title"><h1>Online</h1></div>
		</div>
	</section>
	<section>
		<div class="int_content">
			<div class="wrapper">
                    <p>The growth of the internet over the past two decades has had transformative effects on the business world, increasing productivity whilst bridging oceans, time zones and languages. Many of the tools and techniques used in this modernisation have also helped students and families connect with our tutors and mentors no matter where they are based in the world.</p>
                    <p>Online tutoring and mentoring can offer convenience and flexibility if your family is based abroad or is mobile throughout the year. Face-to-face tutoring can take up precious time for a family, whereas online tuition or mentorship removes this potential burden from the daily routine and gives you more flexibility to receive tutoring or mentoring at hours you otherwise wouldn�t be able to commit to. </p>
                    <p>Online tuition can be just as interactive as face-to-face, thanks to the endless opportunities for collaboration and methods of information sharing. Services like Skype or Zoom can provide the visual and audio requirements of the student-tutor interaction. Platforms like Prezi offer both parties the chance to present information in more creative ways. Realtime collaboration can be made possible with programs like AWW App, a virtual whiteboard with multiple users contributing at the same time. Homework, reports or lesson material can easily be shared with services like WeTransfer or Microsoft Teams.</p>
                    <p>Online tuition is available at all levels and with all subjects. All of our tutors are elites in their respective fields and have years of tuition experience. They are passionate for their subject and are able to bring the subject material alive to really inspire your child to excel in their exams or coursework, even via a computer screen. </p>
				<div class="clear">

				</div>
		</div>
	</section>
	
	
		<section>
		<div class="review_slide blue">
				<div class="wrapper">
					  <div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Our tutor was exceptional, showing the ability to convey<br>the course content in a simple yet concise manner,<br>making it easy to pick up and remember.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Thank you once again for the support<br>and guidance that you and Sebastian gave to our students,<br>it definitely did have a positive impact.</h4>
								<h5>Head of Sixth Form</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
                            <img src="images/ttquotewhite.svg" alt="" >
								<h4>A stroke of brilliance.</h4>
								<h5>Chris, father of BSc Geography dissertation student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>I now feel confident to take my exams and would like<br>to thank them for their patience and commitment<br>towards achieving my goal.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Very quick to reply to our initial search for a geography<br>tutor. They clearly have excellent knowledge of the<br>subject and the current curriculum.</h4>
								<h5>Andrea, mother of A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>James knows a lot about University<br>testing procedures and was able to give<br>advice on a difficult course.</h4>
								<h5>Rhea, mother of BSc Astro Geochemistry student</h5>
							</div>
							</div>
						</div>
					<div class="swiper-pagination"></div>
				  </div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<?php include 'footer_contact-form.php';?>
	</main>

<?php include 'footer.php' ?>
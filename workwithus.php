<?php $title= "Work With Us | London & Internationally | Think Tutors"; ?>
<?php $metadescription= "If you feel passionate about your subject and can deliver tuition and or mentorship to the highest standard then we would like to hear from you.";?>
<?php $page = "services"; include 'header.php'; require_once('./includes/google-analytics-helpers.php'); ?>
<main>
	<section>
		<div class="banner" style=" background-image:url(images/2aboutus.jpg)">
		<div class="title"><h1>Work With Us</h1></div>
		</div>
	</section>
	<section>
		<div class="int_content">
			<div class="wrapper">
					<p>Our network is growing as we expand in the UK and internationally. If you feel passionate about your subject and can deliver tuition and or mentorship to the highest standard, then we would like to hear from you.</p.>
                    <p><b>We require all tutors and mentors to have and provide evidence of:</b></p>
                    <ol>
                    <li>A Bachelor�s degree with transcripts to show a first-class honours or upper second-class honours award. We do prefer tutors to hold a postgraduate degree in their field of expertise, especially when teaching at A-Level or beyond.</li>
                    <li>A curriculum vitae with a minimum of five years of full-time professional tutoring or teaching experience.</li>
                    <li>An Enhanced DBS Certificate no older than a year, and preferably registered on the DBS Update Service. If you do not hold an up-to-date certificate, we can provide you with one via Care Check. Please note, we require all onboarded tutors to be on the update service.</li>
                    <li>A minimum of two relevant referees who we can contact independently to verify your academic, professional, and tutoring attainment.</li>
                    <li>Identity (passport/driving license & proof of address).</li>
                    </ol>
                    <p>Discretion is of upmost importance as we work with many families in the public eye and so we require all tutors and mentors to sign a non-disclosure agreement. If you meet the above requirements, we aim to get back to you within 48 hours to arrange a formal interview. On completion of a successful interview, you will be expected to adhere to our comprehensive policies and procedures.</p>
                    <p>Please note that all tuition and mentorship roles through Think Tutors are for self-employed contractors and you will not be an employee of Think Tutors Limited. More information on the rules and regulations of self-employment can be found�<a href="https://www.gov.uk/employment-status/selfemployed-contractor">here</a>.</p>
				<div class="clear"></div>

		</div>
	</section>

		<section>
</section>
<section>
<?php

if(isset($_POST["signup"])) {
    echo "<script>$(document).ready(function(){
        $('html, body').animate({scrollTop: $('#errorscroll').offset().top}, 100);});
        </script>";

    // Import validation library
    require("validation-2.3.3.php");

    // Rules for validation
    $rules = array();
    $rules[] = "required,signup-data,Please indicate that you have read and agree to our Data Privacy Policy.";

    $errors = validateFields($_POST, $rules);

    // Validate captcha
    $post_data = http_build_query(
        array(
            "secret" => "6Lfkkl4UAAAAAIT1-k4GCKmKDldnRehul7SzsF8D",
            "response" => $_POST["g-recaptcha-response"],
            "remoteip" => $_SERVER["REMOTE_ADDR"]
        )
    );
    $opts = array("http" =>
        array(
            "method"  => "POST",
            "header"  => "Content-type: application/x-www-form-urlencoded",
            "content" => $post_data
        )
    );
    $context  = stream_context_create($opts);
    $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
    $result = json_decode($response);

    // Check if captcha was valid
    if (!$result->success) {
        $errors[] = "The reCAPTCHA submission failed."; // Incorrect captcha solution
    }

    if (!empty($errors)) {
        $fields = $_POST;
    } else { // No errors :)
        
        include("conn.php");

        $title  =  $_POST['title'];
		$first_name =   $_POST['first_name'];
		$last_name =   $_POST['last_name'];
		$date_of_birth = $_POST['date_of_birth'];
		$email_address = $_POST['email_address'];
		$contact_number = $_POST['contact_number'];
		$address_line_1 = $_POST['address_line_1'];
		$address_line_2 = $_POST['address_line_2'];
		$town = $_POST['town'];
		$county = $_POST['county'];
		$post_code = $_POST['post_code'];
		$country = $_POST['country'];
		$employment_status = $_POST['employment_status'];
		$personal_transport = $_POST['personal_transport'];
		$secondary = $_POST['secondary'];
		$college = $_POST['college'];
		$undergraduate = $_POST['undergraduate'];
		$masters = $_POST['masters'];
		$phd = $_POST['phd'];
		$relevant = $_POST['relevant'];
		$dbs = $_POST['dbs'];
		$pcge = $_POST['pcge'];
		$availibility = $_POST['availibility'];
		$start_date = $_POST['start_date'];
		$forseeable = $_POST['forseeable'];
		$commentsn = $_POST['commentsn'];
		$vacancy = $_POST['vacancy'];
		
		$file = $_FILES['file']['name'];
		$qualifications = $_FILES['file']['name'];
		$file_loc = $_FILES['file']['tmp_name'];
		$file_size = $_FILES['file']['size'];
		$file_type = $_FILES['file']['type'];
		$folder="uploads/";
		move_uploaded_file($file_loc,$folder.$file);
		
		$qualifications = $_FILES['qualifications']['name'];
		$file_loc1 = $_FILES['qualifications']['tmp_name'];
		$file_size1 = $_FILES['qualifications']['size'];
		$file_type1 = $_FILES['qualifications']['type'];
		$folder1="uploads/";
		move_uploaded_file($file_loc1,$folder1.$qualifications);
		
		$dbspic = $_FILES['dbspic']['name'];
		$file_loc2 = $_FILES['dbspic']['tmp_name'];
		$file_size2 = $_FILES['dbspic']['size'];
		$file_type2 = $_FILES['dbspic']['type'];
		
		$folder2="uploads/";
		move_uploaded_file($file_loc2,$folder2.$dbspic);
		$date = $_POST['date'];
		
		$bodycontent  = 'Name: '.$title. ',' .$first_name. ' ' .$last_name. '<br>';
		$bodycontent .= 'Date of Birth: '.$date_of_birth.'<br>';
		$bodycontent .= 'Email Address: '.$email_address.'<br>';
		$bodycontent .= 'Contact Number: '.$contact_number.'<br>';
		$bodycontent .= 'Address Line 1: '.$address_line_1.'<br>';
		$bodycontent .= 'Address Line 2: '.$address_line_2.'<br>';
		$bodycontent .= 'Town: '.$town.'<br>';
		$bodycontent .= 'County: '.$county.'<br>';
		$bodycontent .= 'Post Code: '.$post_code.'<br>';
		$bodycontent .= 'Country: '.$country.'<br>';
		$bodycontent .= 'Employment Status: '.$employment_status.'<br>';
		$bodycontent .= 'Personal Transport: '.$personal_transport.'<br>';
		$bodycontent .= 'Secondary: '.$secondary.'<br>';
		$bodycontent .= 'Sixth Form / College: '.$college.'<br>';
		$bodycontent .= 'Undergraduate: '.$undergraduate.'<br>';
		$bodycontent .= 'Masters: '.$masters.'<br>';
		$bodycontent .= 'Phd: '.$phd.'<br>';
		$bodycontent .= 'Relevant Employment History: '.$relevant.'<br>';
		$bodycontent .= 'DBS: '.$dbs.'<br>';
		$bodycontent .= 'PCGE: '.$pcge.'<br>';
		$bodycontent .= 'Availability: '.implode(', ', $availibility).'<br>';
		$bodycontent .= 'Start Date: '.$start_date.'<br>';
		$bodycontent .= 'Forseeable Unavailability: '.$forseeable.'<br>';
		$bodycontent .= 'Comments: '.$commentsn.'<br>';
		$bodycontent .= 'Identification: '.$file.'<br>';
		$bodycontent .= 'Qualifications: '.$qualifications.'<br>';
		$bodycontent .= 'DBS Image: '.$dbspic.'<br>';
		$bodycontent .= 'Vacancy: '.$vacancy.'<br>';
		
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		
		$webemail = 'mail@gmail.com';
		
		$headers .= "From:".$first_name." ".$last_name." <".$webemail.">\n";
		$toemail = "thinktutors1@gmail.com";
		$subject = "Personal Information Form Submitted";
		$sql = "insert into tutor_signup (title, first_name, last_name, date_of_birth, email_address, contact_number, address_line_1, address_line_2, town, county, post_code, country, employment_status, personal_transport,secondary,college,undergraduate,masters,phd,relevant,dbs,pcge,availibility,start_date,forseeable,commentsn, identification, qualifications, dbspic, vacancy, date) VALUES ('$title', '$first_name', '$last_name', '$date_of_birth', '$email_address', '$contact_number', '$address_line_1', '$address_line_2', '$town', '$county', '$post_code', '$country', '$employment_status', '$personal_transport','$secondary','$college','$undergraduate','$masters','$phd','$relevant','$dbs','$pcge', '".implode(', ', $availibility)."','$start_date','$forseeable','$commentsn','$file', '$qualifications','$dbspic', '$vacancy', '$date')";
		if (mysqli_query ($conn , $sql)) {
			//echo "Successfully To Added Detail";
			
			// Calculate the current URL
			$canonicalUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			$canonicalUrl = strtolower($canonicalUrl);
			$canonicalUrl = rtrim($canonicalUrl,"/");
			
			sendGaAnalyticsEvent(gaParseCookie(), "Secondary Goals", "Tutors Form Submission", $canonicalUrl);
			
		}else{
			//echo "Error";
		}
		mysqli_close($conn);
		if (mail($toemail, $subject, $bodycontent, $headers)) {
			$msg = "Thanks for submitting your profile. One of our team will review your application and be in touch in due course.";
				} 	else {
			$msg = "Your application could not be sent due to an unforeseen error - please try again, or alternatively, email info@thinktutors.com with your profile.";
		}
    }
}

?>

<main>
    <section id="tutorsignupform">
		<div class="wrapper">
			
		</div>
	</section>
    <div class="totor_form caldera-grid" id="errorscroll">
        <div class="wrapper">
            <form class="demo-form" method="post" action="" enctype="multipart/form-data">
                 <?php    
                    if (!empty($errors)) {
                      echo "<div class='error' style='color:#003f70;'>Please fix the following errors:\n<ul>";
                      foreach ($errors as $error)
                        echo "<li>$error</li>\n";

                      echo "</ul></div>";
                    }
                ?>
                <div class="caldera-form-page">
                <div class="form-section">  
				<div class="col-sm-12 single">
					<div class="">
						<h2>Personal Information</h2>
						<hr>
					</div>
				</div>     
                <div class="col-sm-4 first_col">
                    <div role="field" data-field-wrapper="fld_2107536" class="form-group">
                        <label class="control-label">Title <span aria-hidden="true" class="field_required">*</span></label>
                        <select name="title" class="form-control" required>
                            <option value=""></option>
                            <option value="Mr" <?php if ($fields["title"] == "Mr") echo "selected" ?>>Mr</option>
                            <option value="Mrs" <?php if ($fields["title"] == "Mrs") echo "selected" ?>>Mrs</option>
                            <option value="Miss" <?php if ($fields["title"] == "Miss") echo "selected" ?>>Miss</option>
                            <option value="Ms" <?php if ($fields["title"] == "Ms") echo "selected" ?>>Ms</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4 ">
                    <div class="form-group">
                        <label class="control-label">First name<span aria-hidden="true" class="field_required">*</span></label>
                        <input type="text" class=" form-control" name="first_name" value="<?php if(isset($fields["first_name"])){ echo $fields["first_name"];}?>" required >
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label">Last name <span aria-hidden="true" role="presentation" class="field_required">*</span></label>
                        <input type="text" class="form-control" name="last_name" value="<?php if(isset($fields["last_name"])){ echo $fields["last_name"];}?>" required>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label">Date of Birth <span aria-hidden="true" role="presentation" class="field_required">*</span></label>
                        <input type="text" id="datepicker" class="form-control" name="date_of_birth" value="<?php if(isset($fields["date_of_birth"])){ echo $fields["date_of_birth"];}?>" required>
                    </div>
                </div>
                <div class="col-sm-4 ">
                    <div class="form-group">
                        <label class="control-label">Email Address <span aria-hidden="true" role="presentation" class="field_required">*</span></label>
                        <input type="email" class=" form-control" name="email_address" value="<?php if(isset($fields["email_address"])){ echo $fields["email_address"];}?>" required >
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label">Contact Number <span aria-hidden="true" role="presentation" class="field_required">*</span></label>
                        <input type="text" class=" form-control" name="contact_number" value="<?php if(isset($fields["contact_number"])){ echo $fields["contact_number"];}?>" required>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label">Address Line 1 <span aria-hidden="true" role="presentation" class="field_required">*</span></label>
                        <input type="text" class="form-control" name="address_line_1" value="<?php if(isset($fields["address_line_1"])){ echo $fields["address_line_1"];}?>" required >
                    </div>
                    <div class="form-group">
                        <label class="control-label">Town <span aria-hidden="true" role="presentation" class="field_required">*</span></label>
                        <input type="text" class=" form-control" name="town" value="<?php if(isset($fields["town"])){ echo $fields["town"];}?>" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Post Code <span aria-hidden="true" role="presentation" class="field_required">*</span></label>
                        <input type="text" class="form-control" name="post_code" value="<?php if(isset($fields["post_code"])){ echo $fields["post_code"];}?>" required>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label">Address Line 2 <span aria-hidden="true" role="presentation" class="field_required">*</span></label>
                        <div class="">
                            <input type="text" class="form-control" name="address_line_2" value="<?php if(isset($fields["address_line_2"])){ echo $fields["address_line_2"];}?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">County <span aria-hidden="true" role="presentation" class="field_required">*</span></label>
                        <input type="text" class=" form-control" name="county" value="<?php if(isset($fields["county"])){ echo $fields["county"];}?>" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Country <span aria-hidden="true" role="presentation" class="field_required">*</span></label>
                        <input type="text" class="form-control" name="country" value="<?php if(isset($fields["country"])){ echo $fields["country"];}?>" required> 
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label">Employment Status <span aria-hidden="true" role="presentation" class="field_required">*</span></label>
                        <div class="">
                            <select name="employment_status" class="form-control" required>
                                <option value=""></option>
                                <option value="Full Time" <?php if ($fields["employment_status"] == "Full Time") echo "selected" ?>>Full Time</option>
                                <option value="Part Time" <?php if ($fields["employment_status"] == "Part Time") echo "selected" ?>>Part Time</option>
                                <option value="Self Employed" <?php if ($fields["employment_status"] == "Self Employed") echo "selected" ?>>Self Employed</option>
                                <option value="Student" <?php if ($fields["employment_status"] == "Student") echo "selected" ?>>Student</option>
                                <option value="Tutor" <?php if ($fields["employment_status"] == "Tutor") echo "selected" ?>>Tutor</option>
                                <option value="Unemployed" <?php if ($fields["employment_status"] == "Unemployed") echo "selected" ?>>Unemployed</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label">Personal Transport</label>
                        <div class="">
                            <label class="checkbox-inline"><input type="radio" name="personal_transport" value="yes" <?php if ($fields["personal_transport"] == "yes") echo "checked" ?>> Yes</label>
                            <label class="checkbox-inline"><input type="radio" name="personal_transport" value="no" <?php if ($fields["personal_transport"] == "no") echo "checked" ?>> No</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 single">
                    <hr id="fld_3865404_1" class="" style="width: 100%">
                </div>
                </div>
                <div class="form-section">
                    <div class="col-sm-12 single">
                        <div class="">
                            <h2>Qualification and Employment</h2>
                            <hr>
                        </div>
                    </div>     
                    <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label">Secondary<span aria-hidden="true" role="presentation" class="field_required">*</span></label>
                        <input type="text" class="form-control" name="secondary" value="<?php if(isset($fields["secondary"])){ echo $fields["secondary"];}?>" required >
                    </div>
                    <div class="form-group">
                        <label class="control-label">Sixth Form / College<span aria-hidden="true" role="presentation" class="field_required">*</span></label>
                        <input type="text" class=" form-control" name="college" value="<?php if(isset($fields["college"])){ echo $fields["college"];}?>" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Undergraduate<span aria-hidden="true" role="presentation" class="field_required">*</span></label>
                        <input type="text" class="form-control" name="undergraduate" value="<?php if(isset($fields["undergraduate"])){ echo $fields["undergraduate"];}?>" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Masters<span aria-hidden="true" role="presentation"></span></label>
                        <input type="text" class=" form-control" name="masters" value="<?php if(isset($fields["masters"])){ echo $fields["masters"];}?>">
                    </div>
                    <div class="form-group">
                        <label class="control-label">PhD<span aria-hidden="true" role="presentation"></span></label>
                        <input type="text" class="form-control" name="phd" value="<?php if(isset($fields["phd"])){ echo $fields["phd"];}?>">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label">Relevant Employment History</label>
                        <div class="">
                            <textarea name="relevant" class="form-control" rows="4"><?php if(isset($fields["relevant"])){ echo $fields["relevant"];}?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">DBS <span aria-hidden="true" role="presentation" class="field_required">*</span></label>
                        <div class="">
                            <label class="checkbox-inline"><input type="radio" name="dbs" value="yes" <?php if ($fields["dbs"] == "yes") echo "checked" ?> required> Yes</label>
                            <label class="checkbox-inline"><input type="radio" name="dbs" value="no" <?php if ($fields["dbs"] == "no") echo "checked" ?> required> No</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">PCGE <span aria-hidden="true" role="presentation" class="field_required">*</span></label>
                        <div class="">
                            <label class="checkbox-inline"><input type="radio" name="pcge" value="yes" <?php if ($fields["dbs"] == "yes") echo "checked" ?> required> Yes</label>
                            <label class="checkbox-inline"><input type="radio" name="pcge" value="no" <?php if ($fields["dbs"] == "no") echo "checked" ?> required> No</label>
                        </div>
                    </div>
                </div>
                </div>
                <div class="form-section">
                    <div class="col-sm-12 single">
                        <div class="">
                            <h2>Tutor Availability</h2>
                            <hr>
                        </div>
                    </div>
                    <div class="col-sm-6">
                    
                    <div class="form-group">
                        <label class="control-label">Availibility <span aria-hidden="true" role="presentation" class="field_required">*</span></label>
                        <div class="">
                            <label class="checkbox"><input type="checkbox" name="availibility[]" value="Monday" required> Monday</label><br />
                            <label class="checkbox"><input type="checkbox" name="availibility[]" value="Tuesday" required> Tuesday</label><br />
                            <label class="checkbox"><input type="checkbox" name="availibility[]" value="Wednesday" required> Wednesday</label><br />
                            <label class="checkbox"><input type="checkbox" name="availibility[]" value="Thursday" required> Thursday</label><br />
                            <label class="checkbox"><input type="checkbox" name="availibility[]" value="Friday" required> Friday</label><br />
                            <label class="checkbox"><input type="checkbox" name="availibility[]" value="Saturday" required> Saturday</label><br />
                            <label class="checkbox"><input type="checkbox" name="availibility[]" value="Sunday" required> Sunday</label>
                        </div>
                    </div>
                </div>     
                    <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label">Start Date</label>
                        <input type="text" class="form-control" name="start_date" value="<?php if(isset($fields["start_date"])){ echo $fields["start_date"];}?>"  >
                    </div>
                    <div class="form-group">
                        <label class="control-label">Forseeable Unavailability</label>
                        <input type="text" class=" form-control" name="forseeable" value="<?php if(isset($fields["forseeable"])){ echo $fields["forseeable"];}?>">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Comments</label>
                        <div class="">
                            <textarea name="commentsn" class="form-control" rows="4"><?php if(isset($fields["commentsn"])){ echo $fields["commentsn"];}?></textarea>
                        </div>
                    </div>
                </div>
                </div>
                    <div class="form-section">
                        <div class="col-sm-12 single">
                            <div class="">
                                <h2>Confirmation</h2>
                                <p><strong>By clicking submit you agree that the information provided to Think Tutors Limited is correct to the best of your knowledge and agree to Think Tutors Limited holding your data on file for up to 3 years.</strong></p>
                                <p><strong>Data will be deleted on written request.</strong></p>
                                <label style="font-family: 'Hind Vadodara', sans-serif;">
                                    <input style="-webkit-appearance: checkbox; width: 18px; height: 18px; margin: 0; position: absolute;" type="checkbox" name="signup-data" <?php if(isset($fields["signup-data"])) echo "checked"; ?> required>
                                    <span style="font-size: 16px; font-weight: 500; color: #78828c; display: inline-block; vertical-align: top; margin-top: -5px; padding-left: 30px; line-height: 25px;">By ticking this box you agree to our receipt and use of your data as set out in our <a href="https://thinktutors.com/data-privacy-policy.php">Data Privacy Policy</a></span>
                                </label>
                                <hr>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group file_pnl" >
                                <label class="control-label">Identification</label><br />
                                <input type="file" name="file" accept="image/*">
                            </div>
                        </div>
                            <div class="col-sm-4">
                            <div class="form-group file_pnl">
                                <label class="control-label">Qualifications</label><br />
                                <input type="file" name="qualifications" accept="image/*">
                            </div>
                        </div>  
                            <div class="col-sm-4">
                            <div class="form-group file_pnl">
                                <label class="control-label">DBS</label><br />
                                <input type="file" name="dbspic" accept="image/*">
                            </div>
                        </div>
                        <div class="col-sm-12" style="margin-top: 15px;">
                            <div class="form-group">
                                <div class="g-recaptcha" data-sitekey="6Lfkkl4UAAAAAOu53yXbM8HpKgiocGozfJ1Q6dDj"></div>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="col-sm-12" style="float: unset;">
                        <div class="form-group">
                            <div class="next_more form-navigation">
                                <input type="hidden" name="vacancy" value="<?php echo $_GET['vacany']; ?>" >
                                <input type="hidden" name="date" value="<?php echo date('Y-m-d H:i:s'); ?>" >
                                <div class="col-sm-3">
                                    <button type="button"  class="btn btn-default previous btn btn-info pull-left">Previous Page</button>
                                    </div>
                                    <div class="col-sm-3">
                                <button type="button"  class="btn btn-default next btn btn-info pull-left" style="display: none !important;">Next Page</button>
                                </div>
                                <div class="col-sm-3">
                                <input class="btn btn-default" type="submit" name="signup" value="submit">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>	
		        <?php if(isset($msg)){ echo $msg; } ?>
            </form>
        </div>
    </div>
				</section>

                	<section>
		<div class="review_slide blue">
				<div class="wrapper">
					  <div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Our tutor was exceptional, showing the ability to convey<br>the course content in a simple yet concise manner,<br>making it easy to pick up and remember.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Thank you once again for the support<br>and guidance that you and Sebastian gave to our students,<br>it definitely did have a positive impact.</h4>
								<h5>Head of Sixth Form</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
                            <img src="images/ttquotewhite.svg" alt="" >
								<h4>A stroke of brilliance.</h4>
								<h5>Chris, father of BSc Geography dissertation student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>I now feel confident to take my exams and would like<br>to thank them for their patience and commitment<br>towards achieving my goal.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Very quick to reply to our initial search for a geography<br>tutor. They clearly have excellent knowledge of the<br>subject and the current curriculum.</h4>
								<h5>Andrea, mother of A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>James knows a lot about University<br>testing procedures and was able to give<br>advice on a difficult course.</h4>
								<h5>Rhea, mother of BSc Astro Geochemistry student</h5>
							</div>
							</div>
						</div>
					<div class="swiper-pagination"></div>
				  </div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<?php include 'footer_contact-form.php';?>
	</main>

<?php include 'footer.php' ?>
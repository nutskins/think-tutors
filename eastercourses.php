<?php $title= "Easter Courses | London & Internationally | Think Tutors"; ?>
<?php $metadescription= "During the Easter Holidays, we provide an exhilarating and alternative approach to A-Level revision. Find out about how we combine sports with revision.";?>
<?php $page = "services"; include 'header.php' ?>
<main>
	<section>
		<div class="banner" style=" background-image:url(images/eastercourses.jpg)">
		<div class="title"><h1>Easter Alpine Revision Courses</h1></div>
		</div>
	</section>
	<section>
		<div class="int_content">
			<div class="wrapper">
					<p>During the Easter Holidays we provide an exhilarating and alternative approach to A-Level revision for years 12 & 13. Our week long course combines snow sports in the French Alps (skiing/snowboarding) with bespoke and focussed tuition for A-Level students leading up to their final exams. After many weeks of studying at school, it can be difficult to remain motivated and focussed whilst revising at home or in a classroom-based revision course. Providing the student with a new and stimulating learning environment helps to improve motivation and when combined with physical activity aids in increased attention span and improved cognitive function. We will provide expert tutors in any subject required. All of our tutors have obtained an excellent Bachelor's degree as a minimum, with the majority having pursued postgraduate degrees in their specialist fields. As a result, they know first-hand what it takes to strive for academic excellence. Our tutors are quick and skilled at identifying gaps in the students’ understanding and focus on resolving these to deliver high results.</p>
				<div class="clear">
				<div class="wrapper">
				<p><b>Includes:</b></p>
					<ul>
						<li>Transfer from Geneva or Chambéry airport to Méribel.</li>
						<li>Return transfer from Méribel to Geneva or Chambéry airport.</li>
						<li>Business class flights from any UK airport to Geneva or Chambéry airport.</li>
						<li>Chalet accommodation with private room.</li>
						<li>Breakfast, Lunch, Afternoon Tea, Dinner and Snacks provided by chalet chef with nutrition in mind to stay mentally agile.</li>
						<li>Ski lessons or guide depending on experience:</li>
						<ul><li>Monday-Saturday (morning – early afternoon)</li>
						<li>Beginner (lessons)</li>
						<li>Intermediate (lessons)</li>
						<li>Advanced (guided)</li></ul>
						<li>Ski/Snowboard equipment provided. </li>
						<li>A number of expert tutors to conduct 1-1 and small group tuition in any subject.</li>
					</ul>
						<p><b>Cost:</b></p>
						<p>£20,000 per student (inclusive VAT).</p>
						<p><b><u>Course 1</u></b></p>
						<p><b>Location: Méribel, France</b></p>
						<p><b>Dates</b>: 27th March – 3rd April 2022</p>
						<p><b>Subjects</b>: All A-Level subjects</p>
						<p><b><u>Course 2</u></b></p>
						<p><b>Location: Méribel, France</b></p>
						<p><b>Dates</b>: 3rd April – 10th April 2022</p>
						<p><b>Subjects</b>: All A-Level subjects</p>
						<p><b><u>Course 3</u></b></p>
						<p><b>Location: Méribel, France</b></p>
						<p><b>Dates</b>: 10th April – 17th April 2022</p>
						<p><b>Subjects</b>: All A-Level subjects</p>
						<br>
					<p>Places are limited so please contact us at <a href="mailto:info@thinktutors.com?Easter%20Courses%20with%20Think%2-Tutors" target="_top"><u>info@thinktutors.com</u></a> to check availability. Due to Covid-19 we are continually reviewing international travel restrictions. If there are travel or safety concerns our 2022 Easter courses will be cancelled and a full refund will be made available.</p> 
				</div>
				</div>
		</div>
	</section>
	
	
		<section>
		<div class="review_slide blue">
				<div class="wrapper">
					  <div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Our tutor was exceptional, showing the ability to convey<br>the course content in a simple yet concise manner,<br>making it easy to pick up and remember.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Thank you once again for the support<br>and guidance that you and Sebastian gave to our students,<br>it definitely did have a positive impact.</h4>
								<h5>Head of Sixth Form</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
                            <img src="images/ttquotewhite.svg" alt="" >
								<h4>A stroke of brilliance.</h4>
								<h5>Chris, father of BSc Geography dissertation student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>I now feel confident to take my exams and would like<br>to thank them for their patience and commitment<br>towards achieving my goal.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Very quick to reply to our initial search for a geography<br>tutor. They clearly have excellent knowledge of the<br>subject and the current curriculum.</h4>
								<h5>Andrea, mother of A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>James knows a lot about University<br>testing procedures and was able to give<br>advice on a difficult course.</h4>
								<h5>Rhea, mother of BSc Astro Geochemistry student</h5>
							</div>
							</div>
						</div>
					<div class="swiper-pagination"></div>
				  </div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<?php include 'footer_contact-form.php';?>
	</main>

<?php include 'footer.php' ?>
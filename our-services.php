<?php $title= "Bespoke tutoring services that exceed our students goals | Think Tutors"; ?>
<?php $metadescription= "Our services are fully tailored to our students’ needs whether on an hourly basis or a long-term residential placement in the UK and overseas.";?>
<?php $page = "services"; include 'header.php' ?>
<main>
	<section>
		<div class="banner" style=" background-image:url(images/tutor.jpg)">
		</div>
	</section>
	<section>
		<div class="int_content">
			<div class="wrapper">
				<h1>Our Services</h1>
				<div class="left_pnl extra">
					<p>We know that no two students are the same, so why should their tuition be? Our services are fully tailored to the individual needs of each of our students’  whether on an hourly basis or a long-term residential placement in the UK or overseas.</p>
					<p>Our tutors have exceptional international experience, having worked across the world from  Moscow to Singapore, Dubai to Hong Kong and on private yachts in Monaco. As a result, they  are accustomed to planning tuition to changing schedules. For our international services please see our dedicated <a href="international-tutoring.php">international page</a>.</p>
					<p>All of our tutors are handpicked, vetted and interviewed by both Neil and James to ensure they share our values and passion for learning. All of our tutors hold excellent bachelor degrees with most having obtained postgraduate qualifications ranging from  masters to PhDs and PGCSEs. As part of our vetting procedure, all of our tutors are required to be in possession of an up-to-date enhanced DBS certificate and provide references from previous clients.</p>
					<h2>Schools</h2>
					<p>Beginning with the 11+ pre-test, our tutors can guide students through the learning process and offer advice to parents on the application procedures for schools across the country. Our family of tutors can then be involved every step of the way from the 11+ and 13+ to the Common Entrance Exam, all the way through to GCSEs, A-levels and International Baccalaureate.</p>
					<p>Our tutors are constantly ensuring they’re up to speed with the latest specifications, changes and exam board requirements. This ensures students are learning effectively and efficiently to excel on exam day.</p>
					<p>We’re proud to work closely with schools, providing tutors during the timetabled day and into the early evening to work on an individual and group basis. Schools use our tutors for intensive pre-exam revision sessions to address any subject weaknesses from the arts to sciences, and to improve exam techniques and confidence.</p>
					<p>Did you know we also offer tuition via Skype? Ideal for those in remote areas or perhaps with a child away at boarding school and in need of a little extra help, our Skype tuition allows our tutors to help students in all locations. </p>
					<p>Our tutors cover a broad range of subjects including, but not limited to, Mathematics, English Literature & Language, Geography, Chemistry, Biology and Physics.</p>
					<h2>Entrance Exams</h2>
					<p>Our highly experienced family of tutors have a wealth of knowledge in guiding young minds through a range of entrance examinations. From the 11+ pre-test to Oxford and Cambridge University entrance exams, our tutors can be there every step of the way to assist your child through these key moments.</p>
					<p>Our tutors are highly experienced in guiding students through all exams, building confidence and knowledge along the way.</p>
					<p>University applications can seem daunting, but our tutors and past alumni can aid with course selection, personal statements, Oxbridge entrance exams and course specific exams (LNAT, BMAT, STEP and TSA).</p>
					<h2>University</h2>
					<p>For university students, Think Tutors has a specialised team of tutors who are able to mentor students. From the very beginning of their first year at undergraduate level all the way through to post graduate studies, our expert tutors can work with students to find a solution that works around their time table. We are able to provide tuition for any subject and can offer guidance with exam revision, proofreading of coursework and dissertations.</p>				</div>
				<div class="right_pnl">
					<div class="color_box">
						<h3>All subject areas<br>inclusive of:</h3>
						<ul>
							<li>11+ pre-test</li>
							<li>11+</li>
							<li>13+</li>
							<li>Common Entrance</li>
							<li>GCSE/ IGCSE</li>
							<li>AS/A2 Level</li>
							<li>International Baccalaureate</li>
							<li>University Applications, including Oxbridge</li>
							<li>Undergraduate</li>
							<li>Postgraduate</li>
							<li>Professional Exams (Law, Medicine)</li>
						</ul>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</section>
	<section>
		<div class="light-color">
			<div class="wrapper">
				<div class="both_pnl">
					<div class="left">
						<h2>Rates</h2>
						<p>Our rates start at £80 per hour based on one-to-one UK tuition. Long-term residential placements will be discussed on an individual basis. Please see our international page for worldwide tuition rates. We invoice at the end of each month and do not charge any introduction fees for our services.</p>
					</div>
					<div class="right">
						<img src="images/4sm.jpg" alt="">
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>	
	</section>
	<section>
	<div class="process_pnl">
			<div class="wrapper">
				<h2>The Process</h2>
				<div class="pro_pnl">
				<div class="pro_three" data-aos="fade-up" data-aos-duration="1000">
					<img src="images/number-01.png" alt="">
					<p>Discuss the students’ needs and academic aspirations, either via telephone, Skype or face-to-face, no matter the location.</p>
				</div>
				<div class="pro_three" data-aos="fade-up" data-aos-duration="1100">
					<img src="images/number-02.png" alt="">
					<p>James and Neil meet with a carefully selected group of their tutors to discuss the students' aims and expectations.</p>
				</div>
				<div class="pro_three" data-aos="fade-up" data-aos-duration="1200">
					<img src="images/number-03.png" alt="">
					<p>A selection of tutor profiles are sent to you, outlining qualifications, previous employment, testimonials, confirmation of an up-to-date enhanced DBS and a CV for your review.</p>
				</div>
				<div class="clear"></div>
				<div class="pro_three" data-aos="fade-up" data-aos-duration="1300">
					<img src="images/number-04.png" alt="">
					<p>If desired, an introductory face-to-face meeting or conference call can be arranged.</p>
				</div>
				<div class="pro_three" data-aos="fade-up" data-aos-duration="1400">
					<img src="images/number-05.png" alt="">
					<p>Once a tutor is confirmed a dedicated and detailed learning plan is created with monthly milestones set and reported on.</p>
				</div>
				<div class="pro_three" data-aos="fade-up" data-aos-duration="1500">
					<img src="images/number-06.png" alt="">
					<p>James and Neil remain in close contact with both the tutor and client throughout the duration of the tuition and are available 24 hours a day, 7 days a week.</p>
				</div>
				<div class="clear"></div>
				</div>
			</div>
		</div>
	</section>
		<section>
		<div class="review_slide blue">
				<div class="wrapper">
					  <div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Our tutor was exceptional, showing the ability to convey<br>the course content in a simple yet concise manner,<br>making it easy to pick up and remember.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Thank you once again for the support<br>and guidance that you and Sebastian gave to our students,<br>it definitely did have a positive impact.</h4>
								<h5>Head of Sixth Form</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
                            <img src="images/ttquotewhite.svg" alt="" >
								<h4>A stroke of brilliance.</h4>
								<h5>Chris, father of BSc Geography dissertation student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>I now feel confident to take my exams and would like<br>to thank them for their patience and commitment<br>towards achieving my goal.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Very quick to reply to our initial search for a geography<br>tutor. They clearly have excellent knowledge of the<br>subject and the current curriculum.</h4>
								<h5>Andrea, mother of A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>James knows a lot about University<br>testing procedures and was able to give<br>advice on a difficult course.</h4>
								<h5>Rhea, mother of BSc Astro Geochemistry student</h5>
							</div>
							</div>
						</div>
					<div class="swiper-pagination"></div>
				  </div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<?php include 'footer_contact-form.php';?>
	</main>

<?php include 'footer.php' ?>
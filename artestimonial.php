<section dir="rtl">
	<div class="testimonial">
		<div class="wrapper">
			<div class="testi_reviews">
				<ul>
					<div class="swiper-container">
							<div class="swiper-wrapper">
								<div class="swiper-slide">
									<li>A vision of connecting aspiring students with vibrant and enthusiastic tutors, who are elites in their fields</li>
								</div>
								<div class="swiper-slide">
									<li>A personalised service, meeting the individual needs of each and every student</li>
								</div>
								<div class="swiper-slide">
									<li>We are passionate about enabling our students to reach their full potential</li>
								</div>
							</div>
						<div class="swiper-pagination"></div>
					</div>
				</ul>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>

<?php $title= "GCSE & IGCSE Tutors | Think Tutors"; ?>
<?php $metadescription= "We provide expert private tuition for all GCSE subjects, from science to the arts, allowing all of our students to unlock their full potential.";?>
<?php $page = "services"; include 'header.php' ?>
<main>
	<section>
		<div class="banner" style=" background-image:url(images/3tuition.jpg)">
		<div class="title"><h1>GCSE / IGCSE</h1></div>
		</div>
	</section>
	<section>
		<div class="int_content">
			<div class="wrapper">
                    <p>GCSE and IGCSE examinations are the culmination of a child’s formative years of schooling from Pre-Prep to Senior School. GCSEs and IGCSEs are taken in May and June of Year 11 covering the core subjects of Maths, English and Science (Biology, Chemistry or Physics) in addition to a broad range of options based on your child’s preference in Year 9. These additional subjects can include Geography, History, ICT, Religious Studies, Art, Music, Economics, Classics, Latin and Modern Languages. The examinations are commissioned by five leading exam boards recognised by the Joint Council for Qualifications, which are Edexcel, AQA, OCR, WJEC and CIE each holding equal merit. The style of questioning in each subject will vary depending on the exam board, each of our tutors knows the intricacies of the examinations so can advise and provide the correct learning material accordingly.</p>
                    <p>We will provide you with a subject specific tutor who is an elite in their respective field and is passionate for bringing the learning experience alive for your child. As at every other stage of education, success in GCSE/IGCSE exams requires confidence and nurturing of the student’s own talents to unlock their full potential, which we believe every child has in abundance. We can work with your child over a longer period of time, starting with two hours per week over the course of one or two years. Also, if there is a sudden need for tuition, perhaps due to an unforeseen mock result, we are happy to step in and provide immediate assistance over a more intensive period. Quite often the child will know the subject matter at hand, but it is the approach to the exam which is letting them down. By bringing out their confidence and cultivating the mindset of thinking through the question thoroughly before putting pen to paper allows many students to shine when it comes to the day of the exam.</p>
                    <p>We are also proud to work closely with schools, providing tutors during the timetabled day and into the early evening to work on an individual and group basis. Schools use our tutors for intensive pre-exam revision sessions to address any subject weaknesses from the arts to sciences, and to improve exam technique and confidence.</p>
				<div class="clear">

				</div>
		</div>
	</section>
	
	
		<section>
		<div class="review_slide blue">
				<div class="wrapper">
					  <div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Our tutor was exceptional, showing the ability to convey<br>the course content in a simple yet concise manner,<br>making it easy to pick up and remember.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Thank you once again for the support<br>and guidance that you and Sebastian gave to our students,<br>it definitely did have a positive impact.</h4>
								<h5>Head of Sixth Form</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
                            <img src="images/ttquotewhite.svg" alt="" >
								<h4>A stroke of brilliance.</h4>
								<h5>Chris, father of BSc Geography dissertation student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>I now feel confident to take my exams and would like<br>to thank them for their patience and commitment<br>towards achieving my goal.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Very quick to reply to our initial search for a geography<br>tutor. They clearly have excellent knowledge of the<br>subject and the current curriculum.</h4>
								<h5>Andrea, mother of A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>James knows a lot about University<br>testing procedures and was able to give<br>advice on a difficult course.</h4>
								<h5>Rhea, mother of BSc Astro Geochemistry student</h5>
							</div>
							</div>
						</div>
					<div class="swiper-pagination"></div>
				  </div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<?php include 'footer_contact-form.php';?>
	</main>

<?php include 'footer.php' ?>
<?php $title= "11 Plus Tutors | London & Internationally | Think Tutors"; ?>
<?php $metadescription= "We provide expert private tuition for 11 Plus, allowing all of our students to unlock their full potential. Find out more.";?>
<?php $page = "services"; include 'header.php' ?>
<main>
	<section>
		<div class="banner" style=" background-image:url(images/3tuition.jpg)">
		<div class="title"><h1>Common Entrance at 11 Plus</h1><div>
		</div>
	</section>
	<section>
		<div class="int_content">
			<div class="wrapper">
					<p>Traditionally taken for entrance into the leading independent girls’ schools, the 11 Plus examination is used as a means of testing and benchmarking prospective students in Year 6 for entry to senior school at Year 7. Our aim is to support your family through this process by providing the very best tuition and experienced advice on the different schooling options available to your child based on their ability and interests. </p>
					<p>The key to success is to start early, up to 12 months prior to the exam, so your child has time to build confidence in the examination material but also the examination structure. Quite often children have been taught the subject matter at school but have not been given exposure to the style of exam questions faced during the 11 Plus test. One of our expert tutors will quickly build a rapport and identify any learning gaps your child may have. Concurrently the tutor will work with your child to build confidence and familiarity in the examination questions themselves by working through numerous example questions and mock tests. By the time of the exam we want each child to be relaxed, confident and completely familiar with the exam so it just feels like another friendly tuition session.</p>
					<p>The ISEB Common Entrance at 11 Plus exam consists of three disciplines:</p>
					<ul>
						<li><b>English</b>: Test of the child’s ability to write creatively.</li>
						<li><b>Maths</b>: Test of the child’s mental maths ability but also math concepts and solving problems in multiple steps.</li>
						<li><b>Science</b>: Testing a range of scientific ability in Chemistry, Physics and Biology.</li>
					</ul>
						<p>The majority of independent schools which offer ISEB 11 Plus entry do so in January for girls and November for boys. </p>
						<p>Schools which accept Common Entrance at 11 Plus include:</p><br>
						<div class="schoolsrow">
						<div class="schoolscolumn">
							<ul>
						<li>Alleyn’s Junior School</li>
						<li>Benenden School </li>
						<li>Downe House School</li>
						<li>Queen Anne's School </li>
						<li>Sherborne Girls School </li>
</ul>
</div>
<div class="schoolscolumn">
	<ul>
						<li>St Mary's School </li>
						<li>St Swithun's School </li>
						<li>Westonbirt School </li>
						<li>Woldingham School</li>
						<li>Wycombe Abbey School </li>
						</ul>
</div>
</div>
				<div class="clear">

				</div>
		</div>
	</section>
	
	
	<section>
		<div class="review_slide blue">
				<div class="wrapper">
					  <div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Our tutor was exceptional, showing the ability to convey<br>the course content in a simple yet concise manner,<br>making it easy to pick up and remember.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Thank you once again for the support<br>and guidance that you and Sebastian gave to our students,<br>it definitely did have a positive impact.</h4>
								<h5>Head of Sixth Form</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
                            <img src="images/ttquotewhite.svg" alt="" >
								<h4>A stroke of brilliance.</h4>
								<h5>Chris, father of BSc Geography dissertation student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>I now feel confident to take my exams and would like<br>to thank them for their patience and commitment<br>towards achieving my goal.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Very quick to reply to our initial search for a geography<br>tutor. They clearly have excellent knowledge of the<br>subject and the current curriculum.</h4>
								<h5>Andrea, mother of A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>James knows a lot about University<br>testing procedures and was able to give<br>advice on a difficult course.</h4>
								<h5>Rhea, mother of BSc Astro Geochemistry student</h5>
							</div>
							</div>
						</div>
					<div class="swiper-pagination"></div>
				  </div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<?php include 'footer_contact-form.php';?>
	</main>

<?php include 'footer.php' ?>
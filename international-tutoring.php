<?php $title= "Bespoke tutoring services that exceed our students goals | Think Tutors"; ?>
<?php $metadescription= "Our services are fully tailored to our students’ needs whether on an hourly basis or a long-term residential placement in the UK and overseas.";?>
<?php $page = "international-tutoring"; include 'header.php' ?>
<main>
	<section>
		<div class="banner" style="background-image:url(images/5-edit-v4.jpg);">
		</div>
	</section>
	<section>
		<div class="int_content">
			<div class="wrapper">
				<h1>International Services</h1>
				<div class="left_pnl extra">
					<p>Think Tutors operates internationally, bringing our discreet and superior travelling or residential service to families all over the world. From Moscow, to Singapore, Dubai to Hong Kong and to private yachts in Monaco, we are able to provide both short and long-term personal tutors to ensure that the education of all aspiring students goes uninterrupted.</p>
					<h2>Travelling Tutors</h2>
						<p>Many of our tutors attended some of the UK’s leading academic institutions including Oxford University and Cambridge University. Our tutors are able to bring their expertise and knowledge to students  studying in schools around the world. Our tutors also have an excellent knowledge of the application procedure and requirements for leading international schools and universities.</p>		
				</div>
				<div class="right_pnl">
					<div class="color_box">
						<h3>All subject areas<br>inclusive of:</h3>
						<ul>
							<li>11+ pre-test</li>
							<li>11+</li>
							<li>13+</li>
							<li>Common Entrance</li>
							<li>GCSE/ IGCSE</li>
							<li>AS/A2 Level</li>
							<li>International Baccalaureate</li>
							<li>University Applications, including Oxbridge</li>
							<li>Undergraduate</li>
							<li>Postgraduate</li>
							<li>Professional Exams (Law, Medicine)</li>
						</ul>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</section>
	<section>
		<div class="light-color">
			<div class="wrapper">
				<div class="both_pnl">
					<div class="left">
						<img src="images/international.png" alt="">
					</div>
					<div class="right">
						<br>
						<h2>Rates</h2>
					<p>Rates for long term tuition (over one week) are discussed on an individual basis. For a short term intensive tuition service, rates start at £500 per day.</p>		
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>	
	</section>
	<section>
		<div class="int_content">
			<div class="wrapper">
				<div class="both_pnl">
					<br>
					<div class="left">
						<h2>Discreet and Superior Service</h2>
						<p>We tailor our international services from the very beginning of an assignment through to the completion, including all travel and accommodation logistics.</p>
						<p>In the event of an urgent requirement for a tutor , we can organise and arrange for a tutor to leave the UK within 48 hours.</p>
						<p>As with the bespoke UK tuition service, all of our tutors are handpicked, vetted and interviewed by both Neil and James to ensure they share our values and passion for learning. All of our tutors hold excellent bachelor degrees with most having obtained postgraduate qualifications consisting of masters, PhDs or PGCSEs.</p>
						<p>As part of our vetting procedure, all of our tutors are required to be in possession of an up-to-date enhanced DBS certificate and references from previous clients.</p>
						<p>Our network of travelling tutors have previous international experience and many are multilingual, in languages including Russian, Arabic, Mandarin and French. A number of our tutors hold competent crew/day skipper or higher qualifications and are therefore more than comfortable with yacht based travel.</p>
					</div>
					<div class="right">
						<br>
						<br>
						<br>
						<img src="images/4sm.jpg" alt="">
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>	
	</section>
	
	<style>
.panel {
    padding: 0 18px;
    background-color: white;
    max-height: 0;
    overflow: hidden;
	transition: max-height 0.2s ease-out;
	text-align:center;
	}
</style>

<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
</script>
		<section>
		<div class="review_slide blue">
				<div class="wrapper">
					  <div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Our tutor was exceptional, showing the ability to convey<br>the course content in a simple yet concise manner,<br>making it easy to pick up and remember.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Thank you once again for the support<br>and guidance that you and Sebastian gave to our students,<br>it definitely did have a positive impact.</h4>
								<h5>Head of Sixth Form</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
                            <img src="images/ttquotewhite.svg" alt="" >
								<h4>A stroke of brilliance.</h4>
								<h5>Chris, father of BSc Geography dissertation student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>I now feel confident to take my exams and would like<br>to thank them for their patience and commitment<br>towards achieving my goal.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Very quick to reply to our initial search for a geography<br>tutor. They clearly have excellent knowledge of the<br>subject and the current curriculum.</h4>
								<h5>Andrea, mother of A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>James knows a lot about University<br>testing procedures and was able to give<br>advice on a difficult course.</h4>
								<h5>Rhea, mother of BSc Astro Geochemistry student</h5>
							</div>
							</div>
						</div>
					<div class="swiper-pagination"></div>
				  </div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<?php include 'footer_contact-form.php';?>
	</main>

<?php include 'footer.php' ?>
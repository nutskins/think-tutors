<?php $title= "ISEB Pre-Test | Think Tutors"; ?>
<?php $metadescription= "We provide expert private tuition for ISEB Pre-Test, allowing all of our students to unlock their full potential.";?>
<?php $page = "services"; include 'header.php' ?>
<main>
	<section>
		<div class="banner" style=" background-image:url(images/3tuition.jpg)">
		<div class="title"><h1>Common Entrance Pre-Test</h1></div>
		</div>
	</section>
	<section>
		<div class="int_content">
			<div class="wrapper">
                    <p>The Common Entrance Pre-Test is used by many independent schools to assess a child’s ability in Year 6 or Year 7, prior to further assessment (13 Plus) in Year 8. The ISEB Pre-Test is used to select only the top students to take forward for the final test, and covers:</p>
                    <ul>
                        <li><b>Verbal Reasoning</b>: Test of the child’s grammar and vocabulary in English. Involves problem solving with words and texts. </li>
                        <li><b>Non-Verbal Reasoning</b>: Test of the child’s ability to solve problems with diagrams and pictures.</li>
                        <li><b>English</b>: Test of the child’s ability to write creatively.</li>
                        <li><b>Maths</b>: Test of the child’s mental maths ability but also math concepts and solving problems in multiple steps.</li>
                    </ul>
                    <p>The exams are commissioned by GL Assessment in a multiple test format and are taken online either at the prospective school or at your child’s current prep school.</p>
                    <p>The key to success is to start early, up to 12 months prior to the exam, so your child has time to build confidence in the examination material but also the examination structure. Quite often children have been taught the subject matter at school but have not been given exposure to the style of exam questions faced during the Pre-Test. One of our expert tutors will quickly build a rapport and identify any learning gaps your child may have. Concurrently the tutor will work with your child to build confidence and familiarity in the examination questions themselves by working through numerous example questions and mock tests. By the time of the exam we want each child to be relaxed, confident and completely familiar with the exam so it just feels like another friendly tuition session.</p>
					<p>Schools which use the ISEB Common Entrance Pre-Test include:</p><br>
					<div class="schoolsrow">
					<div class="schoolscolumn">
                    <ul>
                        <li>Ardingly College</li>
                        <li>Bedford School</li>
                        <li>Benenden School</li>
                        <li>Berkhamsted Boys School</li>
                        <li>Berkhamsted Girls School</li>
                        <li>Bloxham School</li>
                        <li>Bradfield College</li>
                        <li>Brighton College</li>
                        <li>Bryanston School</li>
                        <li>Canford School</li>
                        <li>Caterham School</li>
                        <li>Charterhouse</li>
                        <li>City of London School</li>
                        <li>Cranleigh School</li>
                        <li>Culford School</li>
                        <li>Dauntsey's School</li>
                        <li>Dulwich College</li>
                        <li>Eton College Windsor</li>
                        <li>Felsted School Stebbing</li>
                        <li>Fulham Senior</li>
                        <li>Gateways School</li>
                        <li>Harrow School</li>
                        <li>Headington School</li>
                        <li>Heathside</li>
						<li>Hull Collegiate</li>
						<li>Hurstpierpoint</li>
						<li>King's College</li>
						</ul>
</div>
						<div class="schoolscolumn">
							<ul>
                        <li>King's School</li>
                        <li>Lancing College</li>
                        <li>Leeds Grammar</li>
                        <li>Leys School</li>
                        <li>Marlborough College</li>
                        <li>Oratory School</li>
                        <li>Palmers Green High School</li>
                        <li>Pangbourne College</li>
                        <li>Radley College</li>
                        <li>Roedean School</li>
                        <li>Seaford College</li>
                        <li>St Dunstan's College</li>
                        <li>St Edward's School</li>
                        <li>St Mary's School</li>
                        <li>St Paul's School</li>
                        <li>St Swithun's School</li>
                        <li>Stowe School</li>
                        <li>Sutton Valence School</li>
                        <li>Tonbridge School</li>
                        <li>University College School</li>
                        <li>Wellington College</li>
                        <li>Westminster School</li>
                        <li>Wetherby Senior</li>
                        <li>Winchester College</li>
                        <li>Woldingham School</li>
                        <li>Worth School</li>
					</ul>
</div>
				<div class="clear">

				</div>
		</div>
	</section>
	
	
		<section>
		<div class="review_slide blue">
				<div class="wrapper">
					  <div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Our tutor was exceptional, showing the ability to convey<br>the course content in a simple yet concise manner,<br>making it easy to pick up and remember.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Thank you once again for the support<br>and guidance that you and Sebastian gave to our students,<br>it definitely did have a positive impact.</h4>
								<h5>Head of Sixth Form</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
                            <img src="images/ttquotewhite.svg" alt="" >
								<h4>A stroke of brilliance.</h4>
								<h5>Chris, father of BSc Geography dissertation student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>I now feel confident to take my exams and would like<br>to thank them for their patience and commitment<br>towards achieving my goal.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Very quick to reply to our initial search for a geography<br>tutor. They clearly have excellent knowledge of the<br>subject and the current curriculum.</h4>
								<h5>Andrea, mother of A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>James knows a lot about University<br>testing procedures and was able to give<br>advice on a difficult course.</h4>
								<h5>Rhea, mother of BSc Astro Geochemistry student</h5>
							</div>
							</div>
						</div>
					<div class="swiper-pagination"></div>
				  </div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<?php include 'footer_contact-form.php';?>
	</main>

<?php include 'footer.php' ?>
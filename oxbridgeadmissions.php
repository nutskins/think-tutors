<?php 

// Calculate the full URL
$fullUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$fullUrl = strtolower($fullUrl);

// Redirect contact page to non-.php url
if($fullUrl == "https://thinktutors.com/oxbridgeadmissions.php") {
  	header("Location: https://thinktutors.com/oxbridgeadmissions" ,TRUE, 301);
	exit;
}

?>
<?php $title= "Oxbridge Admissions Tutor | Think Tutors"; ?>
<?php $metadescription= "Our consultants are experts in Oxbridge (Oxford & Cambridge) admissions, from interview preparation to entrance tests (TSA, LNAT, BMAT & CAT).";?>
<?php $page = "services"; include 'header.php' ?>
<main>
	<section>
		<div class="banner" style=" background-image:url(images/5admissions.jpg)">
		<div class="title"><h1>Oxbridge Admissions</h1></div>
		</div>
	</section>
	<section>
		<div class="int_content">
			<div class="wrapper">
                    <p>Oxford and Cambridge are renowned institutions, respected globally for providing world class university education, with unrivalled alumni connections. Gaining a place at either university is highly intense and requires a unique approach along with strong academic attainment. We have experienced advisors who themselves have an Oxbridge education and have advised many of our prospective students and families on the application process, allowing them to stand out from the crowd and gain entry into one of these prestigious universities. Our advisors are there to assist with the entire process from your personal statement, entrance test (TSA, LNAT, BMAT and CAT), the interview and other assessments.</p>
                    <p>The deadline for Oxbridge applications is the 15th October, which is much earlier than all other UK universities. Any applications received after this deadline will not be accepted and you must apply for either Oxford or Cambridge, not both. Oxford and Cambridge operate a collegiate based system. The college is where you will live, study and socialise for much of your degree. It is advisable to check which colleges offer subjects you are interested in and go from there. However, approximately one in five of successful Oxford applicants and 25% of Cambridge applicants end up at a different college to the one they applied for.</p>
                    <p>Both institutions typically invite all applicants who stand a realistic chance of being offered a place to attend an interview. At Oxford, interviews take place in December and can be one or two days. The applicant will typically stay in the college they are applying for to get a feel for the environment. At Cambridge, the majority of interviews will be held in the first three weeks of December and just take one day, with a small number taking place in January. The interview will be with one or two tutors from the course they are applying for. In general, interviews give the universities and colleges a chance to assess your abilities, your interest in the subject and your wider talents. Our Oxbridge admissions advisors fully prepare you for this process by providing detailed advice, mock interviews and department specific knowledge.</p>
                    <p>Usually, the interviews are in the style of a tutorial for Oxford or supervision for Cambridge, to see how well you respond to that style of teaching. The tutors may give you a piece to read, give you some data to interpret or a problem to solve and then discuss it with them. They are not trying to catch you out, rather, to see your thought process. A good interview takes the course of a conversation, discussing ideas and wider topics, rather than a series of questions and answers.</p>
                    <ul>
                        <li><b>Oxford</b>: A* A* A    -    A A A</li>
                        <li><b>Cambridge</b>: A* A* A    -    A* A  A </li>
</ul>
				<div class="clear">

				</div>
		</div>
	</section>
	
	
		<section>
		<div class="review_slide blue">
				<div class="wrapper">
					  <div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Our tutor was exceptional, showing the ability to convey<br>the course content in a simple yet concise manner,<br>making it easy to pick up and remember.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Thank you once again for the support<br>and guidance that you and Sebastian gave to our students,<br>it definitely did have a positive impact.</h4>
								<h5>Head of Sixth Form</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
                            <img src="images/ttquotewhite.svg" alt="" >
								<h4>A stroke of brilliance.</h4>
								<h5>Chris, father of BSc Geography dissertation student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>I now feel confident to take my exams and would like<br>to thank them for their patience and commitment<br>towards achieving my goal.</h4>
								<h5>A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>Very quick to reply to our initial search for a geography<br>tutor. They clearly have excellent knowledge of the<br>subject and the current curriculum.</h4>
								<h5>Andrea, mother of A-level student.</h5>
							</div>
							</div>
							<div class="swiper-slide">
							<div class="review">
								<img src="images/ttquotewhite.svg" alt="" >
								<h4>James knows a lot about University<br>testing procedures and was able to give<br>advice on a difficult course.</h4>
								<h5>Rhea, mother of BSc Astro Geochemistry student</h5>
							</div>
							</div>
						</div>
					<div class="swiper-pagination"></div>
				  </div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<?php include 'footer_contact-form.php';?>
	</main>

<?php include 'footer.php' ?>